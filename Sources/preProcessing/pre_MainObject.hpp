#ifndef pre_MainObject_hpp
#define pre_MainObject_hpp

#include <QObject>
#include <QCommandLineParser>
#include <QDir>
#include <QFile>
#include <QDebug>
#include <Headers/FingAnInterface.hpp>

class mainObject : public QObject {
	
	Q_OBJECT
	
public:
	mainObject(QObject* parent = Q_NULLPTR);
	
	QSettings settings;
	void defaultParameters();
	
	public Q_SLOTS:
	/** The main function: where everything begins. */
	Q_SLOT void mainFunction();
};

#endif /* pre_MainObject_hpp */
