#include <chrono>
#include <debugLib.hpp>
#include <QCoreApplication>
#include <QTimer>
#include "pre_MainObject.hpp"

int main(int argc, char* argv[]) {
	// Initialize application
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("fa_orfo");
    QCoreApplication::setOrganizationName("DMind");
    QCoreApplication::setApplicationVersion("1.0");
	QTimer::singleShot(0, new mainObject(&app), SLOT(mainFunction())); // make the thread start at the beginning of the event loop
    return app.exec();
}
