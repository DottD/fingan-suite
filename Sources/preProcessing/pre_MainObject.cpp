#include "pre_MainObject.hpp"

using namespace std;
using namespace cv;
using namespace fm;
using namespace FA;

mainObject::mainObject(QObject* parent) : QObject(parent){}

void mainObject::defaultParameters(){
	settings.beginGroup("ImageCroppingSimple");
	settings.setValue("minVariation", 0.01);
	settings.setValue("marg", 5);
	settings.endGroup();
	
	settings.beginGroup("ImageBimodalize");
	settings.setValue("brightness", 0.35);
	settings.setValue("leftCut", 0.25);
	settings.setValue("rightCut", 0.5);
	settings.setValue("histSmooth", 25);
	settings.setValue("reparSmooth", 10);
	settings.endGroup();
	
	settings.beginGroup("ImageEqualize");
	settings.setValue("minMaxFilter", 5);
	settings.setValue("mincp1", 0.75);
	settings.setValue("mincp2", 0.9);
	settings.setValue("maxcp1", 0.0);
	settings.setValue("maxcp2", 0.25);
	settings.endGroup();
	
	settings.beginGroup("TopMask");
	settings.setValue("scanAreaAmount", 0.1);
	settings.setValue("gradientFilterWidth", 0.25);
	settings.setValue("binarizationLevel", 0.2);
	settings.setValue("f", 3.5);
	settings.setValue("slopeAngle", 1.5);
	settings.setValue("lev", 0.95);
	settings.setValue("gaussianFilterSide", 5);
	settings.setValue("marg", 5);
	settings.endGroup();
	
	settings.beginGroup("ImageSignificantMask");
	settings.setValue("medFilterSide", 2);
	settings.setValue("gaussFilterSide", 3);
	settings.setValue("minFilterSide", 5);
	settings.setValue("dilate1RadiusVarMask", 5);
	settings.setValue("erodeRadiusVarMask", 35);
	settings.setValue("dilate2RadiusVarMask", 20);
	settings.setValue("maxCompNumVarMask", 2);
	settings.setValue("minCompThickVarMask", 75);
	settings.setValue("maxHolesNumVarMask", -1);
	settings.setValue("minHolesThickVarMask", 18);
	settings.setValue("binLevVarMask", 0.45);
	settings.setValue("histeresisThreshold1Gmask", 30); // in mathematica il threshold è 0.085 - consigliano un rapporto 1:2 - 1:3
	settings.setValue("histeresisThreshold2Gmask", 70);
	settings.setValue("radiusGaussFilterGmask", 10);
	settings.setValue("minMeanIntensityGmask", 0.2);
	settings.setValue("dilate1RadiusGmask", 10);
	settings.setValue("erodeRadiusGmask", 25);
	settings.setValue("dilate2RadiusGmask", 10);
	settings.setValue("maxCompNumGmask", 2);
	settings.setValue("minCompThickGmask", 75);
	settings.setValue("maxHolesNumGmask", -1);
	settings.setValue("minHolesThickGmask", 15);
	settings.setValue("histeresisThreshold3Gmask", 25); // in mathematica il threshold è 0.075 - consigliano un rapporto 1:2 - 1:3
	settings.setValue("histeresisThreshold4Gmask", 50);
	settings.setValue("dilate3RadiusGmask", 10);
	settings.setValue("erode2RadiusGmask", 5);
	settings.setValue("histeresisThreshold5Gmask", 45); // in mathematica il threshold è 0.095 - consigliano un rapporto 1:2 - 1:3
	settings.setValue("histeresisThreshold6Gmask", 90);
	settings.setValue("dilate4RadiusGmask", 4);
	settings.setValue("radiusGaussFilterComp", 30); // previously 20
	settings.setValue("meanIntensityCompThreshold", 0.6); // previously 0.25
	settings.setValue("dilateFinalRadius", 10);
	settings.setValue("erodeFinalRadius", 20);
	settings.setValue("smoothFinalRadius", 10);
	settings.setValue("maxCompNumFinal", 2);
	settings.setValue("minCompThickFinal", 75);
	settings.setValue("maxHolesNumFinal", 4);
	settings.setValue("minHolesThickFinal", 30);
	settings.setValue("fixedFrameWidth", 20);
	settings.setValue("smooth2FinalRadius", 20);
	settings.endGroup();
}

void mainObject::mainFunction(){
	// Initialize the command line parser
	QCommandLineParser parser;
	parser.setApplicationDescription("This tool is part of the FingAn Suite. It takes a fingerprint image as input and perform pre-processing (contrast and brightness adjustment, segmentation, ridge amplification).");
	parser.addHelpOption();
	parser.addVersionOption();
	parser.addPositionalArgument("imageFile", "Path to a fingerprint image, which has to pre-processed.");
	parser.addPositionalArgument("outputPath", "Path to the output folder, where the adjusted image will be stored.");
	
	// Parse the command line arguments
	QCoreApplication* app = QCoreApplication::instance();
	parser.process(*app);
	const QStringList args = parser.positionalArguments();
	if (args.length() != 2) throw QtCustomException("Syntax error: number of input arguments not valid.");
	const QString& imagePath = args[0];
	const QString& outputPath = args[1];
	
	// Load default parameters
	defaultParameters();
	
	// Preprocess the image
	PreProcessing* preprocessing = new PreProcessing;
	preprocessing->setFileName(imagePath);
	preprocessing->run();
	
	// Compute output path
	DoublePrecision::Image image = preprocessing->getFinalImage();
	rescale(image, 255., true);
	QString imageName = QDir(outputPath).absoluteFilePath("adjusted.png");
	imwrite(imageName.toStdString(), image);
	
	QCoreApplication::instance()->quit();
}
