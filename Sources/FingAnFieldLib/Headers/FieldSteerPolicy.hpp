#ifndef FieldSteerPolicy_h
#define FieldSteerPolicy_h

#include <opencv2/opencv.hpp>

namespace fm {
	template <typename PrecisionPolicy>
	class FieldSteerPolicy {
	private:
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::Field Field;
		
	protected:
		Complex referenceDir;
		
		virtual void initWithPoint(cv::Point point) = 0;
		
		virtual void initWithRadialVector(Complex p) = 0;
		
	public:
		Complex getReferenceDir() {
			return this->referenceDir;
		};
	};
	
	template <typename PrecisionPolicy>
	class CentralFieldConcordant : public FieldSteerPolicy<PrecisionPolicy> {
	private:
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::Field Field;
		
	protected:
		
		virtual Field& getFieldSqrtRef() = 0;
		
		/** Initialize the steering class with information about the current point. */
		void initWithPoint(cv::Point point) {
			// Take the square root of the current field value
			this->referenceDir = this->getFieldSqrtRef().operator()(point);
		};
		
		/** Initialize the steering class with information about the current radial vector. */
		void initWithRadialVector(Complex) {};
		
	};
	
	template <typename PrecisionPolicy>
	class RadialVectorConcordant : public FieldSteerPolicy<PrecisionPolicy> {
	private:
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::Field Field;
		
	protected:
		
		/** Initialize the steering class with information about the current point. */
		void initWithPoint(cv::Point) {};
		
		/** Initialize the steering class with information about the current radial vector. */
		void initWithRadialVector(Complex p) {
			this->referenceDir = p;
		};
	};
}

#endif /* FieldSteerPolicy_h */
