#ifndef FieldOperations_h
#define FieldOperations_h

#include <opencv2/opencv.hpp>

namespace fm {
	
	/** Computes the element-wise magnitude of a complex valued matrix.
	 @param[in] input Complex-valued matrix of which the magnitude should be computed.
	 @return The matrix of the magnitude of each element.
	 */
	template <class PrecisionPolicy>
	typename PrecisionPolicy::Image magnitude(const typename PrecisionPolicy::Field input){
		std::vector<typename PrecisionPolicy::Image> channels;
		cv::split(input, channels);
		typename PrecisionPolicy::Image mag;
		cv::magnitude(channels[0], channels[1], mag);
		return mag;
	}
	
	/** Computes the element-wise phase of a complex valued matrix.
	 @param[in] input Complex-valued matrix of which the phase should be computed.
	 @return The matrix of the phase of each element.
	 */
	template <class PrecisionPolicy>
	typename PrecisionPolicy::Image phase(const typename PrecisionPolicy::Field input){
		std::vector<typename PrecisionPolicy::Image> channels;
		cv::split(input, channels);
		typename PrecisionPolicy::Image phase;
		cv::phase(channels[0], channels[1], phase);
		return phase;
	}
	
	/** Convert a complex valued matrix in the polar representation.
	 @param[in] input Complex-valued matrix.
	 @param[out] magnitude Magnitude of the given matrix.
	 @param[out] phase Phase of the given matrix.
	 */
	template <class PrecisionPolicy>
	void cartToPolar(const typename PrecisionPolicy::Field input,
					 typename PrecisionPolicy::Image& magnitude,
					 typename PrecisionPolicy::Image& phase){
		std::vector<typename PrecisionPolicy::Image> xy;
		cv::split(input, xy);
		cv::cartToPolar(xy[0], xy[1], magnitude, phase);
	}
	
	/** Convert a complex valued matrix in the cart representation.
	 @param[in] magnitude Magnitude of the given matrix.
	 @param[in] phase Phase of the given matrix.
	 @param[out] output Complex-valued matrix.
	 */
	template <class PrecisionPolicy>
	void polarToCart(const typename PrecisionPolicy::Image magnitude,
					 const typename PrecisionPolicy::Image phase,
					 typename PrecisionPolicy::Field& output){
		std::vector<typename PrecisionPolicy::Image> xy(2, typename PrecisionPolicy::Image());
		cv::polarToCart(magnitude, phase, xy[0], xy[1]);
		cv::merge(xy, output);
	}
	
	/** Computes the element-wise complex sqrt of a complex valued matrix.
	 @param[in] input Complex-valued matrix of which the sqrt should be computed.
	 @return The field of the sqrt of each element.
	 */
	template <class PrecisionPolicy>
	typename PrecisionPolicy::Field sqrt(const typename PrecisionPolicy::Field input) {
		std::vector<typename PrecisionPolicy::Image> xy, mp(2, typename PrecisionPolicy::Image());
		cv::split(input, xy); // split channels of input field
		cv::cartToPolar(xy[0], xy[1], mp[0], mp[1]); // to polar
		mp[0] = cv::sqrt(mp[0]); // extract square root of magnitude
		mp[1] /= 2.0; // halve the phase
		cv::polarToCart(mp[0], mp[1], xy[0], xy[1]); // to cart
		typename PrecisionPolicy::Field output;
		cv::merge(xy, output); // merge channels to a new field
		return output;
	}
	
	/** Computes the element-wise normalization of a complex valued matrix.
	 @param[in] input Complex-valued matrix of which the normalization should be computed.
	 @return The normalized field.
	 */
	template <class PrecisionPolicy>
	typename PrecisionPolicy::Field normalize(const typename PrecisionPolicy::Field input) {
		typename PrecisionPolicy::Image magnitude, phase;
		typename PrecisionPolicy::Field output;
		// Get the polar representation
		cartToPolar<PrecisionPolicy>(input, magnitude, phase);
		// Set magnitude to 1
		magnitude.setTo(1, magnitude > 0);
		// Get the cartesian representation again
		polarToCart<PrecisionPolicy>(magnitude, phase, output);
		return output;
	}
	
	/** Computes the element-wise complex conjugate of a complex valued matrix.
	 @param[in] input Complex-valued matrix of which the magnitude should be computed.
	 @return The matrix of the magnitude of each element.
	 */
	template <class PrecisionPolicy>
	typename PrecisionPolicy::Field conj(const typename PrecisionPolicy::Field input){
		std::vector<typename PrecisionPolicy::Image> channels;
		cv::split(input, channels);
		channels[1] *= -1.0;
		typename PrecisionPolicy::Field output;
		cv::merge(channels, output);
		return output;
	}
	
	/** Computes the element-wise sqrt and sqrt normalization of a complex valued matrix.
	 Computes the sqrt and sqrt normalization of a complex valued matrix.
	 @param[in] input Complex-valued matrix of which the sqrt should be computed.
	 @param[out] sqrtout Complex-valued matrix that will contain the sqrt; no initialization needed,
	 but if the matrix is already allocated there should be no reallocation.
	 @param[out] normsqrtout Complex-valued matrix that will contain the normsqrt; no initialization needed,
	 but if the matrix is already allocated there should be no reallocation.
	 */
	template <class PrecisionPolicy>
	void sqrtAndNormSqrt(const typename PrecisionPolicy::Field input,
						 typename PrecisionPolicy::Field& sqrtout,
						 typename PrecisionPolicy::Field& normsqrtout) {
		typedef typename PrecisionPolicy::Image Image;
		std::vector<Image> xy;
		Image magnitude, phase;
		cv::split(input, xy); // split channels of input field
		cv::cartToPolar(xy[0], xy[1], magnitude, phase); // to polar
		cv::sqrt(magnitude, magnitude); // extract square root of magnitude
		phase /= 2.0; // halve the phase
		// Convert to xy representation and assign to sqrtout
		cv::polarToCart(magnitude, phase, xy[0], xy[1]); // to cart
		cv::merge(xy, sqrtout); // merge channels to a new field
		// Set magnitude to 1, convert to cartesian and assign to normsqrtout
		magnitude.setTo(1.0, magnitude > 0.0);
		cv::polarToCart(magnitude, phase, xy[0], xy[1]); // to cart
		cv::merge(xy, normsqrtout); // merge channels to a new field
	}
	
}

#endif /* FieldOperations_h */
