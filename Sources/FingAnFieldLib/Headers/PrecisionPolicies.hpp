#ifndef PrecisionPolicies_h
#define PrecisionPolicies_h

#include <opencv2/opencv.hpp>

namespace fm {
	/** Common base definitions. */
	class BaseDefinitions {
	public:
		typedef enum {
			ActualField,
			SqrtField,
			NormSqrtField
		} FieldChoice;
	};
	
	/** Common non floating point definition */
	class IntegerPrecision : public BaseDefinitions {
	public:
		typedef int Int;
		typedef unsigned char Uchar;
		typedef cv::Mat_<Uchar> Mask;
		
		const Uchar MaskFALSE = 0x0;
		const Uchar MaskTRUE = ~MaskFALSE;
	};
	
	/** Single precision policy. */
	class SinglePrecision : public IntegerPrecision {//lead to error ... IntepolationPolicy cannot work with it
	public:
		typedef float Real;
		typedef cv::Vec<Real, 2> Point;
		typedef cv::Mat_<Point> PointList;
		typedef Point Complex;
		typedef cv::Mat_<Complex> Field;
		typedef cv::Mat_<Real> Image;
		
		const float CV_PI_2 = float(CV_PI / 2.0f);
		const float CV_PI_4 = float(CV_PI / 4.0f);
		const float CV_PI_8 = float(CV_PI / 8.0f);
		const float CV_PI_16 = float(CV_PI / 16.0f);
	};
	
	/** Double precision policy. */
	class DoublePrecision : public IntegerPrecision {
	public:
		typedef double Real;
		typedef cv::Vec<Real, 2> Point;
		typedef cv::Mat_<Point> PointList;
		typedef Point Complex;
		typedef cv::Mat_<Complex> Field;
		typedef cv::Mat_<Real> Image;
		
		const double CV_PI_2 = CV_PI / 2.0;
		const double CV_PI_4 = CV_PI / 4.0;
		const double CV_PI_8 = CV_PI / 8.0;
		const double CV_PI_16 = CV_PI / 16.0;
	};
	
}

#endif /* PrecisionPolicies_h */
