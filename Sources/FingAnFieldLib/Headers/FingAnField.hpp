#ifndef FingAnField_h
#define FingAnField_h

#include <Headers/QtCustomException.hpp>
#include <Headers/CommonOperations.hpp>
#include <Headers/PrecisionPolicies.hpp>
#include <Headers/MorphologyOperations.hpp>
#include <Headers/AlglibInterpolation.hpp>
#include <Headers/WeightPolicies.hpp>
#include <Headers/FieldChoicePolicies.hpp>
#include <Headers/ShapePolicies.hpp>
#include <Headers/FieldSteerPolicy.hpp>
#include <Headers/FieldOperators.hpp>
#include <Headers/FieldOperations.hpp>
#include <Headers/ComputeFrequency.hpp>
#include <Headers/ComputeOrientation.hpp>
#include <Headers/ComputeFieldFromImage.hpp>

#endif /* FingAnField_h */
