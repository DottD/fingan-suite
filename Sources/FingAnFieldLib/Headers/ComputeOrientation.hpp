#ifndef ComputeOrientation_h
#define ComputeOrientation_h

#include <opencv2/opencv.hpp>

#include <Headers/PrecisionPolicies.hpp>
#include <Headers/FieldOperations.hpp>

namespace fm {
	
	/**
	 
	 */
	template <typename PrecisionPolicy>
	bool ComputeOrientation(typename PrecisionPolicy::Field& field,
							const typename PrecisionPolicy::Image image,
							const typename PrecisionPolicy::Mask mask,
							const typename PrecisionPolicy::Int radius,
							const typename PrecisionPolicy::Real radDirGrad, // raggio gradiente direzionale
							const typename PrecisionPolicy::Real var1DirGrad, // varianza gradiente direzionale 1
							const typename PrecisionPolicy::Real exp1DirGrad, // esponente gradiente direzionale 1
							const typename PrecisionPolicy::Real var2DirGrad, // varianza gradiente direzionale 2
							const typename PrecisionPolicy::Real exp2DirGrad, // esponente gradiente direzionale 2
							const typename PrecisionPolicy::Int numAngles, // numero di angoli campione
							const typename PrecisionPolicy::Real weightGaussRadius, // rispetto a radius
							const typename PrecisionPolicy::Real weightGaussExp, // esponente pesi
							const typename PrecisionPolicy::Real fieldGaussRadius) { // rispetto a radius
		// Pull PrecisionPolicy types
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Image Image;
		typedef typename PrecisionPolicy::Field Field;
        // Parameter check
        if (numAngles == 0 || radDirGrad == 0.0 || weightGaussRadius == 0.0 || fieldGaussRadius == 0.0 || var2DirGrad == 0.0) {
			throw QtCustomException("Null parameter not allowed");
		}
		
		// Parameter computation
		Int _radDirGrad = (Int)std::round(radDirGrad * (Real)radius);
		Int _weightGaussRadius = (Int)std::round(weightGaussRadius * (Real)radius);
		Int _fieldGaussRadius = (Int)std::round(fieldGaussRadius * (Real)radius);
		Real _var2DirGrad = var2DirGrad * (Real)radius;
		
		// Create the list of angles to check
		arma::Col<Real> angles = arma::linspace(0.0, CV_PI-1E-10/*CV_PI not included*/, numAngles);
		
		// For each angle apply the directional gradient filter and compute the related field
		Image totWeightMatrix = Image::zeros(image.size());
		field = Field::zeros(image.size());
		// Preallocate matrices
		Image weightMatrix(image.size()), realPartMatrix(image.size()), imagPartMatrix(image.size());
		Field currField(image.size());
		for (const Real& angle : angles) {
			// Apply the filter
			cv::filter2D(image, weightMatrix, // input/output
						 -1, // output has the same type as input
						 fm::GenDirGradMatrix<PrecisionPolicy>(_radDirGrad, angle, var1DirGrad, exp1DirGrad, _var2DirGrad, exp2DirGrad), // kernel
						 cv::Point(_radDirGrad+1, _radDirGrad+1), // kernel center position
						 0.0, // value added after convolution
						 cv::BorderTypes::BORDER_REFLECT_101);
			// Compute the magnitude of the filter response, blur and raise to a given power
			weightMatrix = cv::abs(weightMatrix);
			cv::GaussianBlur(weightMatrix, weightMatrix,
							 cv::Size(2*_weightGaussRadius+1, 2*_weightGaussRadius+1), Real(_weightGaussRadius)/2.0);
			cv::pow(weightMatrix, weightGaussExp, weightMatrix);
			// Use the filter response as magnitude for the constant field given by the current direction
			realPartMatrix = cos(2.0*angle) * weightMatrix;
			imagPartMatrix = sin(2.0*angle) * weightMatrix;
			cv::merge(std::vector<Image>({realPartMatrix, imagPartMatrix}), currField);
			// Add current field to total field, and current weight matrix to the total one
			field += currField;
			totWeightMatrix += weightMatrix;
		}
		
		// Apply the total weight matrix
		totWeightMatrix.setTo( 1.0, (totWeightMatrix == 0.0) );
		Field totWeightField(totWeightMatrix.size());
		cv::mixChannels(totWeightMatrix, totWeightField, {0, 0, 0, 1});
		cv::divide( field, totWeightField, field );
		
		// Smooth field
		cv::GaussianBlur(field, field, cv::Size(2*_fieldGaussRadius+1, 2*_fieldGaussRadius+1), Real(_fieldGaussRadius)/2.0);
		
		// Put to zero the elements outside the given mask, if any
		if (!mask.empty()) field.setTo( 0, ~mask );
		// Rescale
		Real minVal, maxVal;
		cv::minMaxIdx( fm::magnitude<PrecisionPolicy>(field), &minVal, &maxVal );
		if (maxVal > 0) field /= maxVal;
		else return false;
		
		return true;
	}
	
}

#endif /* ComputeOrientation_h */
