#ifndef FieldOperators_hpp
#define FieldOperators_hpp

#include <alglib/interpolation.h>
#include <armadillo>
#include <QVector>
#include <Headers/QtCustomException.hpp>
#include <Headers/FieldChoicePolicies.hpp>
#include <Headers/WeightPolicies.hpp>
#include <Headers/FieldSteerPolicy.hpp>
#include <Headers/ShapePolicies.hpp>
#include <Headers/AlglibInterpolation.hpp>
#include <Headers/BilinearInterpolation.hpp>
#include <Headers/MorphologyOperations.hpp>

namespace fm {
	template <typename PrecisionPolicy>
	class FieldOperator {
		
	private:
		// Pull types from super class
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Point Point;
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::PointList PointList;
		typedef typename PrecisionPolicy::Field Field;
		typedef typename PrecisionPolicy::Image Image;
		
	protected:
		Image strength;
		
		virtual Field& getFieldRef() = 0;
		
		/** Computes the local field value for the given point. 
		 Each subclass must implement its own method; the perform
		 method will loop over the points where the field is not zero
		 and apply the computeLocalValue method to create a new field.
		 */
		virtual void computeLocalValue(cv::Point point,
									   Complex& newVal) = 0;
		
		/** Define some operations to be performed at the very end of the computation.
		 When the new field is created by the repeated application of the
		 computeLocalValue method, the additionalOperations method is called
		 to eventually modify this newly created field. After that, this new field
		 is assigned to the one stored in the class.
		 */
		virtual void additionalOperations(Field& newField) = 0;
		
	public:
		void setStrength(Real newStrength, cv::Size size) {
			if (newStrength < 0.0 || newStrength > 1.0) throw QtCustomException("FieldOperator::setStrength - the strength must be in [0,1].");
			this->strength.create(size);
			this->strength.setTo(newStrength);
		};
		void setStrength(cv::InputArray newStrength) {
			if (newStrength.empty()) throw QtCustomException("FieldOperator::setStrength - given empty strength array.");
			// Create the matrix
			this->strength.create(newStrength.size());
			// Check if a field or an image has been given
			if (newStrength.type() == Image().type()) {
				Image S = newStrength.getMat_();
				if (cv::countNonZero(S < 0.0 | S > 1.0) > 0) throw QtCustomException("FieldOperator::setStrength - the strength values must all be in [0,1].");
				this->strength = S.clone();
			} else {
				throw QtCustomException("FieldOperator::setStrength - strength must be of type Image or Field.");
			}
		};
		
		void perform() {
			// Check matrix dimensions
			if (!this->strength.empty() && this->strength.size() != this->getFieldRef().size())
				throw QtCustomException("FieldOperator::perform - the strength array must be empty or have the same size of the field.");
			// Declarations
			Field newField(this->getFieldRef().size());
			newField = Complex(0.0, 0.0); // reset the field
			// Select the elements to loop over
			cv::Mat_<cv::Point> points;
			cv::findNonZero( magnitude<PrecisionPolicy>(this->getFieldRef()) > 0, points );
			
			// Loop over points where the field is non zero
			for (const cv::Point& point : points) {
				Complex& newVal = newField(point);
				
				// Compute the local value according to a set of rules defined in subclasses
				this->computeLocalValue(point, newVal);
			}
			
			// Give subclasses the chance to modify the newly created field before assigning to this->field
			this->additionalOperations(newField);
			
			// Assign the new field to the stored one
			this->getFieldRef() = newField;
		};
		
		void get(Field& field) {
			field = this->getFieldRef().clone();
		};
		
		void getInternalRef(Field& field) {
			field = this->getFieldRef();
		};
	};
	
	template <typename PrecisionPolicy,
	template <typename> class WeightPolicy = WeightNone,
	template <typename> class FieldChoicePolicy = UseActualField,
    template <typename> class InterpolationPolicy = AlglibInterpolation>
	class FieldSmooth : public FieldOperator<PrecisionPolicy>,
	public WeightPolicy<PrecisionPolicy>,
	public FieldChoicePolicy<PrecisionPolicy>,
	public WeightedSumCircle<PrecisionPolicy>,
	public CentralFieldConcordant<PrecisionPolicy>,
	public InterpolationPolicy<PrecisionPolicy> {
		
	private:
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Point Point;
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::Field Field;
		typedef typename PrecisionPolicy::Image Image;
		typedef typename PrecisionPolicy::FieldChoice FieldChoice;
		
	protected:
		Field& getFieldRef() {
			return InterpolationPolicy<PrecisionPolicy>::getFieldRef();
		};
		
		void computeLocalValue(cv::Point point,
							   Complex& newVal) {
			WeightedSumCircle<PrecisionPolicy>::computeLocalValue(point, newVal);
		};
		
		void additionalOperations(Field& newField){
			// Average the new field with the previous one
			if (this->strength.empty()) throw QtCustomException("FieldSmooth::additionalOperations - strength not set.");
			std::vector<Image> fieldXY(2, Image()), newChannels(2, Image());
			cv::split(this->field, fieldXY);
			cv::split(newField, newChannels);
			newChannels[0] = (1-this->strength).mul(fieldXY[0]) + this->strength.mul(newChannels[0]);
			newChannels[1] = (1-this->strength).mul(fieldXY[1]) + this->strength.mul(newChannels[1]);
			// Set the phase of currField as newField's one
			// Set the magnitude of currField as the largest between currField's and newField's
			Image newMagnitude, newPhase,
			oldMagnitude = fm::magnitude<PrecisionPolicy>(this->field);
			cv::cartToPolar(newChannels[0], newChannels[1], newMagnitude, newPhase);
			newMagnitude = max(oldMagnitude, newMagnitude);
			cv::polarToCart(newMagnitude, newPhase, newChannels[0], newChannels[1]);
			cv::merge(newChannels, newField);
		};
		
		Complex calc(FieldChoice choice,
					 Point p) {
			return InterpolationPolicy<PrecisionPolicy>::calc(choice, p);
		}
		
		/** Override the weight functions squaring their result. */
		Real computeWeight(Point p,
						   Point interpPoint) {
			return pow( WeightPolicy<PrecisionPolicy>::computeWeight(p, interpPoint), 2.0);
		};
		
		Complex takeFieldValue(Point interpPoint) {
			return FieldChoicePolicy<PrecisionPolicy>::takeFieldValue(interpPoint);
		};
		
		Complex chooseOutwardDir(Complex F,
								 Complex centralValue) {
			return FieldChoicePolicy<PrecisionPolicy>::chooseOutwardDir(F, centralValue);
		};
		
		Complex angleFunction(Complex val) {
			return FieldChoicePolicy<PrecisionPolicy>::angleFunction(val);
		};
		
		Field& getFieldSqrtRef() {
			return InterpolationPolicy<PrecisionPolicy>::getFieldSqrtRef();
		};
		
		void initWithPoint(cv::Point point) {
			CentralFieldConcordant<PrecisionPolicy>::initWithPoint(point);
		};
		
		void initWithRadialVector(Complex p) {
			CentralFieldConcordant<PrecisionPolicy>::initWithRadialVector(p);
		};
		
		Complex getReferenceDir() {
			return CentralFieldConcordant<PrecisionPolicy>::getReferenceDir();
		};
		
	public:
		Field operator<<(Field& in) {
			this->setField(in);
			this->perform();
			Field out;
			this->get(out);
			return out;
		};
	};
	
	template <typename PrecisionPolicy,
	template <typename> class WeightPolicy = WeightTangent,
    template <typename> class InterpolationPolicy = AlglibInterpolation>
	class FieldDrift : public FieldOperator<PrecisionPolicy>,
	public WeightPolicy<PrecisionPolicy>,
	public UseNormSqrtField<PrecisionPolicy>,
	public WeightedSumCircle<PrecisionPolicy>,
	public RadialVectorConcordant<PrecisionPolicy>,
	public InterpolationPolicy<PrecisionPolicy> {
		
	private:
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Point Point;
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::Field Field;
		typedef typename PrecisionPolicy::Image Image;
		typedef typename PrecisionPolicy::FieldChoice FieldChoice;
		
	protected:
		Field& getFieldRef() {
			return InterpolationPolicy<PrecisionPolicy>::getFieldRef();
		};
		
		void computeLocalValue(cv::Point point,
							   Complex& newVal) {
			WeightedSumCircle<PrecisionPolicy>::computeLocalValue(point, newVal);
		};
		
		void additionalOperations(Field&) {};
		
		Complex calc(FieldChoice choice,
					 Point p) {
			return InterpolationPolicy<PrecisionPolicy>::calc(choice, p);
		}
		
		Real computeWeight(Point p,
						   Point interpPoint) {
			return WeightPolicy<PrecisionPolicy>::computeWeight(p, interpPoint);
		};
		
		Complex takeFieldValue(Point interpPoint) {
			return UseNormSqrtField<PrecisionPolicy>::takeFieldValue(interpPoint);
		};
		
		/** Outward direction is not mandatory. */
		Complex chooseOutwardDir(Complex F,
								 Complex dir) {
			return UseNormSqrtField<PrecisionPolicy>::chooseOutwardDir(F, dir);
		};
		
		Complex angleFunction(Complex val) {
			return UseNormSqrtField<PrecisionPolicy>::angleFunction(val);
		};
		
		Field& getFieldSqrtRef() {
			return InterpolationPolicy<PrecisionPolicy>::getFieldSqrtRef();
		};
		
		void initWithPoint(cv::Point point) {
			RadialVectorConcordant<PrecisionPolicy>::initWithPoint(point);
		};
		
		void initWithRadialVector(Complex p) {
			RadialVectorConcordant<PrecisionPolicy>::initWithRadialVector(p);
		};
		
		Complex getReferenceDir() {
			return RadialVectorConcordant<PrecisionPolicy>::getReferenceDir();
		};
		
	public:
		Field operator<<(Field& in) {
			this->setField(in);
			this->perform();
			Field out;
			this->get(out);
			return out;
		};
	};
	
	template <typename PrecisionPolicy,
    template <typename> class InterpolationPolicy = AlglibInterpolation>
	class FieldAdjust : public FieldOperator<PrecisionPolicy>,
	public WeightTangent<PrecisionPolicy>,
	public UseActualField<PrecisionPolicy>,
	public WeightedSumCircle<PrecisionPolicy>,
	public RadialVectorConcordant<PrecisionPolicy>,
	public InterpolationPolicy<PrecisionPolicy> {
		
	private:
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Point Point;
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::Field Field;
		typedef typename PrecisionPolicy::Image Image;
		typedef typename PrecisionPolicy::FieldChoice FieldChoice;
		
	protected:
		Field& getFieldRef() {
			return InterpolationPolicy<PrecisionPolicy>::getFieldRef();
		};
		
		void computeLocalValue(cv::Point point,
							   Complex& newVal) {
			WeightedSumCircle<PrecisionPolicy>::computeLocalValue(point, newVal);
		};
		
		void additionalOperations(Field& newField){
			// Average the new field with the previous one
			if (this->strength.empty()) throw QtCustomException("FieldAdjust::additionalOperations - strength not set.");
			std::vector<Image> oldXY(2, Image()), newXY(2, Image());
			Image &oldX = oldXY[0], &oldY = oldXY[1], &newX = newXY[0], &newY = newXY[1];
			cv::split(fm::normalize<PrecisionPolicy>(this->field), oldXY);
			cv::split(fm::normalize<PrecisionPolicy>(newField), newXY);
			newX = (1-this->strength).mul(oldX) + this->strength.mul(newX);
			newY = (1-this->strength).mul(oldY) + this->strength.mul(newY);
			// Set the phase of currField as newField's one
			// Set the magnitude of currField as the largest between currField's and newField's
			Image newMagnitude, newPhase,
			oldMagnitude = fm::magnitude<PrecisionPolicy>(this->field);
			cv::cartToPolar(newX, newY, newMagnitude, newPhase);
			newMagnitude = max(oldMagnitude, newMagnitude);
			cv::polarToCart(newMagnitude, newPhase, newX, newY);
			cv::merge(std::vector<Image>({newX,newY}), newField);
		};
		
		Complex calc(FieldChoice choice,
					 Point p) {
			return InterpolationPolicy<PrecisionPolicy>::calc(choice, p);
		}
		
		/** Override the weight functions squaring their result. */
		Real computeWeight(Point p,
						   Point interpPoint) {
			return pow( abs(p.dot(this->takeFieldValue( interpPoint ))), 2.0);
		};
		
		Complex takeFieldValue(Point interpPoint) {
			return UseActualField<PrecisionPolicy>::takeFieldValue(interpPoint);
		};
		
		Complex chooseOutwardDir(Complex F,
								 Complex centralValue) {
//			return UseActualField<PrecisionPolicy>::chooseOutwardDir(F, centralValue);
			Real dprod = F.dot(centralValue);
			if(dprod != 0){
				F *= sgn(dprod);
			}
			Complex out;
			std::swap(F, out);
			return out;
		};
		
		Complex angleFunction(Complex val) {
			return UseActualField<PrecisionPolicy>::angleFunction(val);
		};
		
		Field& getFieldSqrtRef() {
			return InterpolationPolicy<PrecisionPolicy>::getFieldSqrtRef();
		};
		
		void initWithPoint(cv::Point point) {
			RadialVectorConcordant<PrecisionPolicy>::initWithPoint(point);
		};
		
		void initWithRadialVector(Complex p) {
			RadialVectorConcordant<PrecisionPolicy>::initWithRadialVector(p);
		};
		
		Complex getReferenceDir() {
			return RadialVectorConcordant<PrecisionPolicy>::getReferenceDir();
		};
		
	public:
		Field operator<<(Field& in) {
			this->setField(in);
			this->perform();
			Field out;
			this->get(out);
			return out;
		};
	};
	
	/** Smoothes the field iteratively. */
	template <typename PrecisionPolicy,
	template <typename> class WeightPolicy = WeightNone,
	template <typename> class FieldChoicePolicy = UseActualField,
	template <typename> class ShapePolicy = WeightedSumCircle,
    template <typename> class InterpolationPolicy = AlglibInterpolation>
	void FieldSmoothIter(typename PrecisionPolicy::Field field,
						 const typename PrecisionPolicy::Mask mask,
						 const typename PrecisionPolicy::Real ridgePeriod,
						 const typename PrecisionPolicy::Real smoothRadiusFactor,
						 const typename PrecisionPolicy::Int smoothNumSamples,
						 const typename PrecisionPolicy::Real strength,
						 const typename PrecisionPolicy::Real initialLevel,
						 const typename PrecisionPolicy::Int maxSteps,
						 const typename PrecisionPolicy::Real contractRadiusFactor,
						 const typename PrecisionPolicy::Real blurRadiusFactor,
						 const typename PrecisionPolicy::Real levelIncrement){
		// Pull types from template policy
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Mask Mask;
		typedef typename PrecisionPolicy::Field Field;
		// Adapt "...RadiusFactor" to the ridge period
		Real smoothRadius = smoothRadiusFactor*ridgePeriod;
		Int cr = (Int)std::round(contractRadiusFactor*ridgePeriod),
		blurRadius = (Int)std::round(blurRadiusFactor*ridgePeriod);
		// Initialize control variable for the loop
		Int k = 1;
		Real currentLevel = initialLevel;
		FieldSmooth<PrecisionPolicy, WeightNone, UseActualField> fieldSmoothProc;
		Field prevField;
		Mask diffMask, currMask;
		MorphSmoothErosion<PrecisionPolicy> smoothErode(cr);
		MorphSmoothDilation<PrecisionPolicy> smoothDilate(blurRadius);
		Field weightMatrix(field.size());
		
		// Initialize the field smoothing procedure
		fieldSmoothProc.setParameters(smoothRadius, smoothNumSamples);
		fieldSmoothProc.setStrength(strength, field.size());
		fieldSmoothProc.prepare(field.rows, field.cols);
		// Copy the mask to a local variable to allow for changes
		currMask = mask.clone();
		// Cycle until no more changes happen
		do {
			// Make prevField the current field
			cv::swap(field, prevField);
			// Perform the smoothing
			fieldSmoothProc.update(prevField);
			fieldSmoothProc.perform(); // the algorithm modifies the field values directly
			// Make field the header of the smoothed internal field
			fieldSmoothProc.getInternalRef(field);
			// Compute the difference mask
			diffMask = ( fm::magnitude<PrecisionPolicy>(prevField-field) > 2.0*currentLevel );
			// Contracts the current mask and intersect with the difference mask (prevents infinite loops)
			currMask = smoothDilate << ( (smoothErode << currMask) & diffMask );
			// Creates a weight image based on the current mask
			cv::mixChannels(smoothDilate.blur(currMask), weightMatrix, {0, 0, 0, 1});
			// Updates the field with the weight computed
			field = field.mul(weightMatrix) + prevField.mul(1-weightMatrix);
			// Updates the current level and step counter
			currentLevel += levelIncrement;
			k++;
		} while (cv::countNonZero(currMask) > 0 && (k < maxSteps || maxSteps < 0));
	};
}

#endif /* FieldOperators_hpp */
