#ifndef ShapePolicies_h
#define ShapePolicies_h

#include <opencv2/opencv.hpp>
#include <Headers/CommonOperations.hpp>

namespace fm {
	/** Policy that computes the local value as a weighted sum over a circle. */
	template <class PrecisionPolicy>
	class WeightedSumCircle {
	private:
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Point Point;
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::Field Field;
		typedef typename PrecisionPolicy::PointList PointList;
		
	public:
		virtual void setParameters(const Real newRadius,
								   const Int newNumSamples) {
			// Set execution parameters
			this->radius = newRadius;
			this->numSamples = newNumSamples;
			// Generate the list of points of the circle of given radius
			this->shape = RealCircleList<PrecisionPolicy>(this->radius, Point(0.0, 0.0), this->numSamples);
		};
		
	protected:
		PointList shape;
		Real radius;
		Int numSamples;
		
		virtual void initWithPoint(cv::Point point) = 0;
		
		virtual void initWithRadialVector(Complex p) = 0;
		
		virtual Complex getReferenceDir() = 0;
		
		virtual Real computeWeight(Point p,
								   Point interpPoint) = 0;
		
		virtual Complex takeFieldValue(Point interpPoint) = 0;
		
		virtual Complex chooseOutwardDir(Complex F,
										 Complex centralValue) = 0;
		
		virtual Complex angleFunction(Complex val) = 0;
		
		/** Compute the new local field value. */
		void computeLocalValue(cv::Point point,
							   Complex& newVal) {
			// Declarations
			Point interPoint;
			Real localWeight, totalWeight = 0.0;
			// Initialize the steering class with information about the current point
			initWithPoint(point);
			// Add the weighted field for each border point
			Point center(point.x, point.y);
			for (Point& p : this->shape) { // p is the vector from the center to the circle border
				// Initialize the steering class with information about the current radial vector
				initWithRadialVector(p);
				// Apply the functional weightFunction and sum to the new field value
				interPoint = p+center;
				localWeight = this->computeWeight(p/this->radius, interPoint);
				newVal += localWeight * this->chooseOutwardDir( this->takeFieldValue(interPoint), this->getReferenceDir() );
				totalWeight += localWeight;
			}
			if (totalWeight != 0.0) newVal /= totalWeight;
			
			// Eventually double the phase (if halved before)
			newVal = this->angleFunction(newVal);
		}
		
	};
}

#endif /* ShapePolicies_h */
