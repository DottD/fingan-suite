#ifndef BilinearInterpolation_h
#define BilinearInterpolation_h

#include <opencv2/opencv.hpp>

namespace fm {
	/** Interpolation facilities for cv::Mat_<T> using alglib.
	 */
	template <class PrecisionPolicy>
	class BilinearInterpolation {
	private:
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Point Point;
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::Field Field;
		typedef typename PrecisionPolicy::FieldChoice FieldChoice;
		
		Field DiField, DjField, DjDiField;
		Field DiSqrtField, DjSqrtField, DjDiSqrtField;
		Field DiNormSqrtField, DjNormSqrtField, DjDiNormSqrtField;
		
	protected:
		Field field, // a copy of the initial field
		fieldSqrt, fieldNormSqrt; // matrix with the field sqrt and normalized sqrt
		
		Field& getFieldRef() {return std::ref(field);};
		
		Field& getFieldSqrtRef() {return std::ref(fieldSqrt);};
		
		Field& getFieldNormSqrtRef() {return std::ref(fieldNormSqrt);};
		
	public:
		/** Prepare the interpolation instance.
		 Allocates the memory needed to manage a field of given dimensions.
		 @param[in] rows Number of rows that the field will contain.
		 @param[in] rows Number of cols that the field will contain.
		 */
		void prepare(Int rows,
					 Int cols) {
			if (rows > 0 && cols > 0) {
				field.create(rows, cols);
				DiField.create(rows, cols);
				DjField.create(rows, cols);
				DjDiField.create(rows, cols);
				
				fieldSqrt.create(rows, cols);
				DiSqrtField.create(rows, cols);
				DjSqrtField.create(rows, cols);
				DjDiSqrtField.create(rows, cols);
				
				fieldNormSqrt.create(rows, cols);
				DiNormSqrtField.create(rows, cols);
				DjNormSqrtField.create(rows, cols);
				DjDiNormSqrtField.create(rows, cols);
			}
		}
		
		/** Update the interpolation instance with a new field.
		 Deep copy is also performed due to limitations of ALGLIB library,
		 furthermore this method guarantees that the this->field is continuous.
		 @param[in] field Field to be used to update the instance.
		 */
		void update(Field newField) {
			// Check if the dimensions are correct
			if (!newField.empty() && field.size() == newField.size()) {
				// Reference to size
				const Int& rows = field.rows;
				const Int& cols = field.cols;
				// Copy the given field
				field = newField.clone();
				// Set up intepolation
				// Compute the vertical finite differences on field
				DiField.rowRange(0,rows-1) = field.rowRange(1,rows) - field.rowRange(0,rows-1);
				DiField.row(rows-1) = 0.0; // since used in additions, it does not modify the result
				// Compute the horizontal finite differences on field
				DjField.colRange(0,cols-1) = field.colRange(1,cols) - field.colRange(0,cols-1);
				DjField.col(cols-1) = 0.0; // since used in additions, it does not modify the result
				// Compute the vertical finite differences on DiA
				DjDiField.colRange(0,cols-1) = DiField.colRange(1,cols) - DiField.colRange(0,cols-1);
				DjDiField.col(cols-1) = 0.0; // since used in additions, it does not modify the result
				
				// Compute the sqrt and the normalized sqrt version of the input field
				fm::sqrtAndNormSqrt<PrecisionPolicy>(field, fieldSqrt, fieldNormSqrt);
				// Set up intepolation
				// Compute the vertical finite differences on fieldSqrt
				DiSqrtField.rowRange(0,rows-1) = fieldSqrt.rowRange(1,rows) - fieldSqrt.rowRange(0,rows-1);
				DiSqrtField.row(rows-1) = 0.0; // since used in additions, it does not modify the result
				// Compute the horizontal finite differences on fieldSqrt
				DjSqrtField.colRange(0,cols-1) = fieldSqrt.colRange(1,cols) - fieldSqrt.colRange(0,cols-1);
				DjSqrtField.col(cols-1) = 0.0; // since used in additions, it does not modify the result
				// Compute the vertical finite differences on DiA
				DjDiSqrtField.colRange(0,cols-1) = DiField.colRange(1,cols) - DiField.colRange(0,cols-1);
				DjDiSqrtField.col(cols-1) = 0.0; // since used in additions, it does not modify the result
				// Set up intepolation
				// Compute the vertical finite differences on fieldNormSqrt
				DiNormSqrtField.rowRange(0,rows-1) = fieldNormSqrt.rowRange(1,rows) - fieldNormSqrt.rowRange(0,rows-1);
				DiNormSqrtField.row(rows-1) = 0.0; // since used in additions, it does not modify the result
				// Compute the horizontal finite differences on fieldNormSqrt
				DjNormSqrtField.colRange(0,cols-1) = fieldNormSqrt.colRange(1,cols) - fieldNormSqrt.colRange(0,cols-1);
				DjNormSqrtField.col(cols-1) = 0.0; // since used in additions, it does not modify the result
				// Compute the vertical finite differences on DiA
				DjDiNormSqrtField.colRange(0,cols-1) = DiField.colRange(1,cols) - DiField.colRange(0,cols-1);
				DjDiNormSqrtField.col(cols-1) = 0.0; // since used in additions, it does not modify the result
			} else throw QtCustomException("BilinearInterpolation::update - incorrect field dimensions");
		}
		
		/** Setup the interpolation instance with the given field.
		 Deep copy is always performed due to limitations of ALGLIB library.
		 The method sets up the sqrt and the normSqrt versions of the given field.
		 @param[in] field Field to be used in further computations.
		 */
		void setField(const Field newField){
			prepare(newField.rows, newField.cols);
			
			// Set up interpolants
			update(newField);
		};
		
		Complex calc(FieldChoice choice,
					 Point p/*given in xy coordinate with the origin in the upper-left corner*/) {
			// Define i and j as the integer part of pi and pj respectively (also handle extrapolation)
			Real idx[4]; // i, j, di, dj
			
			if (p[1] < 0) idx[0] = 0;
			else if (p[1] > field.rows-1) idx[0] = field.rows-1;
			else idx[0] = float(int(p[1]));//std::floor(p[1]);
			
			if (p[0] < 0) idx[1] = 0;
			else if (p[0] > field.cols-1) idx[1] = field.cols-1;
			else idx[1] = float(int(p[0]));//std::floor(p[0]);
			
			idx[2] = p[1]-idx[0];
			idx[3] = p[0]-idx[1];
			// Compute the interpolated value
			int intIdx[2] = {Int(idx[0]), Int(idx[1])};
			// Chose the field to use for interpolation
			switch(choice){
				case FieldChoice::ActualField: {
					const Complex& Di_ = DiField(intIdx[0], intIdx[1]);
					const Complex& Dj_ = DjField(intIdx[0], intIdx[1]);
					const Complex& DjDi_ = DjDiField(intIdx[0], intIdx[1]);
					const Complex& f_ = field(intIdx[0], intIdx[1]);
					return f_ + idx[2] * Di_ + idx[3] * Dj_ + idx[2] * idx[3] * DjDi_;
				}
				case FieldChoice::SqrtField: {
					const Complex& Di_ = DiSqrtField(intIdx[0], intIdx[1]);
					const Complex& Dj_ = DjSqrtField(intIdx[0], intIdx[1]);
					const Complex& DjDi_ = DjDiSqrtField(intIdx[0], intIdx[1]);
					const Complex& f_ = fieldSqrt(intIdx[0], intIdx[1]);
					return f_ + idx[2] * Di_ + idx[3] * Dj_ + idx[2] * idx[3] * DjDi_;
				}
				case FieldChoice::NormSqrtField: {
					const Complex& Di_ = DiNormSqrtField(intIdx[0], intIdx[1]);
					const Complex& Dj_ = DjNormSqrtField(intIdx[0], intIdx[1]);
					const Complex& DjDi_ = DjDiNormSqrtField(intIdx[0], intIdx[1]);
					const Complex& f_ = fieldNormSqrt(intIdx[0], intIdx[1]);
					return f_ + idx[2] * Di_ + idx[3] * Dj_ + idx[2] * idx[3] * DjDi_;
				}
                        default:
                            throw QtCustomException("Incorrect field choice");
			}
		};
	};
}

#endif /* BilinearInterpolation_h */
