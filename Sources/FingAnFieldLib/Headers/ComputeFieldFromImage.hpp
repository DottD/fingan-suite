#ifndef ComputeFieldFromImage_hpp
#define ComputeFieldFromImage_hpp

#include <Headers/QtCustomException.hpp>
#include <Headers/PrecisionPolicies.hpp>
#include <Headers/MorphologyOperations.hpp>
#include <Headers/AlglibInterpolation.hpp>
#include <Headers/WeightPolicies.hpp>
#include <Headers/FieldChoicePolicies.hpp>
#include <Headers/ShapePolicies.hpp>
#include <Headers/FieldOperators.hpp>
#include <Headers/FieldOperations.hpp>
#include <Headers/ComputeFrequency.hpp>
#include <Headers/ComputeOrientation.hpp>

#include "../../DebugLib/debugLib.hpp"

namespace fm {
	
	template <typename PrecisionPolicy>
	typename PrecisionPolicy::Field ComputeFieldFromImage(const typename PrecisionPolicy::Image image,
														  const typename PrecisionPolicy::Mask mask,
														  const typename PrecisionPolicy::Real period,
														  typename PrecisionPolicy::Int numSamplesPerCircle,
														  typename PrecisionPolicy::Real maskContractionRadius,
														  typename PrecisionPolicy::Real firstFieldRadius,
														  typename PrecisionPolicy::Real firstFieldSmoothRadius,
														  const typename PrecisionPolicy::Real firstFieldSmoothStrength,
														  typename PrecisionPolicy::Real firstDriftRadiusFactor,
														  const typename PrecisionPolicy::Real firstLoopdeltaThreshold,
														  typename PrecisionPolicy::Real firstMaskContractionRadius,
														  typename PrecisionPolicy::Real firstMaskBlurRadius,
														  typename PrecisionPolicy::Real secondFieldRadius,
														  typename PrecisionPolicy::Real secondFieldAdjustRadius,
														  const typename PrecisionPolicy::Real secondFieldAdjustMaskRescale,
														  typename PrecisionPolicy::Real secondFieldAdjustRadius2,
														  const typename PrecisionPolicy::Real secondFieldAdjustMaskRescale2,
														  typename PrecisionPolicy::Real secondDiffBlurRadius,
														  const typename PrecisionPolicy::Real secondDiffThreshold,
														  typename PrecisionPolicy::Real secondDiffMinMaxCentroidDistRadius,
														  typename PrecisionPolicy::Real secondDiffDilationRadius,
														  typename PrecisionPolicy::Real thirdDriftRadius,
														  typename PrecisionPolicy::Real thirdLdBlurRadius,
														  const typename PrecisionPolicy::Real thirdLoopdeltaThreshold,
														  typename PrecisionPolicy::Real thirdLdMinMaxCentroidDistRadius,
														  typename PrecisionPolicy::Real diffDilation2Radius,
														  typename PrecisionPolicy::Real diffDilation3Radius,
														  const typename PrecisionPolicy::Real iterSmoothRadiusFactor,
														  const typename PrecisionPolicy::Real iterSmoothStrength,
														  const typename PrecisionPolicy::Real iterSmoothInitialLevel,
														  const typename PrecisionPolicy::Real iterSmoothMaxSteps,
														  const typename PrecisionPolicy::Real iterSmoothContractionRadiusFactor,
														  const typename PrecisionPolicy::Real iterSmoothBlurRadiusFactor,
														  const typename PrecisionPolicy::Real iterSmoothLevelIncrement) {
		// Pull type definitions from precision policy
		PrecisionPolicy ppObj;
		typedef typename PrecisionPolicy::Image Image;
		typedef typename PrecisionPolicy::Field Field;
		typedef typename PrecisionPolicy::Mask Mask;
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Int Int;
		// Initializations
		Field cfield(image.size());
		Image fieldDT(image.size()), fieldDN(image.size()), cfieldDT(image.size()), cfieldDN(image.size());
		FieldDrift<PrecisionPolicy, WeightTangent> driftTangent;
		FieldDrift<PrecisionPolicy, WeightNormal> driftNormal;
		Image diffImage(image.size());
		
		// Rescale "...Radius" according to period
		firstFieldRadius = std::round(firstFieldRadius * period);
		firstFieldSmoothRadius = std::round(firstFieldSmoothRadius * period);
		firstDriftRadiusFactor = std::round(firstDriftRadiusFactor * period);
		firstMaskContractionRadius = std::round(firstMaskContractionRadius * period);
		firstMaskBlurRadius = std::round(firstMaskBlurRadius * period);
		secondFieldRadius = std::round(secondFieldRadius * period);
		secondFieldAdjustRadius = std::round(secondFieldAdjustRadius * period);
		secondFieldAdjustRadius2 = std::round(secondFieldAdjustRadius2 * period);
		secondDiffBlurRadius = std::round(secondDiffBlurRadius * period);
		secondDiffMinMaxCentroidDistRadius = std::round(secondDiffMinMaxCentroidDistRadius * period);
		secondDiffDilationRadius = std::round(secondDiffDilationRadius * period);
		thirdDriftRadius = std::round(thirdDriftRadius * period);
		thirdLdBlurRadius = std::round(thirdLdBlurRadius * period);
		maskContractionRadius = std::round(maskContractionRadius * period);
		thirdLdMinMaxCentroidDistRadius = std::round(thirdLdMinMaxCentroidDistRadius * period);
		diffDilation2Radius = std::round(diffDilation2Radius * period);
		diffDilation3Radius = std::round(diffDilation3Radius * period);
		
		Int secondDiffBlurDiameter = 2*Int(secondDiffBlurRadius)+1,
		ldBlurDiameter = 2*thirdLdBlurRadius+1,
		firstMaskBlurDiameter = 2*firstMaskBlurRadius+1;
		
		/* Preliminary operations */
		// Erode the initial mask, singularities won't be on the mask border.
		// Leaving it would yield to false singularities on the border.
		Mask initialMask = MorphSmoothErosion<PrecisionPolicy>(maskContractionRadius) << mask;
		//******************************************************************************************
		// Create a fuzzy mask that hides deltas
		//******************************************************************************************
		Field field;
		Image fuzzyLoopDelta; // fuzzy mask without loops
		{
			// Compute the orientation field with the first radius
			// Use the original mask: when computing the fuzzyNoLoops mask, there is always a
			// border that we should remove, due to changes done by the drift operator.
			// The intersection with the contractedMask will be done later.
			int K = 8;
			field = Field::zeros(image.size());
			for (int k = 1; k <= K; k++){
				Field field_k;
				if (!ComputeOrientation<PrecisionPolicy>(field_k, image, mask, Int(k*period), 1.0, 1.0, 2.0, 0.85, 2.0, 36, 1, 4, 0.5))
					throw QtCustomException("ComputeFieldFromImage: Cannot compute orientation");
				double weight = std::exp(-std::pow(k-K, 2)/(K*K/2));
				qDebug() << weight; dbgHelperLog(image, field_k, (QString("field_")+QLocale().toString(k)+QString(".png")).toStdString().c_str(), 5, 2, 2, 4);
				field += weight * fm::normalize<PrecisionPolicy>(field_k);
			}
			field = fm::normalize<PrecisionPolicy>(field);
			dbgHelperLog(image, field, "field_compound.png", 5, 2, 2, 4);
			
			if(true){
				FieldDrift<PrecisionPolicy, WeightTangent> DT;
				DT.setParameters(firstFieldSmoothRadius, numSamplesPerCircle);
				FieldDrift<PrecisionPolicy, WeightNormal> DN;
				DN.setParameters(firstFieldSmoothRadius, numSamplesPerCircle);
				FieldAdjust<PrecisionPolicy> A;
				A.setParameters(firstFieldSmoothRadius, numSamplesPerCircle);
				A.setStrength(Image::ones(field.size()));
				Field fDT = DT << field;
				Field fDN = DN << field;
				Field fA = A << field;
				FieldSmooth<PrecisionPolicy, WeightNone, UseSqrtField> S1;
				S1.setParameters(firstFieldSmoothRadius, numSamplesPerCircle);
				S1.setStrength(1, field.size());
				Field fS1 = S1 << field;
				FieldSmooth<PrecisionPolicy, WeightTangent, UseSqrtField> S1T;
				S1T.setParameters(firstFieldSmoothRadius, numSamplesPerCircle);
				S1T.setStrength(1, field.size());
				Field fS1T = S1T << field;
				FieldSmooth<PrecisionPolicy, WeightNormal, UseSqrtField> S1N;
				S1N.setParameters(firstFieldSmoothRadius, numSamplesPerCircle);
				S1N.setStrength(1, field.size());
				Field fS1N = S1N << field;
				FieldSmooth<PrecisionPolicy, WeightNone, UseActualField> S2;
				S2.setParameters(firstFieldSmoothRadius, numSamplesPerCircle);
				S2.setStrength(1, field.size());
				Field fS2 = S2 << field;
				FieldSmooth<PrecisionPolicy, WeightTangent, UseActualField> S2T;
				S2T.setParameters(firstFieldSmoothRadius, numSamplesPerCircle);
				S2T.setStrength(1, field.size());
				Field fS2T = S2T << field;
				FieldSmooth<PrecisionPolicy, WeightNormal, UseActualField> S2N;
				S2N.setParameters(firstFieldSmoothRadius, numSamplesPerCircle);
				S2N.setStrength(1, field.size());
				Field fS2N = S2N << field;
				std::cout << "out of scope" << std::endl;
			}
			
			// Perform the smoothing of the original field
			Field sfield;
			FieldSmooth<PrecisionPolicy, WeightNone, UseActualField> smooth;
			smooth.setParameters(firstFieldSmoothRadius, numSamplesPerCircle);
			smooth.setStrength(firstFieldSmoothStrength, field.size());
			smooth.setField(field);
			smooth.perform();
			smooth.getInternalRef(sfield);
			
			// Compute the drift (tangent and normal) of the field to highlight loops
			driftTangent.setParameters(firstDriftRadiusFactor, numSamplesPerCircle);
			fieldDT = magnitude<PrecisionPolicy>( driftTangent << sfield );
			driftNormal.setParameters(firstDriftRadiusFactor, numSamplesPerCircle);
			fieldDN = magnitude<PrecisionPolicy>( driftNormal << sfield );
			
			// Create the conjugate field to exploit the dual nature of cores and deltas
			cfield = fm::conj<PrecisionPolicy>(sfield);
			// Compute the drift (tangent and normal) of the field conjugate to highlight deltas
			cfieldDT = magnitude<PrecisionPolicy>( driftTangent << cfield );
			cfieldDN = magnitude<PrecisionPolicy>( driftNormal << cfield );
			
			// Sum the first two drifts, to get high values on loops, and subtract the latter
			// to get very low values on deltas
			fuzzyLoopDelta = fieldDT + fieldDN - cfieldDT - cfieldDN;
			// Create a no-loop mask (instead of dividing by 2 the loopdelta, multiply the threshold)
			Mask loopDelta = abs(fuzzyLoopDelta) > 2.0 * firstLoopdeltaThreshold;
			// Combine the initial mask with the one just created, erode the result
			// Perhaps, the intersection with the initial mask is irrelevant, since it has been used to compute the orientation
			loopDelta = MorphDistDilation<PrecisionPolicy>(firstMaskContractionRadius) << Mask(loopDelta & initialMask);
			// Perform a gaussian blurring on the mask
			fuzzyLoopDelta.setTo(0.0);
			fuzzyLoopDelta.setTo(1.0, loopDelta);
			cv::GaussianBlur(fuzzyLoopDelta, fuzzyLoopDelta, cv::Size(firstMaskBlurDiameter, firstMaskBlurDiameter),
							 Real(firstMaskBlurRadius)/2.0, Real(firstMaskBlurRadius)/2.0, cv::BORDER_REPLICATE);
		}
		
		//******************************************************************************************
		// Create a mask with loops
		//******************************************************************************************
		Mask diffMask;
		{
			// Compute the orientation field with a different radius
			Field field2;
			if (!ComputeOrientation<PrecisionPolicy>(field2, image, mask, Int(secondFieldRadius), 1.0, 1.0, 2.0, 0.85, 2.0, 36, 1, 4, 0.5))
				throw QtCustomException("ComputeFieldFromImage: Cannot compute orientation");
			// Field adjustment alters deltas, hence we use a fuzzy mask that hides loops to locate deltas
			FieldAdjust<PrecisionPolicy> adjust;
			adjust.setParameters(secondFieldAdjustRadius, numSamplesPerCircle);
			adjust.setStrength(secondFieldAdjustMaskRescale*fuzzyLoopDelta);
			field = adjust << field;
			
			adjust.setParameters(secondFieldAdjustRadius2, numSamplesPerCircle);
			adjust.setStrength(secondFieldAdjustMaskRescale2*fuzzyLoopDelta);
			field2 = adjust << field2;
			// Compute the differences with the previous field
			diffImage = fm::magnitude<PrecisionPolicy>(fm::normalize<PrecisionPolicy>(field)-fm::normalize<PrecisionPolicy>(field2));
			// Combine the difference image with the initial mask
			diffImage.setTo(0.0, ~initialMask);
			// Blur the difference image
			cv::GaussianBlur(diffImage, diffImage, cv::Size(secondDiffBlurDiameter, secondDiffBlurDiameter),
							 Real(secondDiffBlurRadius)/2.0, Real(secondDiffBlurRadius)/2.0, cv::BORDER_REPLICATE);
			// Apply a threshold on the difference image
			diffMask = ( diffImage > 2.0*secondDiffThreshold );
			// Suppress too small components
			diffMask = MorphFilterMaxCentrDist<PrecisionPolicy>(secondDiffMinMaxCentroidDistRadius) << diffMask;
			// Dilate the difference mask (in Mathematica MorphSmoothDilation was used)
			diffMask = MorphDistDilation<PrecisionPolicy>(Int(secondDiffDilationRadius+diffDilation2Radius)) << diffMask;
		}
		
		//******************************************************************************************
		// Create a more precise mask with highlighted loops and deltas
		//******************************************************************************************
		Mask loopDelta;
		{
			// Compute the drift (tangent and normal) of the field to highlight loops
			driftTangent.setParameters(thirdDriftRadius, numSamplesPerCircle);
			fieldDT = magnitude<PrecisionPolicy>( driftTangent << field );
			driftNormal.setParameters(thirdDriftRadius, numSamplesPerCircle);
			fieldDN = magnitude<PrecisionPolicy>( driftNormal << field );
			
			// Create the conjugate field to exploit the dual nature of cores and deltas
			cfield = fm::conj<PrecisionPolicy>(field);
			// Compute the drift (tangent and normal) of the field conjugate to highlight deltas
			cfieldDT = magnitude<PrecisionPolicy>( driftTangent << cfield );
			cfieldDN = magnitude<PrecisionPolicy>( driftNormal << cfield );
			
			// Sum the first two drifts, to get high values on loops, and subtract the latter
			// to get very low values on deltas
			diffImage = fieldDT + fieldDN - cfieldDT - cfieldDN;
			// Blur the field drift difference image
			cv::GaussianBlur(diffImage, diffImage, cv::Size(ldBlurDiameter, ldBlurDiameter),
							 Real(thirdLdBlurRadius)/2.0, Real(thirdLdBlurRadius)/2.0, cv::BORDER_REPLICATE);
			// Create a no-delta mask (instead of dividing by 2 the diffImage, multiply the threshold)
			loopDelta = abs(diffImage) > 2.0*thirdLoopdeltaThreshold;
			
			// Force the loops to be inside the mask
			loopDelta &= initialMask;
			// Set to background the connected component with maximum distance from centroid too low
			loopDelta = MorphFilterMaxCentrDist<PrecisionPolicy>(thirdLdMinMaxCentroidDistRadius) << loopDelta;
		}
		
		//******************************************************************************************
		// Join the informations got from the two fields
		//******************************************************************************************
		// Gives true only when diffMask is true and loopDelta is false
		diffMask &= (diffMask ^ loopDelta);
		// Dilate to be sure to cover singularities
		diffMask = MorphSmoothDilation<PrecisionPolicy>(Int(diffDilation3Radius)) << diffMask;
		// Gives the complementary, namely a mask with surely no deltas
		diffMask = ~diffMask;
		
		//******************************************************************************************
		// Iteratively smooth the field
		//******************************************************************************************
		FieldSmoothIter<PrecisionPolicy>(field, diffMask, period,
										 iterSmoothRadiusFactor,
										 numSamplesPerCircle,
										 iterSmoothStrength,
										 iterSmoothInitialLevel,
										 Int(iterSmoothMaxSteps),
										 iterSmoothContractionRadiusFactor,
										 iterSmoothBlurRadiusFactor,
										 iterSmoothLevelIncrement);
		
		return field;
	}
	
}

#endif /* ComputeFieldFromImage_hpp */
