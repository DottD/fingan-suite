#ifndef FieldChoicePolicies_h
#define FieldChoicePolicies_h

#include <opencv2/opencv.hpp>

namespace fm {
	template <typename PrecisionPolicy>
	class UseActualField {
	public:
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Point Point;
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::FieldChoice FieldChoice;
		
	protected:
		virtual Complex calc(FieldChoice choice,
							 Point p) = 0;
		
		virtual Complex angleFunction(Complex val) {
			Complex out;
			std::swap(val, out);
			return out;
		}
		
		virtual Complex takeFieldValue(Point interpPoint) {
			return this->calc( FieldChoice::ActualField, interpPoint);
			
		}
		
		virtual Complex chooseOutwardDir(Complex F,
										 Complex) {
			Complex out;
			std::swap(F, out);
			return out;
		}
		
		virtual Complex chooseInwardDir(Complex F,
										Complex) {
			Complex out;
			std::swap(F, out);
			return out;
		}
	};
	
	template <typename PrecisionPolicy>
	class UseSqrtField {
	public:
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Point Point;
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::FieldChoice FieldChoice;
		
	protected:
		virtual Complex calc(FieldChoice choice,
							 Point p) = 0;
		
		Complex angleFunction(Complex val) {
			std::complex<Real> z(val[0], val[1]);
			z *= z;
			val[0] = z.real();
			val[1] = z.imag(); // returning the argument now would prevent RVO optimizations
			Complex out; // default construction (fast)
			std::swap(val, out); // swap values (fast)
			return out; // return the temporary w, this way through optimization the code can be faster
		}
		
		virtual Complex takeFieldValue(Point interpPoint) {
			return this->calc( FieldChoice::SqrtField, interpPoint);
			
		}
		
		virtual Complex chooseOutwardDir(Complex F,
										 Complex centralValue) {
			Real dprod = F.dot(centralValue);
			if(dprod != 0){
				F *= sgn(dprod);
			}
			Complex out;
			std::swap(F, out);
			return out;
		}
		
		virtual Complex chooseInwardDir(Complex F,
										Complex centralValue) {
			Real dprod = F.dot(centralValue);
			if(dprod != 0){
				F *= -sgn(dprod);
			}
			Complex out;
			std::swap(F, out);
			return out;
		}
	};
	
	template <typename PrecisionPolicy>
	class UseNormSqrtField {
	public:
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Point Point;
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::FieldChoice FieldChoice;
		
	protected:
		virtual Complex calc(FieldChoice choice,
							 Point p) = 0;
		
		Complex angleFunction(Complex val) {
			std::complex<Real> z(val[0], val[1]);
			z *= z;
			val[0] = z.real();
			val[1] = z.imag(); // returning the argument now would prevent RVO optimizations
			Complex out; // default construction (fast)
			std::swap(val, out); // swap values (fast)
			return out; // return the temporary w, this way through optimization the code can be faster
		}
		
		virtual Complex takeFieldValue(Point interpPoint) {
			return this->calc( FieldChoice::NormSqrtField, interpPoint);
		}
		
		virtual Complex chooseOutwardDir(Complex F,
										 Complex centralValue) {
			Real dprod = F.dot(centralValue);
			if(dprod != 0){
				F *= sgn(dprod);
			}
			Complex out;
			std::swap(F, out);
			return out;
		}
		
		virtual Complex chooseInwardDir(Complex F,
										Complex centralValue) {
			Real dprod = F.dot(centralValue);
			if(dprod != 0){
				F *= -sgn(dprod);
			}
			Complex out;
			std::swap(F, out);
			return out;
		}
	};
}

#endif /* FieldChoicePolicies_h */
