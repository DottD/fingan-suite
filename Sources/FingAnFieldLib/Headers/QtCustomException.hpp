#ifndef QTCUSTOMEXCEPTION_H
#define QTCUSTOMEXCEPTION_H

#include <QException>
#include <QString>
#include <QDebug>

class QtCustomException : public QException {
private:
    QString msg;
public:
    QtCustomException(QString msg) { this->msg = msg; }
    void raise() const { qDebug() << "** Runtime custom exception: " << this->msg; throw *this; }
    QtCustomException *clone() const { return new QtCustomException(*this); }
	QString message() const { return this->msg; };
};
#endif // QTCUSTOMEXCEPTION_H
