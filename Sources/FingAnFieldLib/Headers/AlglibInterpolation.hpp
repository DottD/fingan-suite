#ifndef AlglibInterpolation_h
#define AlglibInterpolation_h

#include <alglib/interpolation.h>
#include <armadillo>
#include <opencv2/opencv.hpp>
#include <Headers/FieldOperations.hpp>

namespace fm {
	/** Interpolation facilities for cv::Mat_<T> using alglib.
	 */
	template <class PrecisionPolicy>
	class AlglibInterpolation {
	private:
		alglib::real_1d_array v, rowSpan, colSpan, fieldVal, fieldSqrtVal, fieldNormSqrtVal;
		alglib::spline2dinterpolant fieldInterp, fieldSqrtInterp, fieldSqrtNormInterp;
		
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Point Point;
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::Field Field;
		typedef typename PrecisionPolicy::FieldChoice FieldChoice;
		
	protected:
		Field field, // a copy of the initial field
		fieldSqrt, fieldNormSqrt; // matrix with the field sqrt and normalized sqrt
		
		Field& getFieldRef() {return std::ref(field);};
		
		Field& getFieldSqrtRef() {return std::ref(fieldSqrt);};
		
		Field& getFieldNormSqrtRef() {return std::ref(fieldNormSqrt);};
		
	public:
		/** Prepare the interpolation instance.
		 Allocates the memory needed to manage a field of given dimensions.
		 @param[in] rows Number of rows that the field will contain.
		 @param[in] rows Number of cols that the field will contain.
		 */
		void prepare(Int rows,
					 Int cols) {
			// Set up interpolation dimensions
			arma::Col<Real> arma_rowSpan = arma::regspace(0, rows-1),
			arma_colSpan = arma::regspace(0, cols-1);
			rowSpan.setcontent(rows, arma_rowSpan.memptr());
			colSpan.setcontent(cols, arma_colSpan.memptr());
			// Allocate memory for the field matrix
			fieldVal.setlength(rows*cols*field.channels());
			fieldSqrtVal.setlength(rows*cols*field.channels());
			fieldNormSqrtVal.setlength(rows*cols*field.channels());
			// Create Field header on top of the corresponding preallocated alglib array
			this->field = Field(rows, cols, (Complex*)fieldVal.getcontent());
			this->fieldSqrt = Field(rows, cols, (Complex*)fieldSqrtVal.getcontent());
			this->fieldNormSqrt = Field(rows, cols, (Complex*)fieldNormSqrtVal.getcontent());
		}
		
		/** Update the interpolation instance with a new field.
		 Deep copy is also performed due to limitations of ALGLIB library,
		 furthermore this method guarantees that the this->field is continuous.
		 @param[in] field Field to be used to update the instance.
		 */
		void update(Field newField) {
			// Check if the dimensions are correct
			if (rowSpan.length() == newField.rows && colSpan.length() == newField.cols && newField.isContinuous()) {
				// Assign the field (the property field will be continuous)
				memcpy(fieldVal.getcontent()/*dest*/, newField.ptr()/*src*/, newField.total()*newField.elemSize());
				// Compute the sqrt and the normalized sqrt version of the input field (do not perform reallocation - good)
				fm::sqrtAndNormSqrt<PrecisionPolicy>(field, fieldSqrt, fieldNormSqrt);
				
				// Set up intepolation
				alglib::spline2dbuildbilinearv(colSpan, colSpan.length(), rowSpan, rowSpan.length(), fieldVal, 2/*depth*/, fieldInterp);
				alglib::spline2dbuildbilinearv(colSpan, colSpan.length(), rowSpan, rowSpan.length(), fieldSqrtVal, 2/*depth*/, fieldSqrtInterp);
				alglib::spline2dbuildbilinearv(colSpan, colSpan.length(), rowSpan, rowSpan.length(), fieldNormSqrtVal, 2/*depth*/, fieldSqrtNormInterp);
			} else throw QtCustomException("AlglibInterpolation::update - incorrect field dimensions or field not continuous");
		}
		
		/** Setup the interpolation instance with the given field.
		 Deep copy is always performed due to limitations of ALGLIB library.
		 The method sets up the sqrt and the normSqrt versions of the given field.
		 @param[in] field Field to be used in further computations.
		 */
		void setField(const Field newField){
			prepare(newField.rows, newField.cols);
			
			// Set up interpolants
			update(newField);
		};
		
		Complex calc(FieldChoice choice,
					 Point p) {
			// Return field value at the given point
			switch(choice){
				case FieldChoice::ActualField:
					alglib::spline2dcalcvbuf(fieldInterp, p[0], p[1], v);
					break;
				case FieldChoice::SqrtField:
					alglib::spline2dcalcvbuf(fieldSqrtInterp, p[0], p[1], v);
					break;
				case FieldChoice::NormSqrtField:
					alglib::spline2dcalcvbuf(fieldSqrtNormInterp, p[0], p[1], v);
					break;
			}
			return Complex(v[0], v[1]);
		};
	};
}

#endif /* AlglibInterpolation_h */
