#ifndef ComputeFrequency_h
#define ComputeFrequency_h

#include <opencv2/opencv.hpp>
#include <alglib/interpolation.h>
#include <armadillo>

#include <Headers/myMathFunc.hpp>
#include <Headers/PrecisionPolicies.hpp>
#include <Headers/FieldOperations.hpp>
#include <Headers/QtCustomException.hpp>

namespace fm {
	
	/** Estimate the frequency between two consecutive ridges.
	 This function selects a group of pixels with high field magnitude, assuming this
	 would mean high orientation accuracy. For each of these points, the FFT of the image
	 values along the segment orthogonal to the local orientation is computed;
	 the first peak in the Fourier spectrum is considered as ridge frequency.
	 @param[out] meanFreq Average ridge frequency.
	 @param[out] varFreq Standard deviation of ridge frequency.
	 @param[in] image Image containing the fingerprint.
	 @param[in] field Orientation field previously computed.
	 @param[in] mask Mask used to do the estimation only over selected regions.
	 @param[in] radius Length of the segments in pixels.
	 @param[in] step Minimum distance allowed for to-be-tested pixels.
	 @param[in] numSamples Number of samples per segment.
	 @param[in] minNormQuantile Order of first quantile where points will be selected.
	 @param[in] gaussFilterRad Radius of the gaussian filter kernel for in-segment smoothing.
	 @param[in] minValFreq Minimum allowed frequency (negative to disable).
	 @param[in] maxValFreq Maximum allowed frequency (negative to disable).
	 @param[in] leftQuantile Order of the first quantile of frequency to exclude from results.
	 @param[in] rightQuantile Order of the last quantile of frequency to exclude from results.
	 @return True if everything is fine, false on errors: in the latter case a default value for meanFreq and varFreq is provided.
	 */
	template <typename PrecisionPolicy>
	bool ComputeFrequency(typename PrecisionPolicy::Real& meanFreq,
					   typename PrecisionPolicy::Real& varFreq,
					   const typename PrecisionPolicy::Image image,
					   const typename PrecisionPolicy::Field field,// square field
					   const typename PrecisionPolicy::Mask mask,
					   const typename PrecisionPolicy::Int radius, // segment radius
					   const typename PrecisionPolicy::Int step, // sample distance
					   const typename PrecisionPolicy::Int numSamples, // number of samples per segment
					   const typename PrecisionPolicy::Real minNormQuantile, // min allowed norm for each sample
					   const typename PrecisionPolicy::Int gaussFilterRad, // gaussian filter radius
					   const typename PrecisionPolicy::Real minValFreq, // min allowed frequency
					   const typename PrecisionPolicy::Real maxValFreq, // max allowed frequency
					   const typename PrecisionPolicy::Real leftQuantile, // left extremum (with respect to the radius)
					   const typename PrecisionPolicy::Real rightQuantile) { // right extremum (with respect to the radius)
		// Pull PrecisionPolicy types
		PrecisionPolicy PPobj;
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Mask Mask;
		typedef typename PrecisionPolicy::Image Image;
		typedef typename PrecisionPolicy::Field Field;
		// Check input variables
		if (field.empty() || image.empty() || mask.empty() ||
			(minNormQuantile < 0 || minNormQuantile > 1) ||
			(field.cols <= 2*radius || field.rows <= 2*radius) ||
			(field.cols != image.cols || field.rows != image.rows) ||
			(field.cols != mask.cols || field.rows != mask.rows)) {
			// A proper field is mandatory to compute frequencies
			// Label this exit code for later use
		exit_code:
			// Assign a default value to output parameter
			meanFreq = 10.0;
			varFreq = 1.0;
			return false;
		}
		
		cv::Mat_<cv::Point> suitablePoints;
		// Check the minNormSample parameter
		if (minNormQuantile == 0) {
			// No point would be accepted, resulting in an error
			goto exit_code;
		} else if (minNormQuantile == 1) {
			// Every point would be accepted, without the need to further computation
			// Continue the execution of the function where suitablePoints are set to all the points
			cv::findNonZero(Mask::ones(field.size()), suitablePoints);
		} else {
			// Compute the norm image
			Image normImage = fm::magnitude<PrecisionPolicy>(field);
			// Get the points with a suitable norm (within the first quantile of order minNormSample)
			std::multiset<Real, std::greater<>> normImageValues(normImage.begin(), normImage.end());
			Int quantilePos = (Int)round(minNormQuantile * normImage.total());
			Real normThreshold = *std::next(normImageValues.begin(), quantilePos);
			// Mask all the image, but a grid with given step, leaving a border of given radius
			Mask maskConstraint(field.size(), PPobj.MaskFALSE);
			Mask maskConInner = maskConstraint(cv::Rect(radius, radius, maskConstraint.cols-2*radius, maskConstraint.rows-2*radius));
			for (Int i = 0; i < maskConInner.rows; i += step) {
				for (Int j = 0; j < maskConInner.cols; j += step) {
					maskConInner(i,j) = PPobj.MaskTRUE;
				}
			}
			cv::findNonZero( (normImage >= normThreshold) & maskConstraint & mask , suitablePoints);
		}
		
		if (suitablePoints.empty()) {
			// No point to use for further computation
			goto exit_code;
		}
		
		// Create the interpolant (for later faster interpolation)
		alglib::spline2dinterpolant interpImage;
		arma::Col<Real> arma_rowSpan = arma::regspace(0, image.rows-1),
		arma_colSpan = arma::regspace(0, image.cols-1);
		alglib::real_1d_array rowSpan, colSpan, imageValues;
		rowSpan.setcontent(image.rows, arma_rowSpan.memptr());
		colSpan.setcontent(image.cols, arma_colSpan.memptr());
		imageValues.setcontent(image.total(), image.template ptr<Real>());
		try {
			alglib::spline2dbuildbilinearv(colSpan, image.cols, rowSpan, image.rows, imageValues, 1/*depth*/, interpImage);
		} catch (alglib::ap_error err) {
			std::cout << "frequencyInfo: spline2dbuildlinearv error: " << err.msg << std::endl;
			goto exit_code;
		}
		// Loop over the points found, generating a segment for each one
        arma::Col<Real> arma_radiiCol = arma::linspace(-radius, radius, numSamples);
        Image radiiCol(numSamples, 1, arma_radiiCol.memptr());
		Image pointMat(numSamples, 2), directionRow(1,2), DFTinputs(suitablePoints.total(), numSamples);
		Int segmentIdx = 0;
		for (const cv::Point& point : suitablePoints) {
			const Complex& curField = field(point);
			// Get phase angle, divide by 2 to get the actual direction of ridges, add pi/2 to get the orthogonal direction
			Real angle = std::arg(std::complex<Real>(curField[0], curField[1]))/2.0 + PPobj.CV_PI_2;
			// Create the list of points in the segment
			pointMat.col(0) = Real(point.x);
			pointMat.col(1) = Real(point.y);
			directionRow(0) = cos(angle);
			directionRow(1) = sin(angle);
			Image pointList = pointMat + radiiCol * directionRow;
			// Use interpolation to get points value over the segment
			Image DFTinRow = DFTinputs.row(segmentIdx++);
			for (Int k = 0; k < pointList.rows; k++) {
				DFTinRow(k) = alglib::spline2dcalc(interpImage, pointList(k,0), pointList(k,1));
			}
			// Blur the list of values through a gaussian kernel
			arma::vec smoothSegVal = arma::conv(arma::Col<Real>(DFTinRow.template ptr<Real>(), DFTinRow.total(), false/*no copy*/),
												arma::Col<Real>(cv::getGaussianKernel(2*gaussFilterRad+1, gaussFilterRad/2.0, Image().type()).template ptr<Real>(),
														  2*gaussFilterRad+1, false/*no copy*/),
												"same");
            DFTinRow = Image(1, smoothSegVal.size(), smoothSegVal.memptr()).clone();
		}
		
		// Compute FFT for each segment (row)
		Field DFToutputs;
		cv::dft(DFTinputs, DFToutputs, cv::DftFlags::DFT_ROWS + cv::DftFlags::DFT_COMPLEX_OUTPUT);
		// Compute spectrum and drop the first frequency
		Image spectrum = fm::magnitude<PrecisionPolicy>(DFToutputs.colRange(cv::Range(1,DFToutputs.cols)));
		// Analyze each row
		std::multiset<Real, std::greater_equal<>> freqs; // container for computed freqs
		for (Int i = 0; i < spectrum.rows; i++) {
			// Find the first maximum in the array (preceeded by a slope of given length)
			Int pos = FindFirstMax<PrecisionPolicy>(spectrum.row(i), 1).first;
			if (pos < 0) {
				goto exit_code;
			}
			// Increase pos to compensate the removing of the first frequency from the spectrum
			pos++;
			/* Se la sua posizione è nell'intervallo di frequenze previsto allora la inserisco
			 nella lista di frequenze trovate nell'immagine */
            Real freq = (2.0*Real(radius)+1.0)/Real(pos);
			if ( (freq >= minValFreq || minValFreq < 0.0) && (freq <= maxValFreq || maxValFreq < 0.0) )
				freqs.insert( freq );
		}
		
		// Check consistence and remove outliers
		if (leftQuantile >= rightQuantile || freqs.empty()) {
			// No frequencies would be selected, so exit
			goto exit_code;
		}
		
		Int leftCutIdx = (Int)floor(fmax(leftQuantile,0) * freqs.size()),
		rightCutIdx = (Int)ceil(fmin(rightQuantile,1) * freqs.size());
		// first remove right outliers, otherwise begin iterator changes
		freqs.erase(std::next(freqs.begin(), rightCutIdx), freqs.end());
		freqs.erase(freqs.begin(), std::next(freqs.begin(), leftCutIdx));
		
		// Output mean and standard deviation of the remaining frequencies
		std::vector<Real> stlvec_freqs(freqs.begin(), freqs.end());
		arma::Col<Real> arma_freqs(stlvec_freqs.data(), stlvec_freqs.size(), false/*no copy*/);
		meanFreq = arma::mean(arma_freqs);
		varFreq = arma::stddev(arma_freqs);
		return true;
	}
	
}

#endif /* ComputeFrequency_h */
