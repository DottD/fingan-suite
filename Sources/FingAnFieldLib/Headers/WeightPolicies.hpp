#ifndef WeightPolicies_h
#define WeightPolicies_h

#include <opencv2/opencv.hpp>

namespace fm {
	template <typename PrecisionPolicy>
	class WeightNone {
	private:
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Point Point;
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::FieldChoice FieldChoice;
		
	protected:
		virtual Complex calc(FieldChoice choice,
							 Point p) = 0;
		
		virtual Real computeWeight(Point,
								   Point) {
			return 1.0;
		}
	};
	
	template <typename PrecisionPolicy>
	class WeightTangent {
	private:
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Point Point;
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::FieldChoice FieldChoice;
		
	protected:
		virtual Complex calc(FieldChoice choice,
							 Point p) = 0;
		
		virtual Real computeWeight(Point p,
								   Point interpPoint) {
			return abs(p.dot(this->calc( FieldChoice::NormSqrtField, interpPoint )));
		}
	};
	
	template <typename PrecisionPolicy>
	class WeightNormal {
	private:
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Point Point;
		typedef typename PrecisionPolicy::Complex Complex;
		typedef typename PrecisionPolicy::FieldChoice FieldChoice;
		
	protected:
		virtual Complex calc(FieldChoice choice,
							 Point p) = 0;
		
		virtual Real computeWeight(Point p,
								   Point interpPoint) {
			Complex F = this->calc( FieldChoice::NormSqrtField, interpPoint );
			cv::Vec<Real, 3> F3D(F[0], F[1], 0), p3D(p[0], p[1], 0);
			return abs(p3D.cross(F3D)[2]);
		}
	};
}

#endif /* WeightPolicies_h */
