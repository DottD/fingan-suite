#ifndef MorphologyOperations_h
#define MorphologyOperations_h

#include <opencv2/opencv.hpp>

namespace fm {
	template <typename PrecisionPolicy = DoublePrecision>
	class MorphOperation {
	public:
		// Pull types definitions from precision policy
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Image Image;
		typedef typename PrecisionPolicy::Mask Mask;
		
		virtual Mask operator<<(Mask mask) const = 0;
	};
	
	template <typename PrecisionPolicy = DoublePrecision>
	class MorphSmoothOperation : public MorphOperation<PrecisionPolicy> {
	public:
		// Pull types definitions from precision policy
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Image Image;
		typedef typename PrecisionPolicy::Mask Mask;
		
	private:
		Real sigma;
		Int R, r;
		PrecisionPolicy ppObj;
		
	public:
		MorphSmoothOperation(Int radius) {
			r = radius;
			R = 2*radius+1;
			sigma = Real(radius)/2.0;
		};
		
		Image blur(Mask& mask) const {
			Image realMask;
			mask.convertTo(realMask, Image().type());
			cv::GaussianBlur(realMask/Real(ppObj.MaskTRUE), realMask, cv::Size(this->R, this->R), this->sigma, this->sigma, cv::BorderTypes::BORDER_CONSTANT);
			return realMask;
		}
	};
	
	template <typename PrecisionPolicy = DoublePrecision>
	class MorphSmoothErosion : public MorphSmoothOperation<PrecisionPolicy> {
	public:
		// Pull types definitions from precision policy
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Mask Mask;
		
		MorphSmoothErosion(Int radius) : MorphSmoothOperation<PrecisionPolicy>(radius) {};
		
		Mask operator<<(Mask mask) const {
			Mask out = ( this->blur(mask) > 0.95 );
			return out;
		}
	};
	
	template <typename PrecisionPolicy = DoublePrecision>
	class MorphSmoothDilation : public MorphSmoothOperation<PrecisionPolicy> {
	public:
		// Pull types definitions from precision policy
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Mask Mask;
		
		MorphSmoothDilation(Int radius) : MorphSmoothOperation<PrecisionPolicy>(radius) {};
		
		Mask operator<<(Mask mask) const {
			Mask out = ( this->blur(mask) > 0.05 );
			return out;
		}
	};
	
	template <typename PrecisionPolicy = DoublePrecision>
	class MorphDistOperation : public MorphOperation<PrecisionPolicy> {
	public:
		// Pull types definitions from precision policy
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Image Image;
		typedef typename PrecisionPolicy::Mask Mask;
		
	protected:
		Real lev;
		PrecisionPolicy ppObj;
		
	public:
		MorphDistOperation(Real lev) {
			this->lev = lev;
		}
		
		Image dist(Mask& mask) const {
			cv::Mat distance;
			cv::distanceTransform(mask, distance, cv::DIST_L2, cv::DIST_MASK_5);
			Image realMask;
			distance.convertTo(realMask, Image().type());
			return realMask;
		}
	};
	
	template <typename PrecisionPolicy = DoublePrecision>
	class MorphDistErosion : public MorphDistOperation<PrecisionPolicy> {
	public:
		// Pull types definitions from precision policy
		typedef typename PrecisionPolicy::Mask Mask;
		typedef typename PrecisionPolicy::Real Real;
		
		MorphDistErosion(Real lev) : MorphDistOperation<PrecisionPolicy>(lev) {};
		
		Mask operator<<(Mask mask) const {
			Mask out = ( this->dist(mask) > this->lev );
			return out;
		}
	};
	
	template <typename PrecisionPolicy = DoublePrecision>
	class MorphDistDilation : public MorphDistOperation<PrecisionPolicy> {
	public:
		// Pull types definitions from precision policy
		typedef typename PrecisionPolicy::Mask Mask;
		typedef typename PrecisionPolicy::Real Real;
		
		MorphDistDilation(Real lev) : MorphDistOperation<PrecisionPolicy>(lev) {};
		
		Mask operator<<(Mask mask) const {
			Mask out = ~mask;
			out = ~( this->dist(out) > this->lev );
			return out;
		}
	};
	
	template <typename PrecisionPolicy = DoublePrecision>
	class MorphFilterOperation : public MorphOperation<PrecisionPolicy> {
	public:
		typedef typename PrecisionPolicy::Mask Mask;
		typedef typename PrecisionPolicy::Real Real;
		typedef std::map<int, Real> ValuesContainer;
		
	protected:
		mutable ValuesContainer valuesByLabels;
		PrecisionPolicy ppObj;
		mutable cv::Mat1i labels, stats;
		mutable cv::Mat1d centroids;
		mutable Real threshold;
		
		virtual void computeValues(Mask& mask) const = 0;
		
	public:
		MorphFilterOperation(Real threshold) {
			this->threshold = threshold;
		}
		
		Mask operator<<(Mask mask) const {
			Mask out = mask.clone();
			// Check if the mask contains foreground, otherwise do nothing
			if (cv::countNonZero(out) > 0) {
				cv::connectedComponentsWithStats(out, this->labels, this->stats, this->centroids);
				// Compute components values
				this->computeValues(out);
				// Filter components by value
				for (const auto& element : this->valuesByLabels){
					// Check if the distance is acceptable
					if (element.second < this->threshold){
						out.setTo( this->ppObj.MaskFALSE, this->labels == element.first );
					}
				}
			}
			return out;
		}
	};
	
	template <typename PrecisionPolicy = DoublePrecision>
	class MorphFilterMaxCentrDist : public MorphFilterOperation<PrecisionPolicy> {
	public:
		typedef typename PrecisionPolicy::Real Real;
		
		MorphFilterMaxCentrDist(Real threshold) : MorphFilterOperation<PrecisionPolicy>(threshold) {};
		
	private:
		void computeValues(typename PrecisionPolicy::Mask& mask) const {
			// Adapt the threshold parameter
			this->threshold *= this->threshold;
			// Loop over the mask and compute the distance to the centroid of each label
			cv::Vec2d p, diff;
			typename MorphFilterOperation<PrecisionPolicy>::ValuesContainer::iterator MCDiter;
			for (p[0] = 0.0; p[0] < mask.cols; p[0] += 1.0){ // compute the maximum distance from centroid for each component
				for (p[1] = 0.0; p[1] < mask.rows; p[1] += 1.0){
					const int& label = this->labels(cv::Point_<Real>(p));
					if (label > 0) { // not background
						// Compute the distance from the centroid
						diff[0] = p[0]-this->centroids(label,0);
						diff[1] = p[1]-this->centroids(label,1);
						typename PrecisionPolicy::Real squareDist = diff.dot(diff);
						// Search the label in the stored labels with distances
						MCDiter = this->valuesByLabels.find(label);
						if (MCDiter != this->valuesByLabels.end()){
							// Label found, update the maximum centroid distance for this component
							if (MCDiter->second < squareDist){
								MCDiter->second = squareDist;
							}
						} else {
							// Label not found, insert the computed distance
							this->valuesByLabels[label] = squareDist;
						}
					}
				}
			}
		}
	};
}

#endif /* MorphologyOperations_h */
