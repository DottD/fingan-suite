#ifndef CommonOperations_h
#define CommonOperations_h

#include <Headers/QtCustomException.hpp>
#include <opencv2/opencv.hpp>

namespace fm {
	
	/** Find the first maximum of the input array.
	 Find the first maximum, i.e. the first point where an increasing monotony breaks into a
	 decreasing one; allows for controlling the minimum slope length that must preceed the
	 maximum point.
	 @param[in] array Array where the maximum point should be found. Its shape is irrelevant,
	 since every elements is used.
	 @param[in] minSlopeLength Minimum slope length that must preceed a maximum point.
	 @return A pair the maximum point index and the iterator to that element.
	 */
	template <typename PrecisionPolicy>
	std::pair<typename PrecisionPolicy::Int, typename PrecisionPolicy::Image::iterator>
	FindFirstMax(typename PrecisionPolicy::Image array,
				 const typename PrecisionPolicy::Int minSlopeLength) {
		// Import used symbols from the precision policy
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Image::iterator Iterator;
		// Declare control variables for next loop
		Int count = 0;
		Iterator prec = array.begin(), // it1 points to the first element
		curr = array.begin()+1, // it2 points to the second element
		next = array.begin()+2; // it3 points to the third element
		// Loop over the input array looking for the maximum point
		for (; next != array.end(); prec++, curr++, next++) {
			// Check if the array keeps monotony, otherwise reset count
			if (*prec <= *curr) count++;
			else count = 0;
			// If count is large enough, check if next element breaks the monotony,
			// in that event break the loop with count pointing to the maximum element.
			if (count >= minSlopeLength) {
				if (*curr > *next) break;
			}
		}
		// If the loop has ended without finding any maximums
		if (next == array.end()) {
			// If there was an increasing monotony, return the last point
			if (count >= minSlopeLength) return std::make_pair( (Int)(array.total()-1) , std::prev(array.end(),-1) );
			// otherwise return (-1, end of array)
            else return std::make_pair( -1, array.end() );
		}
		// If a maximum point has been found return its position and its value
		else return std::make_pair( curr-array.begin(), curr );
	}
	
	template <typename PrecisionPolicy>
	typename PrecisionPolicy::PointList RealCircleList(const typename PrecisionPolicy::Real radius,
													   const typename PrecisionPolicy::Point center,
													   const typename PrecisionPolicy::Int numSamples) {
		// Declarations
		typedef typename PrecisionPolicy::Real Real;
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Int Int;
		typename PrecisionPolicy::Point unitCirclePoint;
		typename PrecisionPolicy::PointList circle;
		const Real maxAngle = CV_2PI;
		const Real minAngle = 0.0;
		// Compute number of samples and step
		Real step = (maxAngle-minAngle) / Real(numSamples);
		// Generate the samples
		circle.create(1, Int(numSamples));
		Int k = 0;
		for (Real t = minAngle; t <= maxAngle-step; t += step) {
			unitCirclePoint[0] = cos(t); unitCirclePoint[1] = sin(t);
			circle(k++) = center + radius * unitCirclePoint;
		}
		return circle;
	}
	
	/*
	 GenDiskMatrix(radius)
	 
	 Genera una maschera di lato 2*radius+1, in cui hanno valore true solamente i punti che si
	 trovano all'interno di un cerchio di raggio radius centrato nel centro della maschera.
	 
	 Argomenti:
	 -radius lato della maschera e raggio del cerchio
	 
	 */
	template <typename PrecisionPolicy>
	typename PrecisionPolicy::Mask GenDiskMatrix(const typename PrecisionPolicy::Int radius) {
		// Import used symbols from the precision policy
		PrecisionPolicy ppObj;
		typedef typename PrecisionPolicy::Int Int;
		typedef typename PrecisionPolicy::Mask Mask;
		// Exception if the radius is null
		if (radius == 0) throw QtCustomException("GenDiskMatrix: null radius");
		// Allcate memory for the output
		Mask kernel(2*radius+1, 2*radius+1);
		Int r2 = radius*radius;
		for (Int i = -radius; i <= radius; i++) {
			for(Int j = -radius; j <= radius; j++) {
				kernel(i+radius,j+radius) = i*i+j*j <= r2 ? ppObj.MaskTRUE : ppObj.MaskFALSE;
			}
		}
		return kernel;
	}
	
	/*
	 GenDirGradMatrix(radius, angle, s1, e1, s2, e2)
	 
	 Genera una maschera di lato 2*radius+1; tale maschera è un gradiente unidimensionale nella direzione
	 indicata da angle ed uno smussamento gaussiano nella direzione perpendicolare; i parametri s1, e1, s2, e2
	 servono per modificare la varianza della gaussiana e la sua pendenza.
	 
	 Argomenti:
	 - radius grandezza della maschera
	 - angle direzione lungo cui eseguire il filtro gradiente (nel sistema di coordinate iOj)
	 - s1, e1 varianza e pendenza del filtro lungo la direzione del gradiente
	 - s2, e2 varianza e pendenza del filtro lungo la direzione perpendicolare al gradiente
	 
	 */
	template <typename PrecisionPolicy>
	typename PrecisionPolicy::Image GenDirGradMatrix(const typename PrecisionPolicy::Int radius,
													 typename PrecisionPolicy::Real ang,
													 const typename PrecisionPolicy::Real s1,
													 typename PrecisionPolicy::Real e1,
													 const typename PrecisionPolicy::Real s2,
													 typename PrecisionPolicy::Real e2) {
		// Import used symbols from precision policy
		PrecisionPolicy ppObj;
		typedef typename PrecisionPolicy::Image Image;
		typedef typename PrecisionPolicy::Real Real;
		// Allocate memory for the output image
		Image kernel(2*radius+1, 2*radius+1);
		
		// Slightly modify the parameters
		ang += ppObj.CV_PI_2; // the desired direction is the orthogonal one
		e1 *= 2.0;
		e2 *= 2.0;
		
		/* Ruoto la matrice di -angle, quindi applico la funzione f */
		for (int i = -radius; i <= radius; i++) {
			for (int j = -radius; j <= radius; j++) {
				Real t1 = cos(ang)*i + sin(ang)*j;
				Real t2 = -sin(ang)*i + cos(ang)*j;
				kernel(i+radius, j+radius) = t1*exp( -pow(fabs(t1/s1), e1) - pow(fabs(t2/s2), e2) );
			}
		}
		
		/* Al kernel sottraggo la matrice che si ottiene ruotandolo di 180°, al fine di ottenere somma nulla */
		Image flipKernel(kernel.size());
		cv::flip(kernel, flipKernel, -1); // flip su entrambi gli assi
		kernel -= flipKernel;
		
		/* Moltiplico la maschera ottenuta per una maschera circolare della stessa dimensione */
		kernel.setTo(0, ~fm::GenDiskMatrix<PrecisionPolicy>(radius));
		
		/* Faccio in modo che il kernel abbia somma dei valori assoluti unitaria */
		Real kernelTotal = cv::sum(cv::abs(kernel))[0];
		if (kernelTotal == 0.0) return kernel;
		else return kernel / kernelTotal;
	}

}

#endif /* CommonOperations_h */
