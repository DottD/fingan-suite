#ifndef debugLib_h
#define debugLib_h

#include <string>
#include <opencv2/opencv.hpp>
#include <QDir>
#include "../FingAnFieldLib/Headers/PrecisionPolicies.hpp"
#include "../FingAnFieldLib/Headers/FieldOperations.hpp"

void dbgHelperLog(const cv::Mat& mat,
				  const char* name);
void dbgHelperLog(const cv::Mat& mat,
				  const fm::DoublePrecision::Field& field,
				  const char* name,
				  const int& step = 5,
				  const int& lengthFactor = 2,
				  const int& thicknessFactor = 2,
				  const fm::DoublePrecision::Real& sampling = 4);

bool drawFieldOnImage(cv::Mat& out,
					  const cv::Mat& image,
					  const fm::DoublePrecision::Field& squareField,
					  const int& step,
					  const int& lengthFactor,
					  const int& thicknessFactor,
					  const fm::DoublePrecision::Real& sampling,
					  const bool& fieldComputeOrtho,
					  const cv::Scalar& color);

#endif /* debugLib_h */

