#include "debugLib.hpp"

void dbgHelperLog(const cv::Mat& mat, const char* name){
	if(!mat.empty()){
		QDir dir = QDir::home();
		if(dir.exists("Downloads") && dir.cd("Downloads") &&
		   dir.exists("XCodeDebugDir") && dir.cd("XCodeDebugDir")){
			QString path = dir.filePath(name);
			cv::Mat out;
			cv::normalize(mat, out, 0, 255, cv::NORM_MINMAX);
			cv::imwrite(path.toStdString().c_str(), out);
		}else{
			std::cout << "Check if path ~/Downloads/XCodeDebugDir/ is existent" << std::endl;
		}
	}else{
		std::cout << "Check if input is properly initialized" << std::endl;
	}
}

void dbgHelperLog(const cv::Mat& mat,
				  const fm::DoublePrecision::Field& field,
				  const char* name,
				  const int& step,
				  const int& lengthFactor,
				  const int& thicknessFactor,
				  const fm::DoublePrecision::Real& sampling){
	if(!mat.empty() && !field.empty()){
		QDir dir = QDir::home();
		if(dir.exists("Downloads") && dir.cd("Downloads") &&
		   dir.exists("XCodeDebugDir") && dir.cd("XCodeDebugDir")){
			QString path = dir.filePath(name);
			cv::Mat scaled, colored;
			cv::normalize(mat, scaled, 0, 255, cv::NORM_MINMAX);
			if(drawFieldOnImage(colored, scaled, field, step, lengthFactor, thicknessFactor, sampling, true, cv::Scalar(0,0,255,0))){
				cv::imwrite(path.toStdString().c_str(), colored);
			}
		}else{
			std::cout << "Check if path ~/Downloads/XCodeDebugDir/ is existent" << std::endl;
		}
	}else{
		std::cout << "Check if input is properly initialized" << std::endl;
	}
}

bool drawFieldOnImage(cv::Mat& out,
					  const cv::Mat& image,
					  const fm::DoublePrecision::Field& squareField,
					  const int& step,
					  const int& lengthFactor,
					  const int& thicknessFactor,
					  const fm::DoublePrecision::Real& sampling,
					  const bool& fieldComputeOrtho,
					  const cv::Scalar& color){
	typedef fm::DoublePrecision::Mask Mask;
	typedef fm::DoublePrecision::Image Image;
	typedef fm::DoublePrecision::Field Field;
	typedef fm::DoublePrecision::Int Int;
	typedef fm::DoublePrecision::Real Real;
	typedef fm::DoublePrecision::Complex Complex;
	
	// Compute the ratio between image and field dimensions
	Real fieldSampling;
	if (image.empty() || squareField.empty()){
		std::cout << "drawFieldOnImage - Empty input image or field" << std::endl;
		return false;
	}else{
		Real wratio = Real(image.size().width)/Real(squareField.size().width);
		Real hratio = Real(image.size().height)/Real(squareField.size().height);
		fieldSampling = std::min<Real>(wratio, hratio);
	}
	
	// Compute phase and magnitude of the given field
	Image magnitude, phase;
	fm::cartToPolar<fm::DoublePrecision>(squareField, magnitude, phase);
	// Halve input phase, then rotate if requested
	phase = -( phase + (Real)fieldComputeOrtho * CV_PI ) / 2.0;
	Mask zeroMagnitude = magnitude < 1E-4;
	magnitude.setTo(0.0, zeroMagnitude);
	magnitude.setTo(1.0, ~zeroMagnitude);
	Field dirField;
	fm::polarToCart<fm::DoublePrecision>(magnitude, phase, dirField);
	
	// Generate the list of segments to draw
	std::vector<std::vector<cv::Point>> pts; // list of segments heads and tails
	std::vector<cv::Point> line(2, cv::Point()); // punto del segmento
	for (Int i = step; i < squareField.rows-step; i += step) {
		for (Int j = step; j < squareField.cols-step; j += step) {
			// Check if the magnitude is zero
			if (magnitude(i, j) == 0.0) continue;
			// Compute head and tail of the current segment (rounded to the closest integer)
			const Complex& F = dirField(i, j);
			cv::Point2d midPt = sampling * fieldSampling * cv::Point2d(j, i);
			cv::Point2d sidesPt = sampling * lengthFactor * cv::Point2d(F[0], F[1]);
			line[0] = (midPt + sidesPt);
			line[1] = (midPt - sidesPt);
			// Add to the list of segments
			pts.emplace_back(line);
		}
	}
	// Create a 3-channel image
	cv::Mat resized; // resized version of the input image
	cv::resize(image, resized, cv::Size(), sampling, sampling);
	out.create(resized.size(), CV_MAKETYPE(resized.depth(), 3));
	if(resized.channels() == 3){
		resized.copyTo(out);
	} else if (resized.channels() == 1){
		cv::mixChannels(resized, out, {0, 0, 0, 1, 0, 2});
	} else {
		std::cout << "drawFieldOnImage - The image must have 1 or 3 channels" << std::endl;
		return false;
	}
	// Draw the segments
	cv::polylines(out, pts,
				  false, // we don't need close lines
				  color,
				  thicknessFactor,
				  cv::LineTypes::LINE_8);
	return true;
}
