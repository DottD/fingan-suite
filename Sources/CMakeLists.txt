# FingAnSuite global CMakeLists
cmake_minimum_required(VERSION 3.0 FATAL_ERROR)
cmake_policy(SET CMP0020 NEW)
project(FingAnSuite)

#---Find the necessary packages
find_package(Qt5 COMPONENTS Core REQUIRED)
find_package(OpenCV REQUIRED core imgproc imgcodecs)
find_package(PkgConfig REQUIRED)
pkg_check_modules(ALGLIB REQUIRED alglib)
find_package(Armadillo REQUIRED)

#---Add the subprojects to this workspace
add_subdirectory(DebugLib)
add_subdirectory(FingAnCoreLib)
add_subdirectory(FingAnFieldLib)
add_subdirectory(FingAnInterfaceLib)
add_subdirectory(FingAnOperations)
add_subdirectory(computeOrientation)
add_subdirectory(preProcessing)