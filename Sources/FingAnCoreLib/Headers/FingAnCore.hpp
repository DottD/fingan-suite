#include <Headers/myMathFunc.hpp>
#include <Headers/AdaptiveThreshold.hpp>
#include <Headers/ImageSignificantMask.hpp>
#include <Headers/ImageRescale.hpp>
#include <Headers/ImageNormalization.hpp>
#include <Headers/ImageMaskSimplify.hpp>
#include <Headers/ImageCropping.hpp>
