#include "FVCPipe.hpp"

FVCPipe::FVCPipe() : pipeName("FVCPipeForFVCOrientation") {}

bool FVCPipe::reset() {
	QDir pipeDir;
	pipeDir.setPath(QDir::currentPath());
	if(!pipeDir.exists("pipe")){
		if(!pipeDir.mkdir("pipe"))
			throw QtCustomException("FVCPipe constructor - Cannot create the pipe folder in the current path");
	}
	if(!pipeDir.cd("pipe"))
		throw QtCustomException("FVCPipe constructor - Cannot cd into the pipe folder");
	QFile pipeFile(pipeDir.absoluteFilePath(pipeName));
	if(!pipeFile.open(QIODevice::WriteOnly|QIODevice::Text))
		throw QtCustomException("FVCPipe constructor - Cannot open pipe for writing");
	pipeFile.close();
	return true;
}

FVCPipe& FVCPipe::operator<<(const QString& message) {
	QDir pipeDir;
	pipeDir.setPath(QDir::currentPath());
	if(!pipeDir.exists("pipe")){
		if(!pipeDir.mkdir("pipe"))
			throw QtCustomException("FVCPipe append - Cannot create the pipe folder in the current path");
	}
	if(!pipeDir.cd("pipe"))
		throw QtCustomException("FVCPipe append - Cannot cd into the pipe folder");
	QFile pipeFile(pipeDir.absoluteFilePath(pipeName));
	if(!pipeFile.open(QIODevice::Append|QIODevice::Text))
		throw QtCustomException("FVCPipe append - Cannot open pipe for appending");
	QTextStream ostream(&pipeFile);
    ostream << message;
    pipeFile.close();
	return std::ref(*this);
}
