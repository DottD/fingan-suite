#include "fa_improveMainObject.hpp"

fa_improve::mainObject::mainObject(QObject* parent) : QObject(parent){
	
}

void fa_improve::mainObject::defaultParameters(){
	settings.beginGroup("ImageCroppingSimple");
	settings.setValue("minVariation", 0.01);
	settings.setValue("marg", 5);
	settings.endGroup();
	
	settings.beginGroup("ImageBimodalize");
	settings.setValue("brightness", 0.35);
	settings.setValue("leftCut", 0.25);
	settings.setValue("rightCut", 0.5);
	settings.setValue("histSmooth", 25);
	settings.setValue("reparSmooth", 10);
	settings.endGroup();
	
	settings.beginGroup("ImageEqualize");
	settings.setValue("minMaxFilter", 5);
	settings.setValue("mincp1", 0.75);
	settings.setValue("mincp2", 0.9);
	settings.setValue("maxcp1", 0.0);
	settings.setValue("maxcp2", 0.25);
	settings.endGroup();
	
	settings.beginGroup("TopMask");
	settings.setValue("scanAreaAmount", 0.1);
	settings.setValue("gradientFilterWidth", 0.25);
	settings.setValue("binarizationLevel", 0.2);
	settings.setValue("f", 3.5);
	settings.setValue("slopeAngle", 1.5);
	settings.setValue("lev", 0.95);
	settings.setValue("gaussianFilterSide", 5);
	settings.setValue("marg", 5);
	settings.endGroup();
	
	settings.beginGroup("ImageSignificantMask");
	settings.setValue("medFilterSide", 2);
	settings.setValue("gaussFilterSide", 3);
	settings.setValue("minFilterSide", 5);
	settings.setValue("dilate1RadiusVarMask", 5);
	settings.setValue("erodeRadiusVarMask", 35);
	settings.setValue("dilate2RadiusVarMask", 20);
	settings.setValue("maxCompNumVarMask", 2);
	settings.setValue("minCompThickVarMask", 75);
	settings.setValue("maxHolesNumVarMask", -1);
	settings.setValue("minHolesThickVarMask", 18);
	settings.setValue("binLevVarMask", 0.45);
	settings.setValue("histeresisThreshold1Gmask", 30); // in mathematica il threshold è 0.085 - consigliano un rapporto 1:2 - 1:3
	settings.setValue("histeresisThreshold2Gmask", 70);
	settings.setValue("radiusGaussFilterGmask", 10);
	settings.setValue("minMeanIntensityGmask", 0.2);
	settings.setValue("dilate1RadiusGmask", 10);
	settings.setValue("erodeRadiusGmask", 25);
	settings.setValue("dilate2RadiusGmask", 10);
	settings.setValue("maxCompNumGmask", 2);
	settings.setValue("minCompThickGmask", 75);
	settings.setValue("maxHolesNumGmask", -1);
	settings.setValue("minHolesThickGmask", 15);
	settings.setValue("histeresisThreshold3Gmask", 25); // in mathematica il threshold è 0.075 - consigliano un rapporto 1:2 - 1:3
	settings.setValue("histeresisThreshold4Gmask", 50);
	settings.setValue("dilate3RadiusGmask", 10);
	settings.setValue("erode2RadiusGmask", 5);
	settings.setValue("histeresisThreshold5Gmask", 45); // in mathematica il threshold è 0.095 - consigliano un rapporto 1:2 - 1:3
	settings.setValue("histeresisThreshold6Gmask", 90);
	settings.setValue("dilate4RadiusGmask", 4);
	settings.setValue("radiusGaussFilterComp", 30); // previously 20
	settings.setValue("meanIntensityCompThreshold", 0.6); // previously 0.25
	settings.setValue("dilateFinalRadius", 10);
	settings.setValue("erodeFinalRadius", 20);
	settings.setValue("smoothFinalRadius", 10);
	settings.setValue("maxCompNumFinal", 2);
	settings.setValue("minCompThickFinal", 75);
	settings.setValue("maxHolesNumFinal", 4);
	settings.setValue("minHolesThickFinal", 30);
	settings.setValue("fixedFrameWidth", 20);
	settings.setValue("smooth2FinalRadius", 20);
	settings.endGroup();
	
	settings.beginGroup("InitialOrientationEstimation");
	settings.setValue("radius", 15);
	settings.setValue("radDirGrad", 1.0);
	settings.setValue("var1DirGrad", 1.0);
	settings.setValue("exp1DirGrad", 2.0);
	settings.setValue("var2DirGrad", 0.85);
	settings.setValue("exp2DirGrad", 2.0);
	settings.setValue("numAngles", 36);
	settings.setValue("weightGaussRadius", 1.0);
	settings.setValue("weightGaussExp", 4.0);
	settings.setValue("fieldGaussRadius", 0.5);
	settings.endGroup();
	
	settings.beginGroup("FrequencyEstimation");
	settings.setValue("radius", 15);
	settings.setValue("step", 10);
	settings.setValue("minNormSample", 0.25);
	settings.setValue("gaussFilterSideVal", 3);
	settings.setValue("gaussFilterSideSpectrum", 5);
	settings.setValue("minValFreq", 5);
	settings.setValue("maxValFreq", 30);
	settings.setValue("leftQuantile", 0.05);
	settings.setValue("rightQuantile", 0.75);
	settings.endGroup();
	
	settings.beginGroup("OrientationRefinement");
	settings.setValue("OS_SM1_FIELD_RAD_FAC", 1.5);
	settings.setValue("OS_SM1_SMOOTH_RAD_FAC", 1);
	settings.setValue("OS_SM1_SMOOTH_STRENGTH", 1);
	settings.setValue("OS_SM1_DRIFT_RAD_FAC", 1);
	settings.setValue("OS_SM1_LD_THRESHOLD", 0.3);
	settings.setValue("OS_SM1_MASK_CONTRACTION_RAD_FAC", 3.5);
	settings.setValue("OS_SM1_MASK_BLUR_RAD_FAC", 2);
	settings.setValue("OS_SM2_FIELD_RAD_FAC", 0.5);
	settings.setValue("OS_SM2_ADJUST_RAD_FAC", 0.7);
	settings.setValue("OS_SM2_ADJUST_MASK_RESCALE", 0.9);
	settings.setValue("OS_SM2_ADJUST_RAD_FAC_2", 0.7);
	settings.setValue("OS_SM2_ADJUST_MASK_RESCALE_2", 0.9);
	settings.setValue("OS_SM2_DIFF_BLUR_RAD_FAC", 0.15);
	settings.setValue("OS_SM2_DIFF_THRESHOLD", 0.5);
	settings.setValue("OS_SM2_MIN_MAX_CENTROID_DIST_RAD_FAC", 1);
	settings.setValue("OS_SM2_DIFF_DILATION_RAD_FAC", 1);
	settings.setValue("OS_SM3_DRIFT_RAD_FAC", 1);
	settings.setValue("OS_SM3_LD_BLUR_RAD_FAC", 1.5);
	settings.setValue("OS_SM3_LD_THRESHOLD", 0.1);
	settings.setValue("OS_SM3_MASK_CONTRACTION_RAD_FAC", 3.5);
	settings.setValue("OS_SM3_LD_MIN_MAX_CENTROID_DIST_RAD_FAC", 0.75);
	settings.setValue("OS_SM_DIFF_DILATION2_RAD_FAC", 2);
	settings.setValue("OS_SM_DIFF_DILATION3_RAD_FAC", 2);
	settings.setValue("OS_IS_RAD_FAC", 1.5);
	settings.setValue("OS_IS_STRENGTH", 1);
	settings.setValue("OS_IS_INITIAL_LEV", 0.1);
	settings.setValue("OS_IS_MAX_STEPS", 10);
	settings.setValue("OS_IS_CONTRACTION_RAD_FAC", 0.1);
	settings.setValue("OS_IS_DILATION_RAD_FAC", 2);
	settings.setValue("OS_IS_BLUR_RAD_FAC", 2);
	settings.setValue("OS_IS_LEV_INCREMENT", 0.01);
	settings.endGroup();
}

void fa_improve::mainObject::mainFunction(){
	// Initialize the command line parser
	QCommandLineParser parser;
	parser.setApplicationDescription("This tool is part of the FingAn Suite. It takes fingerprints as input and improve images quality.");
	parser.addHelpOption();
	parser.addVersionOption();
	parser.addPositionalArgument("inputPath", "An image file or a folder containing images to be processed.");
	parser.addPositionalArgument("outputFolder", "The path of the folder where the output files have to be saved. The application will save the output files reproducing the input files tree.");
	
	// Parse the command line arguments
	QCoreApplication* app = QCoreApplication::instance();
	parser.process(*app);
	const QStringList args = parser.positionalArguments();
	const QString& inputPath = args[0];
	qDebug() << "indexFileName" << inputPath;
	const QString& outputFolderName = args[1];
	qDebug() << "outputFolderName" << outputFolderName;
	
	// Set default parameters
	defaultParameters();
	
	// Recursively scan the input folder or directly process the unique input file
	QDir outputFolder;
	if (FA::Utilities::isFolder(outputFolderName)) outputFolder.setPath(outputFolderName);
	else throw std::runtime_error("Output path must be a folder");
	QStringList inputFiles, outputFiles;
	if (FA::Utilities::isFolder(inputPath)){
		FA::RecurseScanInterface scanner;
		scanner.setTopDir(inputPath);
		scanner.setExts(availableExts);
		scanner.run();
		inputFiles = scanner.getFilesAbsolute();
		qDebug() << "inputFiles" << inputFiles;
		outputFiles = scanner.getFilesRelative();
		qDebug() << "outputFiles - relative path" << outputFiles;
		std::for_each(outputFiles.begin(), outputFiles.end(), [&outputFolder](QString& str){
			str = outputFolder.absoluteFilePath(str);
		});
		qDebug() << "outputFiles" << outputFiles;
	} else if (FA::Utilities::isFile(inputPath)){
		inputFiles << inputPath;
		qDebug() << "inputFiles" << inputFiles;
		outputFiles << QDir(outputFolderName).absoluteFilePath(QDir(inputPath).dirName());
		qDebug() << "outputFiles" << outputFiles;
	} else throw std::runtime_error("Input path is neither an existing file nor a folder");
	
	// Launch a thread for each file found
	QListIterator<QString> input(inputFiles), output(outputFiles);
	while (input.hasNext() && output.hasNext()){
		const QString& ifile = input.next();
		qDebug() << "working on" << ifile;
		const QString& ofile = output.next();
		qDebug() << "saving to" << ofile;
		// Create a new preprocessing instance
		FA::PreProcessing* preprocessing = new FA::PreProcessing;
		// Assign the name of the input file
		preprocessing->setFileName(ifile);
		// Assign the output path
		preprocessing->setOutputPath(ofile);
		// Setup the orientation extraction algorithm
		new FA::FOEProcess(preprocessing);
		// Make the connection to save the results
		connect(preprocessing, SIGNAL(finished(FA::Algorithm*)),
				this, SLOT(saveResults(FA::Algorithm*)));
		connect(preprocessing, SIGNAL(destroyed()),
				this, SLOT(checkActiveProcesses()));
		// The Algorithm class is compatible with QThreadPool system
		QThreadPool::globalInstance()->start(preprocessing);
	}
}

void fa_improve::mainObject::saveResults(FA::Algorithm* sender){
	FA::PreProcessing* pre = dynamic_cast<FA::PreProcessing*>(sender);
	if(pre != Q_NULLPTR){
		// Get input and output file paths
		QString inputFileName = pre->getFileName();
		qDebug() << "Saving image" << inputFileName;
		QDir inputFileNameDir(inputFileName);
		QStringList temp = pre->getOutputPath().split(".");
		QString ext = temp.last();
		qDebug() << "saving extension" << ext;
		temp.removeLast();
		QString base = temp.join(".");
		qDebug() << "saving base name" << base;
		// Create missing folders
		QDir outputDir(pre->getOutputPath());
		outputDir.setPath(outputDir.absolutePath().replace(outputDir.dirName(), "")); // remove file name
		if (!QDir().mkpath(outputDir.absolutePath()))
			throw std::runtime_error("Cannot create the path to the child folder");
		
		// Get the cropping rectangle
		cv::Rect rect = FA::Utilities::convertToRect(pre->getCroppingRect());
		// Get the preprocessing images
		cv::Mat1d originalImage = pre->getOriginalImage();
		cv::Mat1d equalizedImage = pre->getEqualizedImage();
		cv::Mat1d segmentedImage = pre->getSegmentedImage();
		cv::Mat1d processedImage = pre->getFinalImage();
		// Save the preprocessing images
		cv::imwrite((base+"_orig."+ext).toStdString(), originalImage);
		cv::imwrite((base+"_equa."+ext).toStdString(), equalizedImage*255);
		cv::imwrite((base+"_segm."+ext).toStdString(), segmentedImage*255);
		cv::imwrite((base+"_proc."+ext).toStdString(), processedImage*255);
		
		// Try to get the FOEProcess
		const FA::FOEProcess* foe = pre->findChild<FA::FOEProcess*>();
		if(foe != Q_NULLPTR){
			// Get the raw and refined fields
			fm::fieldType field = foe->getField();
			fm::fieldType rawField = foe->getRawField();
			// Overlay them on the processed image
			cv::Mat fieldPic, rawFieldPic;
			FA::Utilities::drawFieldOnImage(fieldPic, processedImage(rect)*255, field,
											7/*step*/, 4/*length factor*/, 2/*thick*/, 8/*oversamplig*/,
											true, cv::Scalar(139.0/255.0, 0, 0, 0));
			FA::Utilities::drawFieldOnImage(rawFieldPic, processedImage(rect)*255, rawField,
											7/*step*/, 4/*length factor*/, 2/*thick*/, 8/*oversamplig*/,
											true, cv::Scalar(139.0/255.0, 0, 0, 0));
			// Write images to file
			cv::imwrite((base+"_field."+ext).toStdString(), fieldPic);
			cv::imwrite((base+"_rawField."+ext).toStdString(), rawFieldPic);
		}
		
		// Instruct to free memory as soon as possible
		pre->deleteLater();
	}
}

void fa_improve::mainObject::checkActiveProcesses(){
	qDebug() << "activeThreads" << QThreadPool::globalInstance()->activeThreadCount();
	if(QThreadPool::globalInstance()->activeThreadCount() == 0)
		thread()->quit();
}
