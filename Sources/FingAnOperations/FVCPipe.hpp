#ifndef FVCFVCPipe_h
#define FVCFVCPipe_h

#include <QString>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QDebug>
#include <../FingAnFieldLib/Headers/QtCustomException.hpp>

/** Class to write content to FVC pipe. */
class FVCPipe {
private:
	const QString pipeName;
public:
	FVCPipe();
	
	/** Reset the pipe. */
	bool reset();
	
	/** Append a message to the pipe.
	 @param[in] msg Message to append to the pipe.
	 @return Returns a reference to this class to allow the operator concatenation.
	 */
	FVCPipe& operator<<(const QString& msg);
};

#endif /* FVCFVCPipe_h */
