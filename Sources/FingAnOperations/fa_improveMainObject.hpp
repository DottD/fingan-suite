#ifndef fa_improveMainObject_hpp
#define fa_improveMainObject_hpp

#include <QObject>
#include <QThread>
#include <QThreadPool>
#include <QCommandLineParser>
#include <QSettings>
#include <QSharedPointer>
#include <QStringList>
#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QDebug>
#include <FingAnCore.hpp>
#include <FingAnInterface.hpp>

namespace fa_improve {
	class mainObject;
	
	// Constants declarations
	const QStringList availableExts = {"*.bmp", "*.dib", "*.jpeg", "*.jpg", "*.jpe", "*.jp2", "*.png", "*.webp", "*.pbm", "*.pgm", "*.ppm", "*.pxm", "*.pnm", "*.sr", "*.ras", "*.tiff", "*.tif", "*.exr", "*.hdr", "*.pic"};
}

class fa_improve::mainObject : public QObject {
	
	Q_OBJECT
	
public:
	QSettings settings;
	
	mainObject(QObject* parent = Q_NULLPTR);
	
	/** Set the parameters to their default values. */
	void defaultParameters();
	
	public Q_SLOTS:
	/** The main function: where everything begins. */
	Q_SLOT void mainFunction();
	
	/** Save results from the sender preprocessing algorithm.
	 The results are saved according to FVC specifications.
	 */
	Q_SLOT void saveResults(FA::Algorithm* sender);
	
	/** Quit the current thread if the global QThreadPool is empty. */
	Q_SLOT void checkActiveProcesses();
};

#endif /* fa_improveMainObject_hpp */
