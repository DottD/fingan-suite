#include "fa_orfoMainObject.hpp"

fa_orfo::mainObject::mainObject(QObject* parent) : QObject(parent){}

void fa_orfo::mainObject::defaultParameters(){
	settings.beginGroup("ImageCroppingSimple");
	settings.setValue("minVariation", 0.01);
	settings.setValue("marg", 5);
	settings.endGroup();
	
	settings.beginGroup("ImageBimodalize");
	settings.setValue("brightness", 0.35);
	settings.setValue("leftCut", 0.25);
	settings.setValue("rightCut", 0.5);
	settings.setValue("histSmooth", 25);
	settings.setValue("reparSmooth", 10);
	settings.endGroup();
	
	settings.beginGroup("ImageEqualize");
	settings.setValue("minMaxFilter", 5);
	settings.setValue("mincp1", 0.75);
	settings.setValue("mincp2", 0.9);
	settings.setValue("maxcp1", 0.0);
	settings.setValue("maxcp2", 0.25);
	settings.endGroup();
	
	settings.beginGroup("TopMask");
	settings.setValue("scanAreaAmount", 0.1);
	settings.setValue("gradientFilterWidth", 0.25);
	settings.setValue("binarizationLevel", 0.2);
	settings.setValue("f", 3.5);
	settings.setValue("slopeAngle", 1.5);
	settings.setValue("lev", 0.95);
	settings.setValue("gaussianFilterSide", 5);
	settings.setValue("marg", 5);
	settings.endGroup();
	
	settings.beginGroup("ImageSignificantMask");
	settings.setValue("medFilterSide", 2);
	settings.setValue("gaussFilterSide", 3);
	settings.setValue("minFilterSide", 5);
	settings.setValue("dilate1RadiusVarMask", 5);
	settings.setValue("erodeRadiusVarMask", 35);
	settings.setValue("dilate2RadiusVarMask", 20);
	settings.setValue("maxCompNumVarMask", 2);
	settings.setValue("minCompThickVarMask", 75);
	settings.setValue("maxHolesNumVarMask", -1);
	settings.setValue("minHolesThickVarMask", 18);
	settings.setValue("binLevVarMask", 0.45);
	settings.setValue("histeresisThreshold1Gmask", 30); // in mathematica il threshold è 0.085 - consigliano un rapporto 1:2 - 1:3
	settings.setValue("histeresisThreshold2Gmask", 70);
	settings.setValue("radiusGaussFilterGmask", 10);
	settings.setValue("minMeanIntensityGmask", 0.2);
	settings.setValue("dilate1RadiusGmask", 10);
	settings.setValue("erodeRadiusGmask", 25);
	settings.setValue("dilate2RadiusGmask", 10);
	settings.setValue("maxCompNumGmask", 2);
	settings.setValue("minCompThickGmask", 75);
	settings.setValue("maxHolesNumGmask", -1);
	settings.setValue("minHolesThickGmask", 15);
	settings.setValue("histeresisThreshold3Gmask", 25); // in mathematica il threshold è 0.075 - consigliano un rapporto 1:2 - 1:3
	settings.setValue("histeresisThreshold4Gmask", 50);
	settings.setValue("dilate3RadiusGmask", 10);
	settings.setValue("erode2RadiusGmask", 5);
	settings.setValue("histeresisThreshold5Gmask", 45); // in mathematica il threshold è 0.095 - consigliano un rapporto 1:2 - 1:3
	settings.setValue("histeresisThreshold6Gmask", 90);
	settings.setValue("dilate4RadiusGmask", 4);
	settings.setValue("radiusGaussFilterComp", 30); // previously 20
	settings.setValue("meanIntensityCompThreshold", 0.6); // previously 0.25
	settings.setValue("dilateFinalRadius", 10);
	settings.setValue("erodeFinalRadius", 20);
	settings.setValue("smoothFinalRadius", 10);
	settings.setValue("maxCompNumFinal", 2);
	settings.setValue("minCompThickFinal", 75);
	settings.setValue("maxHolesNumFinal", 4);
	settings.setValue("minHolesThickFinal", 30);
	settings.setValue("fixedFrameWidth", 20);
	settings.setValue("smooth2FinalRadius", 20);
	settings.endGroup();
	
	settings.beginGroup("InitialOrientationEstimation");
	settings.setValue("radius", 10);//prev 15
	settings.setValue("radDirGrad", 1.0);
	settings.setValue("var1DirGrad", 1.0);
	settings.setValue("exp1DirGrad", 2.0);
	settings.setValue("var2DirGrad", 0.85);
	settings.setValue("exp2DirGrad", 2.0);
	settings.setValue("numAngles", 36);
	settings.setValue("weightGaussRadius", 1.0);
	settings.setValue("weightGaussExp", 4.0);
	settings.setValue("fieldGaussRadius", 0.5);
	settings.endGroup();
	
	settings.beginGroup("FrequencyEstimation");
	settings.setValue("radius", 15);
	settings.setValue("step", 10);
	settings.setValue("numSamples", 15);
	settings.setValue("minNormQuantile", 0.25);
    settings.setValue("gaussFilterSideVal", 3);
	settings.setValue("minValFreq", 5);
	settings.setValue("maxValFreq", 30);
	settings.setValue("leftQuantile", 0.05);
	settings.setValue("rightQuantile", 0.75);
	settings.endGroup();
	
	settings.beginGroup("OrientationRefinement");
	settings.setValue("OS_FIELD_NUM_SAMPLES", 20);
	settings.setValue("MASK_CONTRACTION_RAD_FAC", 2); // prev 3.5
	settings.setValue("OS_SM1_FIELD_RAD_FAC", 1.5);
	settings.setValue("OS_SM1_SMOOTH_RAD_FAC", 1);
	settings.setValue("OS_SM1_SMOOTH_STRENGTH", 1);
	settings.setValue("OS_SM1_DRIFT_RAD_FAC", 1);
	settings.setValue("OS_SM1_LD_THRESHOLD", 0.3);
	settings.setValue("OS_SM1_MASK_CONTRACTION_RAD_FAC", 3.5);
	settings.setValue("OS_SM1_MASK_BLUR_RAD_FAC", 2);
	settings.setValue("OS_SM2_FIELD_RAD_FAC", 0.5);
	settings.setValue("OS_SM2_ADJUST_RAD_FAC", 0.7);
	settings.setValue("OS_SM2_ADJUST_MASK_RESCALE", 0.9);
	settings.setValue("OS_SM2_ADJUST_RAD_FAC_2", 0.7);
	settings.setValue("OS_SM2_ADJUST_MASK_RESCALE_2", 0.9);
	settings.setValue("OS_SM2_DIFF_BLUR_RAD_FAC", 0.15);
	settings.setValue("OS_SM2_DIFF_THRESHOLD", 0.5);
	settings.setValue("OS_SM2_MIN_MAX_CENTROID_DIST_RAD_FAC", 0.5);// prev 1
	settings.setValue("OS_SM2_DIFF_DILATION_RAD_FAC", 1);
	settings.setValue("OS_SM3_DRIFT_RAD_FAC", 1);
	settings.setValue("OS_SM3_LD_BLUR_RAD_FAC", 1.5);
	settings.setValue("OS_SM3_LD_THRESHOLD", 0.1);
	settings.setValue("OS_SM3_LD_MIN_MAX_CENTROID_DIST_RAD_FAC", 0.75);
	settings.setValue("OS_SM_DIFF_DILATION2_RAD_FAC", 2);
	settings.setValue("OS_SM_DIFF_DILATION3_RAD_FAC", 2);
	settings.setValue("OS_IS_RAD_FAC", 1.5);
	settings.setValue("OS_IS_STRENGTH", 1);
	settings.setValue("OS_IS_INITIAL_LEV", 0.1);
	settings.setValue("OS_IS_MAX_STEPS", 10);
	settings.setValue("OS_IS_CONTRACTION_RAD_FAC", 0.1);
	settings.setValue("OS_IS_DILATION_RAD_FAC", 2);
	settings.setValue("OS_IS_BLUR_RAD_FAC", 2);
	settings.setValue("OS_IS_LEV_INCREMENT", 0.01);
	settings.endGroup();
}

void fa_orfo::mainObject::mainFunction(){
	// Initialize the command line parser
	QCommandLineParser parser;
	parser.setApplicationDescription("This tool is part of the FingAn Suite. It takes a fingerprint image as input and extract the orientation field.");
	parser.addHelpOption();
	parser.addVersionOption();
	parser.addPositionalArgument("indexfile", "A file containing the file path of each fingerprint to be processed. The first line of the file contains the number of fingerprints; each of the following lines has the format:\n<imagepath> <step> <border>\nwhere:\n<imagepath> is the file path of the fingerprint (bitmap file format);\n<step> is the step to be used to calculate the orientation image;\n<border> is the distance from the image border in pixels: this parameter determines the position of the top-left element. The parameter <step> and the number of sampling points are selected in a way that do not allow any sampling point to be closer to any of the borders than <border> pixels. This enables an algorithm to center, at each sampling point, a filter that extends up to <border> pixels in each direction. Note: you can assume that the border is not smaller than 14 pixels.");
	parser.addPositionalArgument("outputfolder", "The path of the folder where the output files have to be saved");
	
	// Parse the command line arguments
	QCoreApplication* app = QCoreApplication::instance();
	parser.process(*app);
	const QStringList args = parser.positionalArguments();
	if (args.length() != 2) throw SyntaxErrorEx("Syntax error: number of input arguments not valid.");
	const QString& indexFileName = args[0];
	const QString& outputFolderName = args[1];
	
	// Process the input file, and extract a list image paths
	QFile indexFile(indexFileName); // cosntruct the QFile instance to link to the stream
	if(!indexFile.open(QIODevice::ReadOnly | QIODevice::Text)) // open the file in read-only mode and check for success
		throw SyntaxErrorEx("Cannot open index file in read-only mode");
	QTextStream indexStream(&indexFile); // construct the stream to extract paths from
	int imageNum;
	indexStream >> imageNum; // extract the total number of images
	QList<QPair<QString, QPair<int, int>>> imageSpecs;
	for(int k = 0; k < imageNum; k++) {
		QString path;
		int step, border;
		indexStream >> path >> step >> border; // extract the current path, the step and the border from the stream
		QDir dir(path); // create a directory set to the read path
		if(!dir.isAbsolute()){ // if it is not absolute, prepend the index file path
			dir.setPath(indexFileName); // set the dir to the index file path
			if(!dir.cdUp()) continue; // go to the folder of the index file
			path = dir.absoluteFilePath(path); // search the image path inside the folder
		}
		imageSpecs << qMakePair(path, qMakePair(step, border)); // append the current values
	}
	
	// Set default parameters
	defaultParameters();
	
	// Launch a thread for each file found
	for (const QPair<QString, QPair<int, int>>& spec: imageSpecs) {
		// Create a new preprocessing instance
		FA::PreProcessing* preprocessing = new FA::PreProcessing;
		// Assign the name of the input file
		preprocessing->setFileName(spec.first);
		// Assign the name of the input mask
		QStringList temp = spec.first.split(".");
		temp.removeLast();
		QString inputMaskPath = temp.join(".").append(".fg");
		fm::DoublePrecision::Mask inputMask = FA::Utilities::FVC::toFullMat<uchar>(FA::Utilities::FVC::readFGfromFile(inputMaskPath),
																  spec.second.second, spec.second.second,
																  spec.second.first, spec.second.first, 0);
		preprocessing->setMask(inputMask);
		// Set border and step specifications
        preprocessing->setBdStep({spec.second.second, spec.second.first});
		// Assign the output path
		preprocessing->setOutputPath(outputFolderName);
		// Setup the orientation extraction algorithm
		new FA::FOEProcess(preprocessing);
		// Make the connection to save the results
		connect(preprocessing, SIGNAL(finished(FA::Algorithm*)),
				this, SLOT(saveResults(FA::Algorithm*)));
		connect(preprocessing, SIGNAL(destroyed()),
				this, SLOT(checkActiveProcesses()));
		// The Algorithm class is compatible with QThreadPool system
		QThreadPool::globalInstance()->start(preprocessing);
	}
}

void fa_orfo::mainObject::saveResults(FA::Algorithm* sender){
	FA::PreProcessing* pre = dynamic_cast<FA::PreProcessing*>(sender);
	if(pre != Q_NULLPTR){
		QString inputFileName = pre->getFileName();
		QDir inputFileNameDir(inputFileName);
		QDir outputPath(pre->getOutputPath());
		fm::DoublePrecision::Mask mask = pre->getFinalMask();
		cv::Rect rect = FA::Utilities::convertToRect(pre->getCroppingRect());
		QVector<QVariant> vec = pre->getBdStep();
		int border = vec[0].toInt();
		int step = vec[1].toInt();
		// Try to get the FOEProcess
		FA::FOEProcess* foe = pre->findChild<FA::FOEProcess*>();
		if(foe != Q_NULLPTR){
			fm::DoublePrecision::Field field = foe->getField();
            fm::DoublePrecision::Field fullField = fm::DoublePrecision::Field::zeros(mask.size());
            field.copyTo(fullField(rect));
			// Get the file name without extension from input file path
			QStringList tempList = inputFileNameDir.dirName().split(".");
			tempList.removeLast();
			// Get the output file path (output path + output file name)
			QString outFileName = outputPath.absoluteFilePath(tempList.join(".").append(".dirmap"));
			// Save results in FVC format
            FA::Utilities::FVC::writeFieldToFile(outFileName, mask, fullField, border, border, step, step);
			
            /*pre->saveResults();
			foe->setFileName(pre->getFileName());
			foe->setOutputPath(pre->getOutputPath());
            foe->saveResults();*/
		}
        FVCPipe() << inputFileName << "\n";
		pre->deleteLater();
	}
}

void fa_orfo::mainObject::checkActiveProcesses(){
	if(QThreadPool::globalInstance()->activeThreadCount() == 0)
		thread()->quit();
}
