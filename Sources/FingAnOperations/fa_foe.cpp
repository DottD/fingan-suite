#include <QCoreApplication>
#include <QTimer>
#include "fa_foeMainObject.hpp"

// Constants declarations
const QStringList availableExts = {"*.bmp", "*.dib", "*.jpeg", "*.jpg", "*.jpe", "*.jp2", "*.png", "*.webp", "*.pbm", "*.pgm", "*.ppm", "*.pxm", "*.pnm", "*.sr", "*.ras", "*.tiff", "*.tif", "*.exr", "*.hdr", "*.pic"};

int main(int argc, char* argv[]) {
	// Initialize application
	QCoreApplication app(argc, argv);
	QCoreApplication::setApplicationName("fa_foe");
	QCoreApplication::setOrganizationName("DMind");
	QCoreApplication::setApplicationVersion("1.0");
	fa_foe::mainObject* mainObj = new fa_foe::mainObject(&app);
	QTimer::singleShot(0, mainObj, SLOT(mainFunction())); // make the thread start at the beginning of the event loop
	return app.exec();
}
