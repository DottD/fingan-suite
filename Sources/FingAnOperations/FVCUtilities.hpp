#ifndef FVCUtilities_h
#define FVCUtilities_h

#include <Headers/FingAnField.hpp>
#include <Headers/FingAnInterface.hpp>

namespace FA {
	namespace Utilities {
		namespace FVC {
			
			/** Convert a matrix to full dimension, based on given specifications.
			 @param[in] border_x Number of columns left both on the right and on the left.
			 @param[in] border_y Number of rows left both on the top and on the bottom.
			 @param[in] step_x Horizontal step between two consecutive lines.
			 @param[in] step_y Vertical step between two consecutive lines.
			 @param[in] fillValue Value for the elements on the border.
			 @return The full dimension matrix.
			 */
			template<typename T>
			cv::Mat_<T> toFullMat(const cv::Mat_<T>& mat,
								  const int& border_x,
								  const int& border_y,
								  const int& step_x,
								  const int& step_y,
								  const T& fillValue){
				// Get the size of the full matrix
				cv::Size fullSize(2*border_x+step_x*(mat.cols-1), 2*border_y+step_y*(mat.rows-1));
				// Initialize a block
				cv::Rect block(border_x-step_x/2, border_y-step_y/2, step_x, step_y);
				// Initialize output matrix
				cv::Mat_<T> fullMat(fullSize);
				fullMat = fillValue;
				// Fill the two full matrices
				for(int y = 0; y < mat.rows; y++)
					for(int x = 0; x < mat.cols; x++)
						fullMat(block+cv::Point(x*step_x, y*step_y)) = mat(y, x);
				return fullMat;
			};
			
			/** Read a .fg file (FVC format).
			 @return The matrix read from file or an empty matrix if something went wrong.
			 @sa writeFGtoFile
			 */
			template <typename PrecisionPolicy = fm::DoublePrecision>
			typename PrecisionPolicy::Mask readFGfromFile(QString fileName) {
				typedef typename PrecisionPolicy::Uchar Uchar;
				typedef typename PrecisionPolicy::Mask Mask;
				// Check if the string is empty
				if(fileName.isEmpty())
					throw QtCustomException("FVC_readFGfromFile - File name is empty");
				// Check file existence
				QDir fileDir;
				if(!fileDir.exists(fileName))
					throw QtCustomException("FVC_readFGfromFile - .fg file does not exist");
				// Open file in read-only mode
				QFile file(fileDir.absoluteFilePath(fileName));
				if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
					throw QtCustomException("FVC_readFGfromFile - Cannot open file in read-only mode");
				// Open a text stream from the file
				QTextStream istream(&file);
				// Read the matrix dimensions
				int rows, cols, val;
				istream >> rows >> cols;
				Mask out(rows, cols);
				for(int k = 0; k < rows*cols; k++){
					istream >> val;
					out(k) = Uchar(val);
				}
				return out;
			}
			
			/** Save a .fg to file (FVC format).
			 @return true if everything was ok.
			 @sa readFGfromFile
			 */
			template <typename PrecisionPolicy = fm::DoublePrecision>
			void writeFGtoFile(QString fileName,
							   const typename PrecisionPolicy::Mask mat) {
				// Check if the string is empty
				if(fileName.isEmpty())
					throw QtCustomException("FVC_writeFGtoFile - File name is empty");
				// Open file in write-only mode
				QFile file(fileName);
				if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
					throw QtCustomException("FVC_writeFGtoFile - Cannot open file in write-only mode");
				// Open a text stream from the file
				QTextStream ostream(&file);
				// Read the matrix dimensions
				ostream << mat.rows << " " << mat.cols << endl;
				for(int i = 0; i < mat.rows; i++){
					for(int j = 0; j < mat.cols; j++){
						ostream << mat(i,j) << " ";
					}
					ostream << endl;
				}
			}
			
			/** Read a binary file with field information (FVC format). */
			template <typename PrecisionPolicy = fm::DoublePrecision>
			void readFieldFromFile(QString fileName,
								   typename PrecisionPolicy::Mask& mask,
								   typename PrecisionPolicy::Field& squareField){
				typedef typename PrecisionPolicy::Mask Mask;
				typedef typename PrecisionPolicy::Field Field;
				typedef typename PrecisionPolicy::Uchar Uchar;
				typedef typename PrecisionPolicy::Real Real;
				typedef typename PrecisionPolicy::Complex Complex;
				// Check if the string is empty
				if(fileName.isEmpty())
					throw QtCustomException("FVC_readFieldFromFile - File name is empty");
				// Open file in read-only mode
				QFile file(fileName);
				if(!file.open(QIODevice::ReadOnly))
					throw QtCustomException("FVC_readFieldFromFile - Cannot open file in read-only mode");
				// Define a data stream
				QDataStream istream(&file);
				// Skip the fixed header
				istream.skipRawData(8); // header: DIRIMG00
				// Read the 6 integers that define the orientation matrix
				int border_x, border_y, step_x, step_y, cols, rows;
				istream >> border_x >> border_y >> step_x >> step_y >> cols >> rows;
				quint8 phaseVal, maskVal;
				Mask smallMask(rows, cols);
				Field smallField(rows, cols);
				std::complex<Real> z;
				for(int k = 0; k < rows*cols; k++){
					// Read the phase value
					istream >> phaseVal >> maskVal;
					z = std::exp(std::complex<Real>(0, 2.0*Real(phaseVal)*CV_PI/255.0));
					smallField(k)[0] = z.real();
					smallField(k)[1] = z.imag();
					smallMask(k) = (maskVal > 0) ? 255 : 0;
				}
				// Convert the two matrices to full dimension ones
				squareField = toFullMat<Complex>(smallField, border_x, border_y, step_x, step_y, Complex(0,0));
				mask = toFullMat<Uchar>(smallMask, border_x, border_y, step_x, step_y, 0);
			}
			
			/** Write a binary file with field information (FVC format). */
			template <typename PrecisionPolicy = fm::DoublePrecision>
			void writeFieldToFile(QString fileName,
								  const typename PrecisionPolicy::Mask mask,
								  const typename PrecisionPolicy::Field squaredField,
								  const int& border_x,
								  const int& border_y,
								  const int& step_x,
								  const int& step_y){
				typedef typename PrecisionPolicy::Mask Mask;
				typedef typename PrecisionPolicy::Int Int;
				typedef typename PrecisionPolicy::Real Real;
				typedef typename PrecisionPolicy::Uchar Uchar;
				// Check if the string is empty
				if(fileName.isEmpty())
					throw QtCustomException("FVC_writeFieldToFile - File name is empty");
				// Compare mask and field dimensions
				if(mask.size() != squaredField.size())
					throw QtCustomException("FVC_writeFieldToFile - Mask and field size not equal");
				// Get the size of a small matrix
				cv::Size smallSize((mask.cols-2*border_x)/step_x + 1, (mask.rows-2*border_y)/step_y + 1);
				// Initialize the small matrices
				Mask smallMask(smallSize);
				Mask smallPhase(smallSize);
				// Reduce the two full dimension matrices
				const Real M_2PI = 2.0*CV_PI;
                auto mathMod = [](Real x, Real D){
                    return x-std::floor(x/D)*D;
                };
				typename Mask::iterator maskit = smallMask.begin();
				typename Mask::iterator phaseit = smallPhase.begin();
				for(int y = border_y; y <= squaredField.rows-border_y; y += step_y){
					for(int x = border_x; x <= squaredField.cols-border_x; x += step_x){
						*(maskit++) = mask(y, x);
						if (mask(y, x) == 0) *(phaseit++) = 0;
						else {
							std::complex<Real> z(squaredField(y, x)[0], squaredField(y, x)[1]);
                                                        *(phaseit++) = cv::saturate_cast<Uchar>(mathMod(arg(z)/2.0+M_PI_2, M_2PI)/M_PI*255.0);
						}
					}
				}
				// Open file in write-only mode
				QFile file(fileName);
				if(!file.open(QIODevice::WriteOnly))
					throw QtCustomException("FVC_writeFieldToFile - Cannot open file in write-only mode");
				// Define a data stream
				QDataStream ostream(&file);
				// Write the fixed header
                                ostream.writeRawData("DIRIMG00", 8);
				// Write the 6 integers that define the orientation matrix
                                int specs[6] = {border_x, border_y, step_x, step_y, smallSize.width, smallSize.height};
                                ostream.writeRawData((const char*)specs, 6*sizeof(int));
				// Write the matrices
                                std::vector<unsigned char> buffer(smallMask.total()*2);
                for(Int k = 0; k < (Int)smallMask.total(); k++){
                                        buffer[2*k] = smallPhase(k);
                                        buffer[2*k+1] = smallMask(k);
				}
                                ostream.writeRawData((const char*)buffer.data(), buffer.size()*sizeof(unsigned char));
			}
		}
	}
}

#endif /* FVCUtilities_h */
