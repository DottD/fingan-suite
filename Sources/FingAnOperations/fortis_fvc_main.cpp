#include "fortis_fvc_main.hpp"

FORTIS::byte* FORTIS::LoadGrayScaleBitmap(FILE* fp, int* imageWidth, int* imageHeight)
{
    BITMAPFILEHEADER bfh;
    BITMAPINFOHEADER bih;
    int pad_row, i;
    byte tmp[4];
    byte* pimage;
    byte *buffer;

    if (fread(&bfh,sizeof(BITMAPFILEHEADER),1,fp)!=1)
        return NULL;
    if (memcmp(&(bfh.bfType),"BM",2)!=0)
        return NULL;
    if (fread(&bih,sizeof(BITMAPINFOHEADER),1,fp)!=1)
        return NULL;
    if (bih.biCompression!=BI_RGB || bih.biBitCount!=8 || bih.biPlanes!=1)
        return NULL;

    *imageWidth=bih.biWidth;
    *imageHeight=bih.biHeight;
    buffer = (byte*)malloc(bih.biWidth*bih.biHeight);
    pad_row=(4-(bih.biWidth%4))%4;
    fseek(fp,bfh.bfOffBits,SEEK_SET);

    pimage=buffer+bih.biWidth*(bih.biHeight-1);
    for (i=0;i<bih.biHeight;i++)
    {
        if (fread(pimage,bih.biWidth,1,fp)!=1)
        {
            free(buffer);
            return NULL;
        }
        if (pad_row>0)
        {
            if (fread(tmp,pad_row,1,fp)!=1)
            {
                free(buffer);
                return NULL;
            }
        }
        pimage-=bih.biWidth;
    }

    return buffer;
}

FORTIS::byte FORTIS::SaveOrientationsToFile(char* path, byte** orientations, int rows, int columns, int border, int step, byte** foreground)
{
#define fileHeader "DIRIMG00"
    byte strengthOne = 255;
    byte strengthZero = 0;
    int x,y;
    FILE *f = fopen(path,"wb");
    if (f==NULL)
        return 0;
    fwrite(fileHeader,strlen(fileHeader),1,f);
    fwrite(&border,sizeof(int),1,f);   // border x
    fwrite(&border,sizeof(int),1,f);   // border y
    fwrite(&step,sizeof(int),1,f);     // step x
    fwrite(&step,sizeof(int),1,f);     // step y
    fwrite(&columns,sizeof(int),1,f);
    fwrite(&rows,sizeof(int),1,f);
    for (y = 0; y < rows; y++)
    {
        for (x = 0; x < columns; x++)
        {
            fwrite(&(orientations[y][x]),sizeof(byte),1,f);    // the orientation (stored as a byte)
            fwrite(foreground[y][x]?&strengthOne:&strengthZero,sizeof(byte),1,f);    // the strength (not considered in this evaluation)
        }
    }
    fclose(f);
    return 1;
}

FORTIS::FORTIS(QObject* parent): QObject(parent) {};

void FORTIS::main() {
#define maxLineLen 100

    QStringList argv = QCoreApplication::arguments();
    int argc = argv.count();
    const char* pipeName = "\\\\.\\pipe\\PipeForFVCOrientation";
    char indexLine[maxLineLen+1];

    // Define stuff useful to save the orientations
    const fm::DoublePrecision::Real M_2PI = 2.0*CV_PI;
    auto mathMod = [](fm::DoublePrecision::Real x, fm::DoublePrecision::Real D){
        return x-std::floor(x/D)*D;
    };

    // Check input parameters
    if (argc != 3) {
        printf("Syntax error.\nUse: Extractor <index file> <output folder>\n");
        QCoreApplication::instance()->exit(SyntaxError);
    }

    // Manages the index file
    FILE *fIndex = fopen(argv[1].toStdString().c_str(),"rt");
    int imagesCount;
    if (fIndex==NULL || !fgets(indexLine,maxLineLen,fIndex) || sscanf(indexLine,"%d",&imagesCount)!=1) {
        printf("Index file format error.\n");
        QCoreApplication::instance()->exit(SyntaxError);
    }

    // Output path
    const char* outputPath = argv[2].toStdString().c_str();

    // connects to the named pipe
    HANDLE hNamedPipe = CreateFileA(pipeName, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
    // Note: during debugging you may comment out pipe-related code to avoid having to open the pipe during debugging
    if (hNamedPipe==INVALID_HANDLE_VALUE) {
        printf("Cannot open named pipe. Last error: %d\n",GetLastError());
        QCoreApplication::instance()->exit(CannotOpenPipe);
    }

    // Executes the algorithm on all the images
    defaultParameters();
    for (int imageIdx = 1; imageIdx <= imagesCount; ++imageIdx) {
        // Parses the index line
        char imagePath[maxLineLen+1];
        int step, border;
        if (!fgets(indexLine, maxLineLen, fIndex) || sscanf(indexLine,"%s %d %d",imagePath,&step, &border)!=3) {
            printf("Index file format error.\n");
            QCoreApplication::instance()->exit(SyntaxError);
        }

        // a foreground file must exists for each image
        char fgPath[maxLineLen+1];
        strcpy(fgPath, imagePath);
        strcpy(fgPath+strlen(fgPath)-3, "fg");

        // reads the foreground file
        int fgRows, fgColumns;
        FILE* fg = fopen(fgPath, "rt");
        if (fg == NULL)
            QCoreApplication::instance()->exit(CannotOpenForegroundFile);
        if (fscanf(fg,"%d %d", &fgRows, &fgColumns)!=2)
            QCoreApplication::instance()->exit(CannotOpenForegroundFile);
        byte** foreground = (byte**)malloc(sizeof(byte*)*fgRows);
        for (int y = 0; y < fgRows; y++) {
            foreground[y] = (byte*)malloc(fgColumns);
            for (int x = 0; x < fgColumns; x++) {
                int val;
                if (fscanf(fg,"%d",&val)!=1)
                    QCoreApplication::instance()->exit(CannotOpenForegroundFile);
                foreground[y][x] = (byte)val;
            }
        }
        fclose(fg);

        // Loads the input image
        FILE* fImg = fopen(imagePath,"rb");
        if (fImg == NULL)
            QCoreApplication::instance()->exit(CannotOpenImageFile);
        int imageWidth, imageHeight;
        byte* image = LoadGrayScaleBitmap(fImg, &imageWidth, &imageHeight);
        fclose(fImg);
        if (image == NULL)
            QCoreApplication::instance()->exit(CannotOpenImageFile);

        // Compute the orientations
        // Create a new preprocessing instance
        FA::PreProcessing preprocessing;
        // Assign the image to the process
        cv::Mat_<byte> cvimageu(imageHeight, imageWidth, image);
        fm::DoublePrecision::Image cvimage;
        cvimageu.convertTo(cvimage, CV_64FC1);
        preprocessing.setInitialImage(cvimage);
        // Assign the input mask
        cv::Mat_<byte> cvMaskSmallByte(fgRows, fgColumns);
        for(int i = 0; i < fgRows; ++i){
            for(int j = 0; j < fgColumns; ++j){
                cvMaskSmallByte(i,j) = foreground[i][j];
            }
        }
        fm::DoublePrecision::Mask cvMaskSmall;
        cvMaskSmallByte.convertTo(cvMaskSmall, CV_8UC1);
        fm::DoublePrecision::Mask inputMask = FA::Utilities::FVC::toFullMat<fm::DoublePrecision::Uchar>
                (cvMaskSmall, border, border, step, step, 0);
        preprocessing.setMask(inputMask);
        // Set border and step specifications
        preprocessing.setBdStep({border, step});
        // perform the preprocessing
        try {
            std::cout << "Preprocessing " << imagePath << std::endl;
            preprocessing.run();
        } catch (QtCustomException& e) {
            std::cout << "Preprocessing, My error: " << e.message().toStdString() << std::endl;
            QCoreApplication::instance()->exit(InternalError);
        } catch (std::exception& e) {
            std::cout << "Preprocessing, Third party error: " << e.what() << std::endl;
            QCoreApplication::instance()->exit(InternalError);
        } catch (...) {
            std::cout << "Preprocessing, Unknown error!!" << std::endl;
            QCoreApplication::instance()->exit(InternalError);
        }

        // Setup the orientation extraction algorithm
        FA::FOEProcess foeprocess;
        // Make the connection
        foeprocess.setInput(preprocessing.getOutput());
        // perform the orientations extraction
        try {
            std::cout << "Fingerprint orientation extraction on " << imagePath << std::endl;
            foeprocess.run();
        } catch (QtCustomException& e) {
            std::cout << "FOEProcess, My error: " << e.message().toStdString() << std::endl;
            QCoreApplication::instance()->exit(InternalError);
        } catch (std::exception& e) {
            std::cout << "FOEProcess, Third party error: " << e.what() << std::endl;
            QCoreApplication::instance()->exit(InternalError);
        } catch (...) {
            std::cout << "FOEProcess, Unknown error!!" << std::endl;
            QCoreApplication::instance()->exit(InternalError);
        }
        // fill the orientations array
        fm::DoublePrecision::Field fullField; {
            fm::DoublePrecision::Field field = foeprocess.getField();
            fm::DoublePrecision::Mask mask = preprocessing.getFinalMask();
            cv::Rect rect = FA::Utilities::convertToRect(preprocessing.getCroppingRect());
            fullField = fm::DoublePrecision::Field::zeros(mask.size());
            field.copyTo(fullField(rect));
        }

        // Convert orientations and save them in FVC compliant format
        // Allocate memory for the array
        byte** orientations = (byte**)malloc(sizeof(byte*)*fgRows);
        for (int y = 0; y < fgRows; y++) {
            // Allocate memory for each array row
            orientations[y] = (byte*)malloc(fgColumns);
            // Set each row value to zero
            ZeroMemory(orientations[y],fgColumns);
            // Convert the orientations to uchar values
            for (int x = 0; x < fgColumns; x++) {
                if (foreground[y][x]) {
                    int px = border + x * step;
                    int py = border + y * step;

                    std::complex<fm::DoublePrecision::Real> z(fullField(py, px)[0], fullField(py, px)[1]);
                    orientations[y][x] = cv::saturate_cast<fm::DoublePrecision::Uchar>(mathMod(arg(z)/2.0+M_PI_2, M_2PI)/M_PI*255.0);
                }
            }
        }

        // Saves the result
        char fileName[maxLineLen+1];
        char outputFilePath[maxLineLen+1];
        _splitpath(imagePath, NULL, NULL, fileName, NULL);
        sprintf(outputFilePath, "%s\\%s.dirmap", outputPath, fileName);
        if (!SaveOrientationsToFile(outputFilePath, orientations, fgRows, fgColumns, border, step, foreground))
            QCoreApplication::instance()->exit(CannotSaveOrientations);

        for (int y = 0; y < fgRows; y++) {
            free(orientations[y]);
            free(foreground[y]);
        }
        free(orientations);
        free(foreground);
        free(image);

        // Writes the image path to the named pipe to inform the caller about the progress
        char outputLine[maxLineLen+1];
        DWORD nWritten;
        sprintf(outputLine,"%s\n",imagePath);
        WriteFile(hNamedPipe,outputLine,strlen(outputLine),&nWritten,NULL);
    }

    QCoreApplication::instance()->exit(Success);
}

void FORTIS::defaultParameters(){
    settings.beginGroup("ImageCroppingSimple");
    settings.setValue("minVariation", 0.01);
    settings.setValue("marg", 5);
    settings.endGroup();

    settings.beginGroup("ImageBimodalize");
    settings.setValue("brightness", 0.35);
    settings.setValue("leftCut", 0.25);
    settings.setValue("rightCut", 0.5);
    settings.setValue("histSmooth", 25);
    settings.setValue("reparSmooth", 10);
    settings.endGroup();

    settings.beginGroup("ImageEqualize");
    settings.setValue("minMaxFilter", 5);
    settings.setValue("mincp1", 0.75);
    settings.setValue("mincp2", 0.9);
    settings.setValue("maxcp1", 0.0);
    settings.setValue("maxcp2", 0.25);
    settings.endGroup();

    settings.beginGroup("TopMask");
    settings.setValue("scanAreaAmount", 0.1);
    settings.setValue("gradientFilterWidth", 0.25);
    settings.setValue("binarizationLevel", 0.2);
    settings.setValue("f", 3.5);
    settings.setValue("slopeAngle", 1.5);
    settings.setValue("lev", 0.95);
    settings.setValue("gaussianFilterSide", 5);
    settings.setValue("marg", 5);
    settings.endGroup();

    settings.beginGroup("ImageSignificantMask");
    settings.setValue("medFilterSide", 2);
    settings.setValue("gaussFilterSide", 3);
    settings.setValue("minFilterSide", 5);
    settings.setValue("dilate1RadiusVarMask", 5);
    settings.setValue("erodeRadiusVarMask", 35);
    settings.setValue("dilate2RadiusVarMask", 20);
    settings.setValue("maxCompNumVarMask", 2);
    settings.setValue("minCompThickVarMask", 75);
    settings.setValue("maxHolesNumVarMask", -1);
    settings.setValue("minHolesThickVarMask", 18);
    settings.setValue("binLevVarMask", 0.45);
    settings.setValue("histeresisThreshold1Gmask", 30); // in mathematica il threshold è 0.085 - consigliano un rapporto 1:2 - 1:3
    settings.setValue("histeresisThreshold2Gmask", 70);
    settings.setValue("radiusGaussFilterGmask", 10);
    settings.setValue("minMeanIntensityGmask", 0.2);
    settings.setValue("dilate1RadiusGmask", 10);
    settings.setValue("erodeRadiusGmask", 25);
    settings.setValue("dilate2RadiusGmask", 10);
    settings.setValue("maxCompNumGmask", 2);
    settings.setValue("minCompThickGmask", 75);
    settings.setValue("maxHolesNumGmask", -1);
    settings.setValue("minHolesThickGmask", 15);
    settings.setValue("histeresisThreshold3Gmask", 25); // in mathematica il threshold è 0.075 - consigliano un rapporto 1:2 - 1:3
    settings.setValue("histeresisThreshold4Gmask", 50);
    settings.setValue("dilate3RadiusGmask", 10);
    settings.setValue("erode2RadiusGmask", 5);
    settings.setValue("histeresisThreshold5Gmask", 45); // in mathematica il threshold è 0.095 - consigliano un rapporto 1:2 - 1:3
    settings.setValue("histeresisThreshold6Gmask", 90);
    settings.setValue("dilate4RadiusGmask", 4);
    settings.setValue("radiusGaussFilterComp", 30); // previously 20
    settings.setValue("meanIntensityCompThreshold", 0.6); // previously 0.25
    settings.setValue("dilateFinalRadius", 10);
    settings.setValue("erodeFinalRadius", 20);
    settings.setValue("smoothFinalRadius", 10);
    settings.setValue("maxCompNumFinal", 2);
    settings.setValue("minCompThickFinal", 75);
    settings.setValue("maxHolesNumFinal", 4);
    settings.setValue("minHolesThickFinal", 30);
    settings.setValue("fixedFrameWidth", 20);
    settings.setValue("smooth2FinalRadius", 20);
    settings.endGroup();

    settings.beginGroup("InitialOrientationEstimation");
    settings.setValue("radius", 15);
    settings.setValue("radDirGrad", 1.0);
    settings.setValue("var1DirGrad", 1.0);
    settings.setValue("exp1DirGrad", 2.0);
    settings.setValue("var2DirGrad", 0.85);
    settings.setValue("exp2DirGrad", 2.0);
    settings.setValue("numAngles", 36);
    settings.setValue("weightGaussRadius", 1.0);
    settings.setValue("weightGaussExp", 4.0);
    settings.setValue("fieldGaussRadius", 0.5);
    settings.endGroup();

    settings.beginGroup("FrequencyEstimation");
    settings.setValue("radius", 15);
    settings.setValue("step", 10);
    settings.setValue("numSamples", 15);
    settings.setValue("minNormQuantile", 0.25);
    settings.setValue("gaussFilterSideVal", 3);
    settings.setValue("minValFreq", 5);
    settings.setValue("maxValFreq", 30);
    settings.setValue("leftQuantile", 0.05);
    settings.setValue("rightQuantile", 0.75);
    settings.endGroup();

    settings.beginGroup("OrientationRefinement");
    settings.setValue("OS_FIELD_NUM_SAMPLES", 15);
    settings.setValue("OS_SM1_FIELD_RAD_FAC", 1.5);
    settings.setValue("OS_SM1_SMOOTH_RAD_FAC", 1);
    settings.setValue("OS_SM1_SMOOTH_STRENGTH", 1);
    settings.setValue("OS_SM1_DRIFT_RAD_FAC", 1);
    settings.setValue("OS_SM1_LD_THRESHOLD", 0.3);
    settings.setValue("OS_SM1_MASK_CONTRACTION_RAD_FAC", 3.5);
    settings.setValue("OS_SM1_MASK_BLUR_RAD_FAC", 2);
    settings.setValue("OS_SM2_FIELD_RAD_FAC", 0.5);
    settings.setValue("OS_SM2_ADJUST_RAD_FAC", 0.7);
    settings.setValue("OS_SM2_ADJUST_MASK_RESCALE", 0.9);
    settings.setValue("OS_SM2_ADJUST_RAD_FAC_2", 0.7);
    settings.setValue("OS_SM2_ADJUST_MASK_RESCALE_2", 0.9);
    settings.setValue("OS_SM2_DIFF_BLUR_RAD_FAC", 0.15);
    settings.setValue("OS_SM2_DIFF_THRESHOLD", 0.5);
    settings.setValue("OS_SM2_MIN_MAX_CENTROID_DIST_RAD_FAC", 0.5);// prev 1
    settings.setValue("OS_SM2_DIFF_DILATION_RAD_FAC", 1);
    settings.setValue("OS_SM3_DRIFT_RAD_FAC", 1);
    settings.setValue("OS_SM3_LD_BLUR_RAD_FAC", 1.5);
    settings.setValue("OS_SM3_LD_THRESHOLD", 0.1);
    settings.setValue("OS_SM3_MASK_CONTRACTION_RAD_FAC", 3.5);
    settings.setValue("OS_SM3_LD_MIN_MAX_CENTROID_DIST_RAD_FAC", 0.75);
    settings.setValue("OS_SM_DIFF_DILATION2_RAD_FAC", 2);
    settings.setValue("OS_SM_DIFF_DILATION3_RAD_FAC", 2);
    settings.setValue("OS_IS_RAD_FAC", 1.5);
    settings.setValue("OS_IS_STRENGTH", 1);
    settings.setValue("OS_IS_INITIAL_LEV", 0.1);
    settings.setValue("OS_IS_MAX_STEPS", 10);
    settings.setValue("OS_IS_CONTRACTION_RAD_FAC", 0.1);
    settings.setValue("OS_IS_DILATION_RAD_FAC", 2);
    settings.setValue("OS_IS_BLUR_RAD_FAC", 2);
    settings.setValue("OS_IS_LEV_INCREMENT", 0.01);
    settings.endGroup();
}
