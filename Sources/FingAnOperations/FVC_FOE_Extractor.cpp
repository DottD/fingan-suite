#include <QSettings>
#include <QCoreApplication>
#include <QTimer>
#include "fortis_fvc_main.hpp"

int main(int argc, char* argv[]) {
    // Initialize application
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("FORTIS");
    QCoreApplication::setOrganizationName("DMind");
    QCoreApplication::setApplicationVersion("1.0");
    FORTIS* fortisMain = new FORTIS(&app);
    // make the thread start at the beginning of the event loop
    QTimer::singleShot(0, fortisMain, SLOT(main()));
    return app.exec();
}
