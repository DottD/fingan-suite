#ifndef FORTIS_FVC_MAIN_H
#define FORTIS_FVC_MAIN_H

#include <stdio.h>
#include <windows.h>
#include <QSettings>
#include <QDebug>
#include <QStringList>
#include <Headers/FingAnInterface.hpp>
#include "FVCUtilities.hpp"

class FORTIS : public QObject {
    Q_OBJECT

public:
    typedef enum {
        Success = 0,
        SyntaxError = 1,
        CannotOpenImageFile = 2,
        CannotOpenForegroundFile = 3,
        CannotOpenPipe = 4,
        CannotSaveOrientations = 5,
        InternalError = 6 // my own definition
    } ExtractorResult;
    typedef unsigned char byte;

    QSettings settings;

    FORTIS(QObject* parent = Q_NULLPTR);

    /** Set the parameters to their default values. */
    void defaultParameters();

    byte* LoadGrayScaleBitmap(FILE* fp, int* imageWidth, int* imageHeight);

    byte SaveOrientationsToFile(char* path, byte** orientations, int rows, int columns, int border, int step, byte** foreground);

public Q_SLOTS:
    /** The main function: where everything begins. */
    Q_SLOT void main();
};

#endif // FORTIS_FVC_MAIN_H
