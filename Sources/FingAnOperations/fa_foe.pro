#Main Qt project file (for win32 static release compilation)

#Instruct to compile a console application
TEMPLATE = app
CONFIG += console release c++14 static

#Link OpenCV
INCLUDEPATH += C:\Libs\OpenCV\include
LIBS += "C:\Libs\OpenCV\x86\vc15\staticlib\*.lib"

#Name the output file
TARGET = fa_foe

#Instruct about build folder
BUILD_DIR = ../../Build
DESTDIR = $$BUILD_DIR

#Add the dependencies
DEPENDPATH += $$BUILD_DIR
INCLUDEPATH += ../FingAnCoreLib ../FingAnInterfaceLib
LIBS += -L$$BUILD_DIR -lfa_core_lib -lfa_interface_lib

#Add sources and headers
HEADERS += fa_foeMainObject.hpp
SOURCES += fa_foe.cpp fa_foeMainObject.cpp
