#include <QCoreApplication>
#include <QTimer>
#include "fa_improveMainObject.hpp"

int main(int argc, char* argv[]) {
	// Initialize application
	QCoreApplication app(argc, argv);
	QCoreApplication::setApplicationName("fa_improve");
	QCoreApplication::setOrganizationName("DMind");
	QCoreApplication::setApplicationVersion("1.0");
	fa_improve::mainObject* mainObj = new fa_improve::mainObject(&app);
	QTimer::singleShot(0, mainObj, SLOT(mainFunction())); // make the thread start at the beginning of the event loop
	return app.exec();
}
