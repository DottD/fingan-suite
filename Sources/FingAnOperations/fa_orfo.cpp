#include <chrono>
#include <debugLib.hpp>
#include <QCoreApplication>
#include <QTimer>
#include "fa_orfoMainObject.hpp"

typedef enum {
	Success = 0,
	SyntaxError = 1,
	CannotOpenImageFile = 2,
	CannotOpenForegroundFile = 3,
	CannotOpenPipe = 4,
	CannotSaveOrientations = 5,
} ExtractorResult;

class Application final : public QCoreApplication {
public:
    Application(int& argc, char** argv) : QCoreApplication(argc, argv) {}
    virtual bool notify(QObject *receiver, QEvent *event) override {
        try {
            return QCoreApplication::notify(receiver, event);
        } catch(fa_orfo::SyntaxErrorEx&) {
            this->exit(SyntaxError);
        } catch(fa_orfo::CannotOpenImageFileEx&) {
            this->exit(CannotOpenImageFile);
        } catch(fa_orfo::CannotOpenForegroundFileEx&) {
            this->exit(CannotOpenForegroundFile);
        } catch(fa_orfo::CannotOpenPipeEx&) {
            this->exit(CannotOpenPipe);
        } catch(fa_orfo::CannotSaveOrientationsEx&) {
            this->exit(CannotSaveOrientations);
        } catch(QtCustomException& exc) {
            FVCPipe() << exc.message();
            this->exit(SyntaxError);
        } catch (std::exception& exc) {
            FVCPipe() << exc.what();
            this->exit(SyntaxError);
        } catch (...) {
            std::cout << "Unknown error!!" << std::endl;
            FVCPipe() << "Unknown error!!";
            this->exit(SyntaxError);
        }

        return false;
    }
};

// Constants declarations
const QStringList availableExts = {"*.bmp", "*.dib", "*.jpeg", "*.jpg", "*.jpe", "*.jp2", "*.png", "*.webp", "*.pbm", "*.pgm", "*.ppm", "*.pxm", "*.pnm", "*.sr", "*.ras", "*.tiff", "*.tif", "*.exr", "*.hdr", "*.pic"};

int main(int argc, char* argv[]) {
    FVCPipe().reset();
	// Set up for timing
    /*std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    std::time_t start_time = std::chrono::system_clock::to_time_t(start);
    FVCPipe() << "Started computation at " << QString(std::ctime(&start_time));*/
	// Initialize application
    Application app(argc, argv);
    Application::setApplicationName("fa_orfo");
    Application::setOrganizationName("DMind");
    Application::setApplicationVersion("1.0");
	fa_orfo::mainObject* mainObj = new fa_orfo::mainObject(&app);
	QTimer::singleShot(0, mainObj, SLOT(mainFunction())); // make the thread start at the beginning of the event loop
    int returnCode = app.exec();
	// Read timing and output to stream
    /*end = std::chrono::system_clock::now();
	int elapsed_seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);
    FVCPipe() << "Finished computation at " << QString(std::ctime(&end_time));
    FVCPipe() << "Elapsed time: " << QLocale().toString(elapsed_seconds) << " ms";*/
	// Exit
    return returnCode;
}
