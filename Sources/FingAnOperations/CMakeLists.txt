#---This is the FingAnOperations CMakeLists.txt

cmake_minimum_required(VERSION 2.8)

#---Create a new subproject
project(FingAnOperationsProj)

#---Include the headers into the search path of the project
include_directories(
	.
	../DebugLib/
	../FingAnCoreLib/
	../FingAnInterfaceLib/
	${OpenCV_INCLUDE_DIRS})

#---Search for header files
file(GLOB OTHER FVCUtilities.hpp FVCPipe.hpp FVCPipe.cpp)


#---Instruct CMake to automatically run the moc
set(CMAKE_AUTOMOC ON)

#---Instruct the linker where to find the Alglib libraries (before target creation)
link_directories(${ALGLIB_LIBRARY_DIRS})

#---Set up the list of subprograms to generate
set(FA_OPERATIONS "fa_orfo") #orientation refinement through field operators / missing "fa_improve" "fa_foe"

#---Set common properties
set(FA_OPERATIONS_INCLUDES 
	.
	../DebugLib/
	../FingAnCoreLib/
	../FingAnInterfaceLib/
	${OpenCV_INCLUDE_DIRS}
	${ALGLIB_INCLUDE_DIRS}
	${ARMADILLO_INCLUDE_DIRS})
set(FA_OPERATIONS_LIBRARIES
	Qt5::Core
	FingAnCore FingAnInterface DebugLib
	${OpenCV_LIBS}
	${ALGLIB_LIBRARIES}
	${ARMADILLO_LIBRARIES})

#---Create executables
foreach(operation IN LISTS FA_OPERATIONS)
	add_executable(${operation} "${operation}.cpp" "${operation}MainObject.cpp" "${operation}MainObject.hpp" ${OTHER})
	target_include_directories(${operation} PUBLIC ${FA_OPERATIONS_INCLUDES})
	target_link_libraries(${operation} ${FA_OPERATIONS_LIBRARIES})
	#---Add C++14 support to the project
	set_property(TARGET ${operation} PROPERTY CXX_STANDARD 14)
	message(STATUS "Generated: ${operation} executable")
endforeach(operation)
