#Main Qt project file (for win32 static release compilation)

#Instruct to compile a console application
TEMPLATE = app
CONFIG += console release c++14 static
QT -= gui

#Link OpenCV
INCLUDEPATH += C:/Libs/OpenCV/include
LIBS += "C:/Libs/OpenCV/x86/vc15/staticlib/*.lib"

#Link Armadillo
INCLUDEPATH += C:/Libs/armadillo/include
LIBS += $$files(C:/Libs/armadillo/lib/*.lib, true)

#Link ALGLIB
INCLUDEPATH += C:/Libs/alglib
LIBS += $$files(C:/Libs/alglib/lib/*.lib, true)

#Name the output file
TARGET = Extractor

#Instruct about build folder
DESTDIR = $$BLD_DIR

#Add the dependencies
DEPENDPATH += $$BLD_DIR
INCLUDEPATH += $$SRC_DIR/FingAnCoreLib $$SRC_DIR/FingAnFieldLib $$SRC_DIR/FingAnInterfaceLib
LIBS += -L$$BLD_DIR -lfa_core_lib -lfa_interface_lib

#Add sources and headers
HEADERS += FVCUtilities.hpp \
    fortis_fvc_main.hpp
SOURCES += FVC_FOE_Extractor.cpp \
    fortis_fvc_main.cpp
