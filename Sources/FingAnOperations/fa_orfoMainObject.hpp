#ifndef fa_orfoMainObject_hpp
#define fa_orfoMainObject_hpp

#include <QObject>
#include <QThread>
#include <QThreadPool>
#include <QCommandLineParser>
#include <QSettings>
#include <QSharedPointer>
#include <QStringList>
#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QDebug>
#include <Headers/FingAnInterface.hpp>
#include "FVCUtilities.hpp"
#include "FVCPipe.hpp"

namespace fa_orfo {
	class mainObject;
	
	class SyntaxErrorEx : public QtCustomException {
	public:
		SyntaxErrorEx(QString msg): QtCustomException(msg) {};
	};
	class CannotOpenImageFileEx : public QtCustomException {
	public:
		CannotOpenImageFileEx(QString msg): QtCustomException(msg) {};
	};
	class CannotOpenForegroundFileEx : public QtCustomException {
	public:
		CannotOpenForegroundFileEx(QString msg): QtCustomException(msg) {};
	};
	class CannotOpenPipeEx : public QtCustomException {
	public:
		CannotOpenPipeEx(QString msg): QtCustomException(msg) {};
	};
	class CannotSaveOrientationsEx : public QtCustomException {
	public:
		CannotSaveOrientationsEx(QString msg): QtCustomException(msg) {};
	};
}

class fa_orfo::mainObject : public QObject {
	
	Q_OBJECT
	
public:
	QSettings settings;
	
	mainObject(QObject* parent = Q_NULLPTR);
	
	/** Set the parameters to their default values. */
	void defaultParameters();
	
	public Q_SLOTS:
	/** The main function: where everything begins. */
	Q_SLOT void mainFunction();
	
	/** Save results from the sender preprocessing algorithm.
	 The results are saved according to FVC specifications.
	 */
	Q_SLOT void saveResults(FA::Algorithm* sender);
	
	/** Quit the current thread if the global QThreadPool is empty. */
	Q_SLOT void checkActiveProcesses();
};

#endif /* fa_orfoMainObject_hpp */
