#ifndef fa_foeMainObject_hpp
#define fa_foeMainObject_hpp

#include <QObject>
#include <QThread>
#include <QThreadPool>
#include <QCommandLineParser>
#include <QSettings>
#include <QSharedPointer>
#include <QStringList>
#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QDebug>
#include <FingAnCore.hpp>
#include <FingAnInterface.hpp>
#include <Utilities.hpp>

namespace fa_foe {
	class mainObject;
}

class fa_foe::mainObject : public QObject {
	
	Q_OBJECT
	
public:
	QSettings settings;
	
	mainObject(QObject* parent = Q_NULLPTR);
	
	/** Set the parameters to their default values. */
	void defaultParameters();
	
	public Q_SLOTS:
	/** The main function: where everything begins. */
	Q_SLOT void mainFunction();
	
	/** Save results from the sender preprocessing algorithm.
	 The results are saved according to FVC specifications.
	 */
	Q_SLOT void saveResults(FA::Algorithm* sender);
	
	/** Quit the current thread if the global QThreadPool is empty. */
	Q_SLOT void checkActiveProcesses();
};

#endif /* fa_foeMainObject_hpp */
