#Add every subprojects, qmake will take care of everything
TEMPLATE = subdirs

SUBDIRS += FingAnCoreLib FingAnFieldLib FingAnInterfaceLib FingAnOperations

#Specify dependencies
FingAnInterfaceLib.depends = FingAnCoreLib FingAnFieldLib
FingAnOperations.depends = FingAnInterfaceLib FingAnCoreLib FingAnFieldLib

CONFIG += ordered
