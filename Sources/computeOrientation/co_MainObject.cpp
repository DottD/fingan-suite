#include "co_MainObject.hpp"

using namespace std;
using namespace cv;
using namespace fm;

mainObject::mainObject(QObject* parent) : QObject(parent){}

void mainObject::mainFunction(){
	// Initialize the command line parser
	QCommandLineParser parser;
	parser.setApplicationDescription("This tool is part of the FingAn Suite. It takes a fingerprint image as input and extract the orientation field.");
	parser.addHelpOption();
	parser.addVersionOption();
	parser.addPositionalArgument("imageFile", "Path to a fingerprint image, whose orientation has to be extracted.");
	parser.addPositionalArgument("outputFile", "Path to the output file, that will receive the orientation field.");
	
	// Parse the command line arguments
	QCoreApplication* app = QCoreApplication::instance();
	parser.process(*app);
	const QStringList args = parser.positionalArguments();
	if (args.length() != 2) throw QtCustomException("Syntax error: number of input arguments not valid.");
	const QString& imagePath = args[0];
	const QString& outputPath = args[1];
	
	// Read the image and compute the orientation field
	DoublePrecision::Image image = imread(imagePath.toStdString(), IMREAD_GRAYSCALE);
	normalize(image, image, 0, 1, NORM_MINMAX);
	DoublePrecision::Field field;
	DoublePrecision::Mask mask(image.size(), DoublePrecision().MaskTRUE);
	ComputeOrientation<DoublePrecision>(field, image, mask, 9, 1.0, 1.0, 2.0, 0.85, 2.0, 36, 1.0, 4.0, 0.5);
	
	// Compute orientation angle
	DoublePrecision::Image phase = fm::phase<DoublePrecision>(field);
	phase /= 2.; // halve the angles
	phase -= CV_PI_2; // take the orthogonal
	phase *= 180. / CV_PI; // convert to degrees
	subtract(phase, 180., phase, phase > 90.); // in [-180, 180] !!!
	add(phase, 180., phase, phase < -90.);
	
	// Compute output path
	QString imageName = QDir(imagePath).dirName().split('.').first();
	QFile phaseFile(outputPath);
	if(!phaseFile.open(QIODevice::WriteOnly | QIODevice::Text)) // open the file in write-only mode and check for success
		throw QtCustomException("Cannot open index file in write-only mode");
	QTextStream phaseStream(&phaseFile);
	for (int i = 0; i < phase.rows; i++){
		for (int j = 0; j < phase.cols-1; j++){
			phaseStream << phase(i,j) << " ";
		}
		phaseStream << phase(i,phase.cols-1) << endl;
	}
	phaseFile.close();
	cout << outputPath.toStdString() << endl;
	
	QCoreApplication::instance()->quit();
}
