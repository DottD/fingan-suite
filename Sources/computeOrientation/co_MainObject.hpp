#ifndef co_MainObject_hpp
#define co_MainObject_hpp

#include <QObject>
#include <QCommandLineParser>
#include <QDir>
#include <QFile>
#include <QDebug>
#include <Headers/FingAnField.hpp>

class mainObject : public QObject {
	
	Q_OBJECT
	
public:
	mainObject(QObject* parent = Q_NULLPTR);
	
	public Q_SLOTS:
	/** The main function: where everything begins. */
	Q_SLOT void mainFunction();
};

#endif /* co_MainObject_hpp */
