#include <Headers/Algorithm.hpp>

FA::Algorithm::Algorithm(QObject* parent) :
QObject(parent),
QRunnable(){
	// Prevent the QThreadPool to delete a parent instance
	this->setAutoDelete(false);
}

bool FA::Algorithm::isEnded(){
	return this->ended;
}

void FA::Algorithm::run(){
	this->ended = true;
	checkChildren();
}

void FA::Algorithm::checkChildren(){
	// Check if any children have completed their tasks
	QList<Algorithm*> receivers = findChildren<Algorithm*>(""/*name*/, Qt::FindDirectChildrenOnly);
	bool allStopped = true;
	for(Algorithm* receiver: receivers){
		if(!receiver->isEnded()){
			allStopped = false;
			// Pass output to that child
			receiver->setInput(this->getOutput());
			// When this instance completes its task, signal the parent
			// to check whether siblings have all completed their tasks.
			connect(receiver, SIGNAL(finished()), this, SLOT(checkChildren()));
			// Run child using the QThreadPool
			QThreadPool::globalInstance()->start(receiver);
		}
	}
	if(allStopped)
		Q_EMIT finished(this);
}

QList<QPair<QMetaProperty, QVariant>> FA::Algorithm::getOutput(){
	QList<QPair<QMetaProperty, QVariant>> propList;
    const QMetaObject* obj = this->metaObject();
	int propNum = obj->propertyCount();
    for(int k = 0; k < propNum; k++){
		// Check whether the current property's name starts with "algout_"
		if(QString(obj->property(k).name()).startsWith("algout_")){
			QMetaProperty prop = obj->property(k);
            propList << qMakePair(prop, prop.read(this));
		}
    }
	return propList;
}

void FA::Algorithm::setInput(QList<QPair<QMetaProperty, QVariant>> inputProps){
    const QMetaObject* obj = this->metaObject();
	int propNum = obj->propertyCount();
	for(int k = 0; k < propNum; k++){
		// Check whether the current property's name starts with "algin_"
		// In that event a prop with the same name must be given in input
		QMetaProperty outProp = obj->property(k);
        QString propName = outProp.name();
		if(propName.startsWith("algin_")){
			for(QPair<QMetaProperty, QVariant>& pair: inputProps){
				QMetaProperty& inputProp = pair.first;
                QVariant& inputValue = pair.second;
				if(propName.replace("algin_", "algout_") == inputProp.name()){
                    outProp.write(this, inputValue);
				}
			}
		}
	}
}
