#include <Headers/Utilities.hpp>

bool FA::Utilities::isFolder(const QString &path){
	return QDir(path).exists();
}

bool FA::Utilities::isFile(const QString &path){
	return QDir().exists(path);
}

cv::RotatedRect FA::Utilities::convertToRotatedRect(const cv::Rect &rect,
													const int& border){
	return cv::RotatedRect(rect.tl() + cv::Point(int(std::ceil(rect.width/2.0)), int(std::ceil(rect.height/2.0))) /*center x, y*/,
						   rect.size()+cv::Size(2*border, 2*border) /*width, height*/,
						   0.0 /*angle*/);
}

cv::Rect FA::Utilities::convertToRect(const cv::RotatedRect &rect,
									  const int& border){
	return cv::Rect(int(std::floor(rect.center.x-rect.size.width/2.0))-border,
					int(std::floor(rect.center.y-rect.size.height/2.0))-border,
					int(std::ceil(rect.size.width))+2*border,
					int(std::ceil(rect.size.height))+2*border);
}

QVector<QVariant> FA::Utilities::cvRotRect2QtVec(const cv::RotatedRect& rect){
	QVector<QVariant> vec;
	vec << rect.angle;
	vec << rect.size.width;
	vec << rect.size.height;
	vec << rect.center.x;
	vec << rect.center.y;
	return vec;
}

cv::RotatedRect FA::Utilities::QtVec2cvRotRect(const QVector<QVariant>& vec){
	cv::RotatedRect rect;
	rect.angle = vec[0].toFloat();
	rect.size.width = vec[1].toFloat();
	rect.size.height = vec[2].toFloat();
	rect.center.x = vec[3].toFloat();
	rect.center.y = vec[4].toFloat();
	return rect;
}
