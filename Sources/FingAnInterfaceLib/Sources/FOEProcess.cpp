#include <Headers/FOEProcess.hpp>

FA::FOEProcess::FOEProcess(QObject* parent) : FA::Algorithm(parent){
	qRegisterMetaType<QVector<QVariant>>();
	qRegisterMetaType<fm::DoublePrecision::Complex>();
}

fm::DoublePrecision::Field FA::FOEProcess::getField() const {
	return FA::Utilities::QtVec2cvMat<fm::DoublePrecision::Complex>(this->algout_field);
}

fm::DoublePrecision::Field FA::FOEProcess::getRawField() const {
	return FA::Utilities::QtVec2cvMat<fm::DoublePrecision::Complex>(this->algout_rawField);
}

void FA::FOEProcess::setFileName(QString fileName) {
	this->algin_fileName = std::move(fileName);
}

QString FA::FOEProcess::getFileName() const{
	return this->algin_fileName;
}

void FA::FOEProcess::setOutputPath(QString outputPath) {
	this->algin_outputPath = outputPath;
}

QString FA::FOEProcess::getOutputPath() const{
	return this->algin_outputPath;
}

void FA::FOEProcess::run(){
	// Set up for timing
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	// Load input
	fm::DoublePrecision::Image processedImage = Utilities::QtVec2cvMat<fm::DoublePrecision::Real>(algin_processedImage);
	fm::DoublePrecision::Mask maskFull = Utilities::QtVec2cvMat<fm::DoublePrecision::Uchar>(algin_mask);
	cv::RotatedRect rect = Utilities::QtVec2cvRotRect(algin_rect);
	// Check input variable
	if(rect.size.width == 0 || rect.size.height == 0 ||
	   processedImage.empty() ||
	   maskFull.empty())
		throw QtCustomException("FOEProcess::run - input is not correctly set");
	
	/* Declarations */
	fm::DoublePrecision::Real meanPeriod, varPeriod;
	cv::Rect cropRect = FA::Utilities::convertToRect(rect);
	fm::DoublePrecision::Image diffMask, image = processedImage(cropRect);
	fm::DoublePrecision::Mask mask;
	maskFull(cropRect).copyTo(mask); // direct use leads to error about matrix not continuous
	fm::DoublePrecision::Field field;
	QSettings settings;
	
	/* Compute the mean frequency of ridges and its standard deviation */
	fm::ComputeOrientation<fm::DoublePrecision>(field, image, mask,
												settings.value("InitialOrientationEstimation/radius").toInt(),
												settings.value("InitialOrientationEstimation/radDirGrad").toDouble(),
												settings.value("InitialOrientationEstimation/var1DirGrad").toDouble(),
												settings.value("InitialOrientationEstimation/exp1DirGrad").toDouble(),
												settings.value("InitialOrientationEstimation/var2DirGrad").toDouble(),
												settings.value("InitialOrientationEstimation/exp2DirGrad").toDouble(),
												settings.value("InitialOrientationEstimation/numAngles").toInt(),
												settings.value("InitialOrientationEstimation/weightGaussRadius").toDouble(),
												settings.value("InitialOrientationEstimation/weightGaussExp").toDouble(),
												settings.value("InitialOrientationEstimation/fieldGaussRadius").toDouble());
	
	algout_rawField = FA::Utilities::cvMat2QtVec<fm::DoublePrecision::Complex>(field);
	
	bool freqOK = fm::ComputeFrequency<fm::DoublePrecision>(meanPeriod, varPeriod, image, field, mask,
															settings.value("FrequencyEstimation/radius").toInt(),
															settings.value("FrequencyEstimation/step").toInt(),
															settings.value("FrequencyEstimation/numSamples").toInt(),
															settings.value("FrequencyEstimation/minNormQuantile").toDouble(),
															settings.value("FrequencyEstimation/gaussFilterSideVal").toInt(),
															settings.value("FrequencyEstimation/minValFreq").toDouble(),
															settings.value("FrequencyEstimation/maxValFreq").toDouble(),
															settings.value("FrequencyEstimation/leftQuantile").toDouble(),
															settings.value("FrequencyEstimation/rightQuantile").toDouble());
	if (!freqOK) throw QtCustomException("frequencyInfo reported an error");
	
	/* Smoothes field */
    field = fm::ComputeFieldFromImage<fm::DoublePrecision>(image, mask, meanPeriod,
                                                           settings.value("OrientationRefinement/OS_FIELD_NUM_SAMPLES").toInt(),
														   settings.value("OrientationRefinement/MASK_CONTRACTION_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM1_FIELD_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM1_SMOOTH_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM1_SMOOTH_STRENGTH").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM1_DRIFT_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM1_LD_THRESHOLD").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM1_MASK_CONTRACTION_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM1_MASK_BLUR_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM2_FIELD_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM2_ADJUST_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM2_ADJUST_MASK_RESCALE").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM2_ADJUST_RAD_FAC_2").toDouble(),//0.7
                                                           settings.value("OrientationRefinement/OS_SM2_ADJUST_MASK_RESCALE_2").toDouble(),//0.9
                                                           settings.value("OrientationRefinement/OS_SM2_DIFF_BLUR_RAD_FAC").toDouble(),//0.15
                                                           settings.value("OrientationRefinement/OS_SM2_DIFF_THRESHOLD").toDouble(),//0.5
                                                           settings.value("OrientationRefinement/OS_SM2_MIN_MAX_CENTROID_DIST_RAD_FAC").toDouble(),//1
                                                           settings.value("OrientationRefinement/OS_SM2_DIFF_DILATION_RAD_FAC").toDouble(),//1
                                                           settings.value("OrientationRefinement/OS_SM3_DRIFT_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM3_LD_BLUR_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM3_LD_THRESHOLD").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM3_LD_MIN_MAX_CENTROID_DIST_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM_DIFF_DILATION2_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_SM_DIFF_DILATION3_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_IS_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_IS_STRENGTH").toDouble(),
                                                           settings.value("OrientationRefinement/OS_IS_INITIAL_LEV").toDouble(),
                                                           settings.value("OrientationRefinement/OS_IS_MAX_STEPS").toDouble(),
                                                           settings.value("OrientationRefinement/OS_IS_CONTRACTION_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_IS_BLUR_RAD_FAC").toDouble(),
                                                           settings.value("OrientationRefinement/OS_IS_LEV_INCREMENT").toDouble());

	// Convert field
	algout_field = FA::Utilities::cvMat2QtVec<fm::DoublePrecision::Complex>(field);
	
	// Read timing and output to stream
	end = std::chrono::system_clock::now();
	int elapsed_seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
	std::cout << "Finished FOEProcess in " << elapsed_seconds << " ms" << std::endl;
	
	Algorithm::run();
}

void FA::FOEProcess::saveResults(){
	// Get the input file name
	QDir inputdir(algin_fileName);
	QString inputBaseName = inputdir.dirName();
	// Remove the extension from it
	QStringList dotSeparated = inputBaseName.split(".");
	dotSeparated.removeLast();
	inputBaseName = dotSeparated.join(".");
	// Check if outputPath is empty, eventually assign current directory
	if (algin_outputPath.isEmpty()) algin_outputPath = QDir::currentPath();
	// Get path to the original image output file
	QDir outputdir(algin_outputPath);
	QString outputRawFieldPath = outputdir.absoluteFilePath(inputBaseName+"_rawfield.png");
	// Get path to the significant image output file
	QString outputRefinedFieldPath = outputdir.absoluteFilePath(inputBaseName+"_field.png");
	
	// Save
	fm::DoublePrecision::Field rawfield = this->getRawField();
	fm::DoublePrecision::Field field = this->getField();
	PreProcessing* pre = dynamic_cast<PreProcessing*>(this->parent());
	fm::DoublePrecision::Image image;
	if (pre != Q_NULLPTR) {
		fm::DoublePrecision::Image temp = pre->getFinalImage();
		cv::Rect cropRect = Utilities::convertToRect(pre->getCroppingRect(), 0);
		image = temp(cropRect);
	} else {
		image = cv::imread(algin_fileName.toStdString());
	}
	fm::rescale(image, 255);
	cv::Mat outField;
	FA::Utilities::drawFieldOnImage<fm::DoublePrecision>(outField, image, field, 6, 4, 2, 4, true, cv::Scalar(255, 0, 0, 0));
	cv::imwrite(outputRawFieldPath.toStdString(), outField);
	FA::Utilities::drawFieldOnImage<fm::DoublePrecision>(outField, image, rawfield, 6, 4, 2, 4, true, cv::Scalar(255, 0, 0, 0));
	cv::imwrite(outputRefinedFieldPath.toStdString(), outField);
}

void FA::FOEProcess::loadResults(){
#pragma message("FOEProcess::loadResults not implemented")
}
