#include <Headers/RecurseScanInterface.hpp>
#include <QDebug>

void FA::RecurseScanInterface::run(){
	// Memory allocation
	QSharedPointer<QStringList> currentEntries(new QStringList);
	QSharedPointer<QDir> dir(new QDir), relToTopDir(new QDir);
	QSharedPointer<QString> relToTopPath(new QString);
	// Check if a path to the top directory has been given
	if (topDir.isNull() || topDir->path().isEmpty()) throw std::runtime_error("No topDir has been given.");
	// Check if every variable have been allocated
	if (exts.isNull()) exts = QSharedPointer<QStringList>(new QStringList);
	if (filesListAbs.isNull()) filesListAbs = QSharedPointer<QStringList>(new QStringList);
	if (filesListRel.isNull()) filesListRel = QSharedPointer<QStringList>(new QStringList);
	// Set filters to retrieve the correct files
	topDir->setFilter(QDir::Filter::NoDotAndDotDot|QDir::Filter::NoSymLinks|
				   QDir::Filter::Readable|QDir::Filter::Dirs);
	// Recursively extract a list of every processable file in the given directory
	QDirIterator dirIt(*topDir, QDirIterator::Subdirectories);
	// Change dir to find files
	topDir->setFilter(QDir::Filter::Files|QDir::Filter::Readable);
	// Append current directory files (as below for each subfolder)
	currentEntries->clear();
	currentEntries->append(topDir->entryList(*exts));
	for(QString& file: *currentEntries) file = topDir->absoluteFilePath(file);
	filesListAbs->append(*currentEntries);
	// Loop over directories found
	while (dirIt.hasNext()) {
		// Reassign dir pointing to the current directory
		dir->setPath(dirIt.next());
		// Assign the files contained in the current directory to a temporary list
		currentEntries->clear();
		currentEntries->append(dir->entryList(*exts));
		// Change every path contained there to an absolute one
		for(QString& file: *currentEntries) file = dir->absoluteFilePath(file);
		// Append the current list of strings to the general entries list
		filesListAbs->append(*currentEntries);
	}
	// Compute the relative path from the top directory to each file
	for (QString& fileAbs: *filesListAbs) {
		// Assign the path to the list
		filesListRel->append(topDir->relativeFilePath(fileAbs));
	}
}

FA::RecurseScanInterface::RecurseScanInterface(QObject *parent) : QObject(parent), QRunnable() {
	// Initialize pointers
	topDir = QSharedPointer<QDir>(new QDir);
	filesListAbs = QSharedPointer<QStringList>(new QStringList);
	filesListRel = QSharedPointer<QStringList>(new QStringList);
	exts = QSharedPointer<QStringList>(new QStringList);
}

FA::RecurseScanInterface::~RecurseScanInterface(){
	topDir.clear();
	filesListAbs.clear();
	filesListRel.clear();
	exts.clear();
}

void FA::RecurseScanInterface::setTopDir(const QString& dir) {
	this->topDir->setPath(dir);
}
void FA::RecurseScanInterface::setTopDir(QSharedPointer<QDir> newTopDir) {
	this->topDir = newTopDir;
}
QSharedPointer<QDir> FA::RecurseScanInterface::getTopDirPtr() {
	return this->topDir;
}
QDir FA::RecurseScanInterface::getTopDir() const {
	return *this->topDir;
}

void FA::RecurseScanInterface::setExts(const QStringList& newExts) {
	*(this->exts) = newExts;
}
void FA::RecurseScanInterface::setExts(QSharedPointer<QStringList> newExts) {
	this->exts = newExts;
}
QSharedPointer<QStringList> FA::RecurseScanInterface::getExtsPtr() {
	return this->exts;
}
QStringList FA::RecurseScanInterface::getExts() {
	return *this->exts;
}

QSharedPointer<QStringList> FA::RecurseScanInterface::getFilesAbsolutePtr() {
	return filesListAbs;
}
QSharedPointer<QStringList> FA::RecurseScanInterface::getFilesRelativePtr() {
	return filesListRel;
}

QStringList FA::RecurseScanInterface::getFilesAbsolute() {
	return *filesListAbs;
}
QStringList FA::RecurseScanInterface::getFilesRelative() {
	return *filesListRel;
}
