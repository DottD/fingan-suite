#include <Headers/PreProcessing.hpp>

FA::PreProcessing::PreProcessing(QObject* parent) : FA::Algorithm(parent) {
	qRegisterMetaType<QVector<QVariant>>();
}

void FA::PreProcessing::setInitialImage(fm::DoublePrecision::Image image){
    this->algout_originalImage = FA::Utilities::cvMat2QtVec<fm::DoublePrecision::Real>(image);
    setLoadImage(false);
}

fm::DoublePrecision::Image FA::PreProcessing::getInitialImage(){
    return FA::Utilities::QtVec2cvMat<fm::DoublePrecision::Real>(this->algout_originalImage);
}

void FA::PreProcessing::setLoadImage(bool loadImage){
    this->loadImage = loadImage;
}

bool FA::PreProcessing::getLoadImage() const{
    return this->loadImage;
}

void FA::PreProcessing::setFileName(QString fileName) {
	this->algin_fileName = std::move(fileName);
}

QString FA::PreProcessing::getFileName() const{
	return this->algin_fileName;
}

void FA::PreProcessing::setOutputPath(QString outputPath) {
	this->algin_outputPath = outputPath;
}

QString FA::PreProcessing::getOutputPath() const{
	return this->algin_outputPath;
}

void FA::PreProcessing::setMask(const cv::Mat1b& mask){
	this->algin_mask = Utilities::cvMat2QtVec<uchar>(mask);
	setComputeMask(false);
}

cv::Mat1b FA::PreProcessing::getMask() const{
	return Utilities::QtVec2cvMat<uchar>(this->algin_mask);
}

void FA::PreProcessing::setBdStep(QVector<QVariant> bd_step){
	this->algin_bd_step = bd_step;
}

QVector<QVariant> FA::PreProcessing::getBdStep() const{
	return this->algin_bd_step;
}

void FA::PreProcessing::setRotateImage(bool newRotateImage){
	this->rotateImage = newRotateImage;
}

bool FA::PreProcessing::getRotateImage() const{
	return this->rotateImage;
}

void FA::PreProcessing::setComputeMask(bool newComputeMask){
	this->computeMask = newComputeMask;
}

bool FA::PreProcessing::getComputeMask() const{
	return this->computeMask;
}

cv::Mat1d FA::PreProcessing::getFinalImage() const{
	return Utilities::QtVec2cvMat<double>(this->algout_processedImage);
}

cv::Mat1d FA::PreProcessing::getOriginalImage() const{
	return Utilities::QtVec2cvMat<double>(this->algout_originalImage);
}

cv::Mat1d FA::PreProcessing::getEqualizedImage() const{
	return Utilities::QtVec2cvMat<double>(this->algout_equalizedImage);
}

cv::Mat1d FA::PreProcessing::getSegmentedImage() const{
	return Utilities::QtVec2cvMat<double>(this->algout_segmentedImage);
}

cv::Mat1b FA::PreProcessing::getFinalMask() const{
	return Utilities::QtVec2cvMat<uchar>(this->algout_mask);
}

cv::RotatedRect FA::PreProcessing::getCroppingRect() const{
	return Utilities::QtVec2cvRotRect(this->algout_rect);
}

void FA::PreProcessing::run(){
	// Set up for timing
	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();
	/* Declarations */
	cv::Mat1d croppedImage, significantImage;
	cv::Mat1b significantMask;
	cv::Mat foreground, affineTrans;
	cv::Rect cropRect;
    cv::Mat1b mask;
	
    // Input check
	if(!computeMask && algin_mask.isEmpty()) // check if the mask must be computed
		throw std::runtime_error("Input mask is empty");
	
    // Read the image if needed
    cv::Mat1d processedImage;
    if (this->getLoadImage()){
        // Check input
        QDir fileNameDir(algin_fileName);
        if(algin_fileName.isEmpty() || !fileNameDir.exists(algin_fileName)/*as a file*/)
            throw std::runtime_error("Input file doesn't exist");
        // Load image
        processedImage = cv::imread(algin_fileName.toStdString(), cv::IMREAD_GRAYSCALE);
        algout_originalImage = FA::Utilities::cvMat2QtVec(processedImage);
    } else {
        processedImage = FA::Utilities::QtVec2cvMat<fm::DoublePrecision::Real>(this->algout_originalImage);
    }
    fm::rescale(processedImage); // scale in [0,1]
    QSettings settings;

	// Bimodalizzo l'immagine
	fm::imageBimodalize(processedImage, processedImage,
						settings.value("ImageBimodalize/brightness").toDouble(),
						settings.value("ImageBimodalize/leftCut").toDouble(),
						settings.value("ImageBimodalize/rightCut").toDouble(),
						settings.value("ImageBimodalize/histSmooth").toInt(),
						settings.value("ImageBimodalize/reparSmooth").toInt());
	algout_equalizedImage = FA::Utilities::cvMat2QtVec(processedImage);

	// Check if the mask must be computed
	if(computeMask){
		// Set the initial mask
		mask = cv::Mat1b::ones(processedImage.size()) * MASK_TRUE;
		
		// Remove from the image a border with almost constant values
		fm::ImageCroppingSimple(processedImage, mask,
								settings.value("ImageCroppingSimple/minVariation").toDouble(),
								settings.value("ImageCroppingSimple/marg").toInt());
		
		// Remove the part of the image outside the frame (for fingerprint cards)
		fm::ImageCroppingLines(processedImage, mask,
							   settings.value("TopMask/scanAreaAmount").toDouble(),
							   settings.value("TopMask/gradientFilterWidth").toDouble(),
							   settings.value("TopMask/gaussianFilterSide").toInt(),
							   settings.value("TopMask/binarizationLevel").toDouble(),
							   settings.value("TopMask/f").toDouble(),
							   settings.value("TopMask/slopeAngle").toDouble(),
							   settings.value("TopMask/lev").toDouble(),
							   settings.value("TopMask/marg").toInt());
		
		// Get only the significant part of the image
		fm::ImageSignificantMask(processedImage, mask,
								 settings.value("ImageSignificantMask/medFilterSide").toInt(),
								 settings.value("ImageSignificantMask/gaussFilterSide").toInt(),
								 settings.value("ImageSignificantMask/minFilterSide").toInt(),
								 settings.value("ImageSignificantMask/binLevVarMask").toDouble(),
								 settings.value("ImageSignificantMask/dilate1RadiusVarMask").toInt(),
								 settings.value("ImageSignificantMask/erodeRadiusVarMask").toInt(),
								 settings.value("ImageSignificantMask/dilate2RadiusVarMask").toInt(),
								 settings.value("ImageSignificantMask/maxCompNumVarMask").toInt(),
								 settings.value("ImageSignificantMask/minCompThickVarMask").toInt(),
								 settings.value("ImageSignificantMask/maxHolesNumVarMask").toInt(),
								 settings.value("ImageSignificantMask/minHolesThickVarMask").toInt(),
								 settings.value("ImageSignificantMask/histeresisThreshold1Gmask").toInt(),
								 settings.value("ImageSignificantMask/histeresisThreshold2Gmask").toInt(),
								 settings.value("ImageSignificantMask/radiusGaussFilterGmask").toInt(),
								 settings.value("ImageSignificantMask/minMeanIntensityGmask").toDouble(),
								 settings.value("ImageSignificantMask/dilate1RadiusGmask").toInt(),
								 settings.value("ImageSignificantMask/erodeRadiusGmask").toInt(),
								 settings.value("ImageSignificantMask/dilate2RadiusGmask").toInt(),
								 settings.value("ImageSignificantMask/maxCompNumGmask").toInt(),
								 settings.value("ImageSignificantMask/minCompThickGmask").toInt(),
								 settings.value("ImageSignificantMask/maxHolesNumGmask").toInt(),
								 settings.value("ImageSignificantMask/minHolesThickGmask").toInt(),
								 settings.value("ImageSignificantMask/histeresisThreshold3Gmask").toInt(),
								 settings.value("ImageSignificantMask/histeresisThreshold4Gmask").toInt(),
								 settings.value("ImageSignificantMask/dilate3RadiusGmask").toInt(),
								 settings.value("ImageSignificantMask/erode2RadiusGmask").toInt(),
								 settings.value("ImageSignificantMask/histeresisThreshold5Gmask").toInt(),
								 settings.value("ImageSignificantMask/histeresisThreshold6Gmask").toInt(),
								 settings.value("ImageSignificantMask/dilate4RadiusGmask").toInt(),
								 settings.value("ImageSignificantMask/radiusGaussFilterComp").toInt(),
								 settings.value("ImageSignificantMask/meanIntensityCompThreshold").toDouble(),
								 settings.value("ImageSignificantMask/dilateFinalRadius").toInt(),
								 settings.value("ImageSignificantMask/erodeFinalRadius").toInt(),
								 settings.value("ImageSignificantMask/smoothFinalRadius").toInt(),
								 settings.value("ImageSignificantMask/maxCompNumFinal").toInt(),
								 settings.value("ImageSignificantMask/minCompThickFinal").toInt(),
								 settings.value("ImageSignificantMask/maxHolesNumFinal").toInt(),
								 settings.value("ImageSignificantMask/minHolesThickFinal").toInt(),
								 settings.value("ImageSignificantMask/fixedFrameWidth").toInt(),
								 settings.value("ImageSignificantMask/smooth2FinalRadius").toInt());
		
		// Make sure mask is 0,255 valued
		mask = mask > 0;
    }else {
		// take the mask from input property
		mask = Utilities::QtVec2cvMat<uchar>(algin_mask) > 0;
		// Reduce the dimension of the image to fit the mask dimension
		cv::Rect maskRect(0, 0, mask.cols, mask.rows);
		cv::Mat1d temp;
		processedImage(maskRect).copyTo(temp);
		processedImage.release();
        temp.copyTo(processedImage);
	}
	
    // Temp
	cv::Mat1d segmented(processedImage.size());
	std::transform(mask.begin(), mask.end(), processedImage.begin(), segmented.begin(), [](const int& m, const double& b){
		if (m) return b;
		else return 0.0;
	});
	algout_segmentedImage = FA::Utilities::cvMat2QtVec(segmented);

	// Locate the foreground of the mask
	cv::RotatedRect rect;
	cv::findNonZero(mask, foreground);
	if (rotateImage && !foreground.empty()){
		// Compute the minimum area rectangle containing the foreground part of the mask
		rect = cv::minAreaRect(foreground);
		// Change the rectangle so that it minimizes the rotation angle
		if (std::abs(rect.angle) > 45) {
			rect.angle = -fm::sgn(rect.angle)*(90-std::abs(rect.angle));
			std::swap(rect.size.width, rect.size.height);
		}
		// Rotate the image and get the significant part
		affineTrans = cv::getRotationMatrix2D(rect.center, rect.angle, 1);
		cv::warpAffine(processedImage, processedImage, affineTrans, processedImage.size());
		cv::warpAffine(mask, mask, affineTrans, processedImage.size());
		// Compute the cropping rectangle
		cropRect = Utilities::convertToRect(rect, 0);
	}else if (!foreground.empty()){
		// Compute the cropping rectangle
		cropRect = cv::boundingRect(foreground);
		// Store information
		rect = Utilities::convertToRotatedRect(cropRect, 0);
	} else {
		// If the mask is empty use the whole image
		mask = MASK_TRUE;
		cropRect.x = 0;
		cropRect.y = 0;
		cropRect.height = processedImage.size().height;
		cropRect.width = processedImage.size().width;
		rect = Utilities::convertToRotatedRect(cropRect, 0);
    }
	significantMask = mask(cropRect); // use the same memory
	croppedImage = processedImage(cropRect); // use the same memory
    croppedImage.copyTo(significantImage, significantMask);

	// Equalization
	fm::ImageEqualize(significantImage, significantImage,
					  settings.value("ImageEqualize/minMaxFilter").toInt(),
					  settings.value("ImageEqualize/mincp1").toDouble(),
					  settings.value("ImageEqualize/mincp2").toDouble(),
					  settings.value("ImageEqualize/maxcp1").toDouble(),
					  settings.value("ImageEqualize/maxcp2").toDouble(),
					  significantMask);
    // set the whole image to 0, the foreground will be filled in the next line
    processedImage.setTo(0);
    significantImage.copyTo(croppedImage);

	// Convert to QMetaType compliant
	algout_rect = Utilities::cvRotRect2QtVec(rect);
	algout_mask = Utilities::cvMat2QtVec<uchar>(mask);
    processedImage.setTo(0, ~mask);
	algout_processedImage = Utilities::cvMat2QtVec<double>(processedImage);

	// Read timing and output to stream
	end = std::chrono::system_clock::now();
	int elapsed_seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
	std::cout << "Finished PreProcessing in " << elapsed_seconds << " ms" << std::endl;

	Algorithm::run();
}

void FA::PreProcessing::saveResults(){
	// Get the input file name
	QDir inputdir(algin_fileName);
	QString inputBaseName = inputdir.dirName();
	// Remove the extension from it
	QStringList dotSeparated = inputBaseName.split(".");
	dotSeparated.removeLast();
	inputBaseName = dotSeparated.join(".");
	// Check if outputPath is empty, eventually assign current directory
	if (algin_outputPath.isEmpty()) algin_outputPath = QDir::currentPath();
	// Get path to the original image output file
	QDir outputdir(algin_outputPath);
	QString outputOrigImagePath = outputdir.absoluteFilePath(inputBaseName+".png");
	// Get path to the significant image output file
	QString outputImagePath = outputdir.absoluteFilePath(inputBaseName+"_preproc.png");
	// Get path to the significant mask output file
	QString outputMaskPath = outputdir.absoluteFilePath(inputBaseName+"_mask.png");
	// Get path to the cropping parameters output file
	QString outputCropPath = outputdir.absoluteFilePath(inputBaseName+"_crop.txt");
	
	// Save
	cv::RotatedRect rect = Utilities::QtVec2cvRotRect(algout_rect);
	cv::Rect cropRect = Utilities::convertToRect(rect);
	cv::Mat1d processedImage = Utilities::QtVec2cvMat<double>(algout_processedImage);
	cv::Mat1b mask = Utilities::QtVec2cvMat<uchar>(algout_mask);
	fm::rescale(processedImage, 255);
	cv::imwrite(outputOrigImagePath.toStdString(), processedImage);
	cv::imwrite(outputImagePath.toStdString(), processedImage(cropRect));
	cv::imwrite(outputMaskPath.toStdString(), mask(cropRect)*255);
	QFile cropFile(outputCropPath);
	if (!cropFile.open(QFile::OpenModeFlag::WriteOnly))
		throw std::runtime_error("Unable to write cropping parameter file");
	QTextStream cropFileStream(&cropFile);
	cropFileStream << "Rotation center: " << rect.center.x << " " << rect.center.y << "\n";
	cropFileStream << "Rotation angle: " << rect.angle << "\n";
	cropFileStream << "Cropping rectangle: " << cropRect.x << " " << cropRect.y << " " << cropRect.width << " " << cropRect.height;
	cropFile.close();
}

void FA::PreProcessing::loadResults(){
#pragma message("PreProcessing::loadResults() not implemented")
}
