#ifndef FOEProcess_hpp
#define FOEProcess_hpp

#include <chrono>
#include <opencv2/opencv.hpp>
#include <Headers/FingAnCore.hpp>
#include <Headers/FingAnField.hpp>
#include <Headers/Algorithm.hpp>
#include <Headers/PreProcessing.hpp>
#include <Headers/Utilities.hpp>
#include <QSettings>

#ifndef QDECL_QVEC_QVAR
#define QDECL_QVEC_QVAR
Q_DECLARE_METATYPE(QVector<QVariant>)
#endif

#ifndef QDECL_CMPLX_NUM
#define QDECL_CMPLX_NUM
Q_DECLARE_METATYPE(fm::DoublePrecision::Complex);
#endif

namespace FA {
	class FOEProcess;
}

class FA::FOEProcess : public Algorithm {
	
	Q_OBJECT
	
	Q_PROPERTY(QVector<QVariant> algin_processedImage MEMBER algin_processedImage)
	Q_PROPERTY(QVector<QVariant> algin_mask MEMBER algin_mask)
	Q_PROPERTY(QVector<QVariant> algin_rect MEMBER algin_rect)
	Q_PROPERTY(QString algin_fileName READ getFileName WRITE setFileName)
	Q_PROPERTY(QString algin_outputPath READ getOutputPath WRITE setOutputPath)
	
	Q_PROPERTY(QVector<QVariant> algout_field MEMBER algout_field)
	Q_PROPERTY(QVector<QVariant> algout_rawField MEMBER algout_rawField)
	
private:
	QString algin_fileName, algin_outputPath;
	QVector<QVariant> algin_processedImage;
	QVector<QVariant> algin_mask;
	QVector<QVariant> algin_rect;
	
	QVector<QVariant> algout_field;
	QVector<QVariant> algout_rawField;
	
public:
	/** Empty constructor. */
	FOEProcess(QObject* parent = Q_NULLPTR);
	
	/** Returns the field. */
	fm::DoublePrecision::Field getField() const;
	
	/** Returns the field before the refinment. */
	fm::DoublePrecision::Field getRawField() const;
	
	/** Setter for the input image file name.
	 @sa getFileName
	 */
	void setFileName(QString fileName);
	
	/** Getter for the input image file name.
	 @sa setFileName
	 @return The input image file name.
	 */
	QString getFileName() const;
	
	/** Setter for the output path.
	 This path is used by the function saveResults.
	 @sa saveResults, getOutputPath
	 */
	void setOutputPath(QString outputPath);
	
	/** Getter for the output path.
	 @sa setOutputPath, saveResults
	 @return The path where the results will be saved.
	 */
	QString getOutputPath() const;
	
	public Q_SLOTS:
	/** Apply the preprocessing algorithm to the image corresponding to fileName.*/
	Q_SLOT void run();
	
	/** Save the results to files. */
	Q_SLOT void saveResults();
	
	/** Load the results from files. */
	Q_SLOT void loadResults();
	
};

#endif /* FOEProcess_hpp */
