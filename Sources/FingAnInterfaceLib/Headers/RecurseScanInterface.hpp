#ifndef RecurseScanInterface_hpp
#define RecurseScanInterface_hpp

#include <QObject>
#include <QRunnable>
#include <QSharedPointer>
#include <QDirIterator>
#include <QDir>

namespace FA {
	class RecurseScanInterface;
}

class FA::RecurseScanInterface : public QObject, public QRunnable {
	
	Q_OBJECT
	
private:
	QSharedPointer<QDir> topDir;
	QSharedPointer<QStringList> filesListAbs, filesListRel, exts;
	
public:
	RecurseScanInterface(QObject *parent=Q_NULLPTR);
	
	virtual ~RecurseScanInterface();
	
	void setTopDir(const QString& dir);
	void setTopDir(QSharedPointer<QDir> topDir);
	QSharedPointer<QDir> getTopDirPtr();
	QDir getTopDir() const;
	
	void setExts(const QStringList& exts);
	void setExts(QSharedPointer<QStringList> exts);
	QSharedPointer<QStringList> getExtsPtr();
	QStringList getExts();
	
	QSharedPointer<QStringList> getFilesAbsolutePtr();
	QSharedPointer<QStringList> getFilesRelativePtr();
	
	QStringList getFilesAbsolute();
	QStringList getFilesRelative();
	
	virtual void run();
	
signals:
	void newFileFound(QString);
};

#endif // RecurseScanInterface_hpp
