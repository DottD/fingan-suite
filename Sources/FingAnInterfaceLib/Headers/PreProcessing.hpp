#ifndef PreProcessing_hpp
#define PreProcessing_hpp

#include <QObject>
#include <QString>
#include <QSharedPointer>
#include <QDir>
#include <QDebug>

#include <opencv2/opencv.hpp>

#include <Headers/FingAnCore.hpp>
#include <Headers/FingAnField.hpp>
#include <Headers/Algorithm.hpp>
#include <Headers/Utilities.hpp>

#ifndef QDECL_QVEC_QVAR
#define QDECL_QVEC_QVAR
Q_DECLARE_METATYPE(QVector<QVariant>)
#endif

namespace FA {
	class PreProcessing;
}

/** Class that performs the preprocessing algorithm. */
class FA::PreProcessing : public FA::Algorithm {
	
	Q_OBJECT
	
	Q_PROPERTY(QString algin_fileName READ getFileName WRITE setFileName)
	Q_PROPERTY(QString algin_outputPath READ getOutputPath WRITE setOutputPath)
	Q_PROPERTY(cv::Mat1b algin_mask READ getMask WRITE setMask)
	Q_PROPERTY(QVector<QVariant> algin_bd_step READ getBdStep WRITE setBdStep)
	Q_PROPERTY(bool rotateImage READ getRotateImage WRITE setRotateImage)
	Q_PROPERTY(bool computeMask READ getComputeMask WRITE setComputeMask)
    Q_PROPERTY(bool loadImage READ getLoadImage WRITE setLoadImage)
	Q_PROPERTY(QVector<QVariant> algout_originalImage MEMBER algout_originalImage)
	Q_PROPERTY(QVector<QVariant> algout_equalizedImage MEMBER algout_equalizedImage)
	Q_PROPERTY(QVector<QVariant> algout_segmentedImage MEMBER algout_segmentedImage)
	Q_PROPERTY(QVector<QVariant> algout_processedImage MEMBER algout_processedImage)
	Q_PROPERTY(QVector<QVariant> algout_mask MEMBER algout_mask)
	Q_PROPERTY(QVector<QVariant> algout_rect MEMBER algout_rect)
	
private:
	QString algin_fileName, algin_outputPath;
	QVector<QVariant> algin_mask;
	QVector<QVariant> algin_bd_step;
	
	bool rotateImage = false;
	bool computeMask = true;
    bool loadImage = true;
	
	QVector<QVariant> algout_originalImage, algout_equalizedImage, algout_segmentedImage, algout_processedImage;
	QVector<QVariant> algout_mask;
	QVector<QVariant> algout_rect;
	
public:
	/** Empty constructor. */
	PreProcessing(QObject* parent = Q_NULLPTR);

    /** Setter for the initial image.
    This function set the initial image and prevent the algorithm to reload it from file.
    @getInitialImage
    */
    void setInitialImage(fm::DoublePrecision::Image image);

    /** Getter for the initial image property.
    @return The initial image property.
    @sa setInitialImage
    */
    fm::DoublePrecision::Image getInitialImage();

    /** Establish if the image should be loaded or taken as input.
     @sa getLoadImage
     */
    void setLoadImage(bool loadImage = true);

    /** Check if the image must be loaded.
     @sa setLoadImage
     @return Whether of not the image must be loaded.
     */
    bool getLoadImage() const;
	
	/** Setter for the input image file name.
	 @sa getFileName
	 */
	void setFileName(QString fileName);
	
	/** Getter for the input image file name.
	 @sa setFileName
	 @return The input image file name.
	 */
	QString getFileName() const;
	
	/** Setter for the output path.
	 This path is used by the function saveResults.
	 @sa saveResults, getOutputPath
	 */
	void setOutputPath(QString outputPath);
	
	/** Getter for the output path.
	 @sa setOutputPath, saveResults
	 @return The path where the results will be saved.
	 */
	QString getOutputPath() const;
	
	/** Set an initial input mask.
	 This function internally call setComputeMask(false).
	 @sa setComputeMask, getMask
	 */
	void setMask(const cv::Mat1b& mask);
	
	/** Get the initial input mask.
	 If computeMask is set to true an empty matrix is returned.
	 @sa setComputeMask, setMask
	 @return The initial input mask or an empty matrix if computeMask is true.
	 */
	cv::Mat1b getMask() const;
	
	/** Set border and step specifications. */
	void setBdStep(QVector<QVariant> bd_step);
	
	/** Get the current border and step specifications. */
	QVector<QVariant> getBdStep() const;
	
	/** Establish if the image cropping rectangle should consider rotations.
	 Before the equalization procedure, a rectangle that encapsules the foreground part
	 of the mask is computed. To reduce storage dimensions and computation time,
	 the user can tell this class to consider also a rotation angle for the cropping
	 rectangle. Default is false.
	 @sa getRotateImage
	 */
	void setRotateImage(bool rotateImage);
	
	/** Check if the rotation of the cropping rectangle is enabled.
	 @sa setRotateImage
	 @return Whether the rotation of the cropping rectangle is enabled.
	 */
	bool getRotateImage() const;
	
	/** Establish if the mask should be computed or taken as input.
	 @sa getComputeMask
	 */
	void setComputeMask(bool computeMask);
	
	/** Check if the mask must be computed.
	 @sa setComputeMask
	 @return Whether of not the mask must be computed.
	 */
	bool getComputeMask() const;
	
	/** Returns the processed image. */
	cv::Mat1d getFinalImage() const;
	
	/** Returns the original image. */
	cv::Mat1d getOriginalImage() const;
	
	/** Returns the equalized image. */
	cv::Mat1d getEqualizedImage() const;
	
	/** Returns the segmented image. */
	cv::Mat1d getSegmentedImage() const;
	
	/** Returns the final mask. */
	cv::Mat1b getFinalMask() const;
	
	/** Returns the cropping rectangle. */
	cv::RotatedRect getCroppingRect() const;
	
	public Q_SLOTS:
	/** Apply the preprocessing algorithm to the image corresponding to fileName.*/
	Q_SLOT void run();
	
	/** Save the results to files. */
	Q_SLOT void saveResults();
	
	/** Load the results from files. */
	Q_SLOT void loadResults();
	
};

#endif /* PreProcessing_hpp */
