#ifndef Algorithm_hpp
#define Algorithm_hpp

#include <QObject>
#include <QRunnable>
#include <QMetaObject>
#include <QMetaProperty>
#include <QThreadPool>
#include <QDebug>

namespace FA {
	class Algorithm;
}

class FA::Algorithm : public QObject, public QRunnable {
	
	Q_OBJECT
	
	Q_PROPERTY(bool ended READ isEnded)
	
private:
	bool ended = false;
	bool isEnded();
	
    public Q_SLOTS:
	/** Returns properties list for algorithm connections.
	 The output of this function can be passed to the setInput() method
	 of another algorithm instance, whose process() method will use that.
	 This method handles only the properties whose name start with the string
	 "algout_".
	 It is not guaranteed that the process() method makes any controls or
	 even takes input into account. It depends on the specific subclass.
	 Any properties can be passed through different algorithms, provided that
	 the property type is registered as a metatype (i.e. any type accepted
	 by QVariant can be used as property type).
	 @sa setInput(), process()
	 @return The list of properties in a format that allows sharing among
	 different instances.
	 */
	Q_SLOT QList<QPair<QMetaProperty, QVariant>> getOutput();
	
	/** Takes properties list from another algorithm instance.
	 The properties whose name starts with "algin_" are filled with values
	 from the properties whose name starts with "algout_" from inputProps.
	 @sa getOutput()
	 @param[in] inputProps List of properties from another algorithm; they
	 are used to setup the current Algorithm instance before running.
	 */
	Q_SLOT void setInput(QList<QPair<QMetaProperty, QVariant>> inputProps);

private Q_SLOTS:
	/** Check whether every children have completed their tasks. */
	Q_SLOT void checkChildren();
	
public:
	/** Constructor with parent.
	 An algorithm will always try to load properties from parent algorithms
	 and to pass properties to its children algorithms. Hence the Qt
	 parent-children system is the way chosen to provide algorithm
	 communication. Each algorithm is by default run in a separate thread
	 exploiting the features of QThreadPool.
	 To make connections among algorithms you only need to properly set a
	 parent-children hierarchy; no other setting is required.
	 If you need to handle signals from a mid-hierarchy algorithm you can
	 always add Qt connections.
	 */
	Algorithm(QObject* parent = Q_NULLPTR);
	
	public Q_SLOTS:
	/** Run the algorithm.
	 This pure virtual function must be reimplemented in each subclass;
	 indeed each subclass is a different algorithm, with its own rules.
	 A good process method should take into account the input given by
	 setInput(), and should emit the finished() signal at the end
	 of the computation. This will make possible connecting two algorithms
	 together, without the need to set up each one and instruct them 
	 how to run.
	 @sa setInput, finished
	 */
	Q_SLOT virtual void run();
	
	/** Save the results to files. */
	Q_SLOT virtual void saveResults() = 0;
	
	/** Load the results from files. */
	Q_SLOT virtual void loadResults() = 0;
	
Q_SIGNALS:
	/** Signal emitted when every children have completed their tasks.
	 Each subclass must call Algorithm::run() at the end of the computation,
	 otherwise this signal is not emitted. The argument of the signal
	 is a pointer to the emitting algorithm instance; it can be used
	 to get the results from other threads.
	 @sa run()
	 */
	Q_SIGNAL void finished(FA::Algorithm* sender = Q_NULLPTR);
};

#endif /* Algorithm_hpp */
