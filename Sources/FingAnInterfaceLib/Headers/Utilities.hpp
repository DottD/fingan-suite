#ifndef Utilities_hpp
#define Utilities_hpp

#include <QtCore>
#include <QVector>
#include <QVariant>
#include <QDir>
#include <QFile>
#include <QTextStream>
#include <QDataStream>
#include <opencv2/opencv.hpp>
#include <Headers/FingAnField.hpp>
#include <Headers/FieldOperations.hpp>

namespace FA {
	namespace Utilities {
		/** Utility to check is a given path is actually a file or a folder.
		 This is the same as calling QDir::exists() on a QDir instance
		 containing the same path.
		 @param[in] path Path to be checked.
		 @return True if the given path corresponds to an existing folder, false
		 if it is a file or does not exist.
		 */
		bool isFolder(const QString& path);
		
		/** Utility to check is a given path is actually a file or a folder.
		 This is the same as calling QDir::exists(path).
		 @param[in] path Path to be checked.
		 @return True if the given path corresponds to an existing file, false
		 if it is a folder or does not exist.
		 */
		bool isFile(const QString& path);
		
		/** Utility to convert a cv::Rect in cv::RotatedRect with the same features.
		 @sa PreProcessing::convertToRect
		 @return The rotated version of the given rectangle.
		 @param[in] rect The rectangle to be converted.
		 @param[in] border An optional offset to be added to the final rectangle.
		 */
		cv::RotatedRect convertToRotatedRect(const cv::Rect& rect,
											 const int& border = 0);
		
		/** Utility to convert a cv::RotatedRect in cv::Rect with the same features.
		 The angle of the given rectangle is not considered during conversion.
		 @sa PreProcessing::convertToRotatedRect
		 @return The up-right version of the given rectangle.
		 @param[in] rect The rectangle to be converted.
		 @param[in] border An optional offset to be added to the final rectangle.
		 */
		cv::Rect convertToRect(const cv::RotatedRect& rect,
							   const int& border = 0);
		
		/** Utility to convert from cv::Mat to QVector. */
		template <typename T>
		QVector<QVariant> cvMat2QtVec(const cv::Mat_<T>& mat){
			QVector<QVariant> vec;
			vec << uint(mat.rows);
			vec << uint(mat.cols);
			for(const T& val: mat)
				vec << QVariant::fromValue(T(val));
			return vec;
		};
		
		/** Utility to convert from QVector to cv::Mat. */
		template <typename T>
		cv::Mat_<T> QtVec2cvMat(const QVector<QVariant>& vec){
			int rows = vec[0].toInt();
			int cols = vec[1].toInt();
			cv::Mat_<T> mat(rows, cols);
			typename cv::Mat_<T>::iterator i = mat.begin();
			for(int k = 2; k < vec.length() && i != mat.end(); k++, i++)
				*i = vec[k].value<T>();
			return mat;
		};
		
		/** Utility to convert from cv::RotatedRect to QVector. */
		QVector<QVariant> cvRotRect2QtVec(const cv::RotatedRect& rect);
		
		/** Utility to convert from cv::RotatedRect to QVector. */
		cv::RotatedRect QtVec2cvRotRect(const QVector<QVariant>& vec);
		
		/** Draw a field over an image.
		 Outputs a 3-channels image with the input vector field drawn as a set
		 of arrows. Many parameters are present to customize the output.
		 @param[out] out Output 3-channel image, same type as input.
		 @param[in] image Image where the vector field must be drawn.
		 @param[in] squareField Vector field to draw.
		 @param[in] steo Distance between two adjacent arrows.
		 @param[in] lengthFactor Control the length of the arrows.
		 @oaram[in] thicknessFactor Control the thickness of the arrows.
		 @param[in] sampling Oversampling performed before the drawing; may be needed to
		 enhance the quality of the output.
		 @param[in] fieldComputeOrtho Whether to draw the orthogonal or the original field.
		 @param[in] color Color used to draw the field.
		 */
		template <typename PrecisionPolicy = fm::DoublePrecision>
		void drawFieldOnImage(cv::Mat& out,
							  const cv::Mat& image,
							  const typename PrecisionPolicy::Field squareField,
							  const int& step,
							  const int& lengthFactor,
							  const int& thicknessFactor,
							  const typename PrecisionPolicy::Real sampling,
							  const bool& fieldComputeOrtho,
							  const cv::Scalar& color){
			typedef typename PrecisionPolicy::Mask Mask;
			typedef typename PrecisionPolicy::Image Image;
			typedef typename PrecisionPolicy::Field Field;
			typedef typename PrecisionPolicy::Int Int;
			typedef typename PrecisionPolicy::Real Real;
			typedef typename PrecisionPolicy::Complex Complex;
			
			// Compute the ratio between image and field dimensions
			Real fieldSampling;
			if (image.empty() || squareField.empty()) throw QtCustomException("drawFieldOnImage - Empty input image or field");
			else {
				Real wratio = Real(image.size().width)/Real(squareField.size().width);
				Real hratio = Real(image.size().height)/Real(squareField.size().height);
				fieldSampling = std::min<Real>(wratio, hratio);
			}
			
			// Compute phase and magnitude of the given field
			Image magnitude, phase;
			fm::cartToPolar<PrecisionPolicy>(squareField, magnitude, phase);
			// Halve input phase, then rotate if requested
			phase = -( phase + (Real)fieldComputeOrtho * CV_PI ) / 2.0;
			Mask zeroMagnitude = magnitude < 1E-4;
			magnitude.setTo(0.0, zeroMagnitude);
			magnitude.setTo(1.0, ~zeroMagnitude);
			Field dirField;
			fm::polarToCart<PrecisionPolicy>(magnitude, phase, dirField);
			
			// Generate the list of segments to draw
			std::vector<std::vector<cv::Point>> pts; // list of segments heads and tails
			std::vector<cv::Point> line(2, cv::Point()); // punto del segmento
			for (Int i = step; i < squareField.rows-step; i += step) {
				for (Int j = step; j < squareField.cols-step; j += step) {
					// Check if the magnitude is zero
					if (magnitude(i, j) == 0.0) continue;
					// Compute head and tail of the current segment (rounded to the closest integer)
					const Complex& F = dirField(i, j);
					cv::Point2d midPt = sampling * fieldSampling * cv::Point2d(j, i);
					cv::Point2d sidesPt = sampling * lengthFactor * cv::Point2d(F[0], F[1]);
					line[0] = (midPt + sidesPt);
					line[1] = (midPt - sidesPt);
					// Add to the list of segments
					pts.emplace_back(line);
				}
			}
			// Create a 3-channel image
			cv::Mat resized; // resized version of the input image
			cv::resize(image, resized, cv::Size(), sampling, sampling);
			out.create(resized.size(), CV_MAKETYPE(resized.depth(), 3));
			if(resized.channels() == 3){
				resized.copyTo(out);
			} else if (resized.channels() == 1){
				cv::mixChannels(resized, out, {0, 0, 0, 1, 0, 2});
			} else {
				throw QtCustomException("drawFieldOnImage - The image must have 1 or 3 channels");
			}
			// Draw the segments
			cv::polylines(out, pts,
						  false, // we don't need close lines
						  color,
						  thicknessFactor,
						  cv::LineTypes::LINE_8);
		}
		
	}
}

#endif /* Utilities_hpp */
