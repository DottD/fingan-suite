#Main Qt project file (for win32 static release compilation)

#Instruct to compile a console application
TEMPLATE = lib
CONFIG += release c++14 static
QT -= gui

#Link OpenCV
INCLUDEPATH += C:/Libs/OpenCV/include
LIBS += "C:/Libs/OpenCV/x86/vc15/staticlib/*.lib"

#Link Armadillo
INCLUDEPATH += C:/Libs/armadillo/include
LIBS += $$files(C:/Libs/armadillo/lib/*.lib, true)

#Link ALGLIB
INCLUDEPATH += C:/Libs/alglib
LIBS += $$files(C:/Libs/alglib/lib/*.lib, true)

#Name the output file
TARGET = fa_interface_lib

#Instruct about build folder
DESTDIR = $$BLD_DIR

#Add the dependencies
DEPENDPATH += $$BLD_DIR
INCLUDEPATH += $$SRC_DIR/FingAnCoreLib $$SRC_DIR/FingAnFieldLib
LIBS += -L$$BLD_DIR -lfa_core_lib

#Add sources and headers
HEADERS += $$files(Headers/*.hpp, true)
SOURCES += $$files(Sources/*.cpp, true)
